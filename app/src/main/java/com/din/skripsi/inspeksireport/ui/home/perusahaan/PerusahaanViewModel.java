package com.din.skripsi.inspeksireport.ui.home.perusahaan;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.delete_perusahaan.DeletePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanNavigator;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class PerusahaanViewModel extends BaseViewModel<PerusahaanNavigator> implements PerusahaanItemViewModel.PerusahaanItemViewModelListener {

    private final MutableLiveData<List<PerusahaanItemViewModel>> perusahaanItemsLiveData;



    public PerusahaanViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        perusahaanItemsLiveData = new MutableLiveData<>();
//        fetchperusahaans();
    }

    public void fetchperusahaans() {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);
        getCompositeDisposable().add(getDataManager()
                .getAllPerusahaanApiCall()
                .map(perusahaanResponse -> perusahaanResponse.getListDataPerusahaan())
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(perusahaanResponse -> {
                    perusahaanItemsLiveData.setValue(perusahaanResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public LiveData<List<PerusahaanItemViewModel>> getPerusahaanItemsLiveData() {
        return perusahaanItemsLiveData;
    }

    private Single<List<PerusahaanItemViewModel>> getViewModelList(List<PerusahaanResponse.DataPerusahaan> repoList) {
        return Observable.fromIterable(repoList)
                .map(repo -> {
                    // masuk
                    return new PerusahaanItemViewModel(
                            repo.getIdPerusahaan(), repo.getNamaPerusahaan(),repo.getKodePerusahaan(),repo.getAlamatPerusahaan(), repo.getCreatedDate(),this);
                }).toList();
    }


    public void deletePerusahaan(int id_perusahaan) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postDeletePerusahaanApiCall(new DeletePerusahaanRequest(id_perusahaan))
                .map(deletePerusahaanResponse -> deletePerusahaanResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
//                    setIsLoading(false);
                    getNavigator().isRefresh();
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void toCreatePerusahaanClick() {
        getNavigator().openCreatePerusahaanActivity();
    }


    public void onBackClick() {
        getNavigator().onBack();
    }


    @Override
    public void onItemClick(int idperusahaan,String nama,String kode_perusahaan,String alamat_perusahaan) {
                try {
                    getNavigator().openDialog(idperusahaan,nama,kode_perusahaan,alamat_perusahaan);
        } catch (Exception e) {
        }

    }
}
