package com.din.skripsi.inspeksireport.ui.create_product;

public interface CreateProductNavigator {

    void handleError(Throwable throwable);

    void createProduct();

    void openMainActivity();

}
