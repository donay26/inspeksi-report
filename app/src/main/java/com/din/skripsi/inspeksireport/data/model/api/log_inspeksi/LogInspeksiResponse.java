package com.din.skripsi.inspeksireport.data.model.api.log_inspeksi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LogInspeksiResponse {


        @Expose
        @SerializedName("data")
        private List<ItemLogInspeksi> listLogInspeksi;

        @Expose
        @SerializedName("status_code")
        private boolean statusCode;

        @Expose
        @SerializedName("message")
        private String message;

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof LogInspeksiResponse)) {
                return false;
            }

            LogInspeksiResponse that = (LogInspeksiResponse) o;

            if (statusCode != that.statusCode) {
                return false;
            }
            if (!message.equals(that.message)) {
                return false;
            }


            return listLogInspeksi.size() != that.listLogInspeksi.size();
        }

        @Override
        public int hashCode() {

            int result = listLogInspeksi.hashCode();
            result = 31 * result;
            return result;
        }

        public List<ItemLogInspeksi> getListItemLogInspeksi() {
            return listLogInspeksi;
        }
        public String getMessage(){
            return message;
        }

        public boolean getStatusCode() {
            return statusCode;
        }

        public void setDataProduct(List<ItemLogInspeksi> data) {
            this.listLogInspeksi = data;
        }


        public static class ItemLogInspeksi {

            @Expose
            @SerializedName("nama")
            private String nama;

            @Expose
            @SerializedName("role")
            private String role;

            @Expose
            @SerializedName("serial_number")
            private String serial_number;

            @Expose
            @SerializedName("note")
            private String note;

            @Expose
            @SerializedName("created_date")
            private String created_date;



            public String getNama() {
                return nama;
            }

            public String getRole() {
                return role;
            }


            public String getSerialNumber() {
                return serial_number;
            }

            public String getNote() {
                return note;
            }

            public String getCreatedDate() {
                return created_date;
            }




        }
    }

