package com.din.skripsi.inspeksireport.ui.home.rekomendasi;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.delete_rekomendasi.DeleteRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class RekomendasiViewModel extends BaseViewModel<RekomendasiNavigator> implements RekomendasiItemViewModel.RekomendasiItemViewModelListener {

    private final MutableLiveData<List<RekomendasiItemViewModel>> productItemsLiveData;



    public RekomendasiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        productItemsLiveData = new MutableLiveData<>();
//        fetchRekomendasis();
    }

    public void fetchRekomendasis() {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);
        getCompositeDisposable().add(getDataManager()
                .getAllRekomendasiApiCall()
                .map(rekomendasiResponse -> rekomendasiResponse.getListDataRekomendasi())
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(productResponse -> {
                    productItemsLiveData.setValue(productResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public LiveData<List<RekomendasiItemViewModel>> getRekomendasiItemsLiveData() {
        return productItemsLiveData;
    }

    private Single<List<RekomendasiItemViewModel>> getViewModelList(List<RekomendasiResponse.DataRekomendasi> repoList) {
        return Observable.fromIterable(repoList)
                .map(repo -> {
                    // masuk
                    return new RekomendasiItemViewModel(
                            repo.getIdRekomendasi(), repo.getTilteRekomendasi(), repo.getCreatedDate(),this);
                }).toList();
    }


    public void deleteRekomendasi(int id_rekomendasi) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postDeleteRekomendasiApiCall(new DeleteRekomendasiRequest(id_rekomendasi))
                .map(deleteRekomendasiResponse -> deleteRekomendasiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
//                    setIsLoading(false);
                    getNavigator().isRefresh();
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void toCreateRekomendasiClick() {
        getNavigator().openCreateRekomendasiActivity();
    }


    public void onBackClick() {
        getNavigator().onBack();
    }


    @Override
    public void onItemClick(int idproduct,String nama) {
                try {
                    getNavigator().openDialog(idproduct,nama,0);
        } catch (Exception e) {
        }

    }
}
