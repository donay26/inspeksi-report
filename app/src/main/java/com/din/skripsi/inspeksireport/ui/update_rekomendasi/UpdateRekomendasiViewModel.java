package com.din.skripsi.inspeksireport.ui.update_rekomendasi;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.update_rekomendasi.UpdateRekomendasiRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class UpdateRekomendasiViewModel extends BaseViewModel<UpdateRekomendasiNavigator> {


    public UpdateRekomendasiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }
        return true;
    }

    public void updateRekomendasi(int idRekomendasi,String title) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postUpdateRekomendasiApiCall(new UpdateRekomendasiRequest(title,idRekomendasi))
                .map(updateRekomendasiResponse -> updateRekomendasiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void onServerUpdateRekomendasiClick() {
        getNavigator().updateRekomendasi();
    }
}