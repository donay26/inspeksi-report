package com.din.skripsi.inspeksireport.ui.log_inspeksi;

public class LogInspeksiEmptyItemViewModel {

    private final LogInspeksiEmptyItemViewModelListener mListener;

    public LogInspeksiEmptyItemViewModel(LogInspeksiEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface LogInspeksiEmptyItemViewModelListener {

        void onRetryClick();
    }
}

