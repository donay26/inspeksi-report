package com.din.skripsi.inspeksireport.ui.create_item_inspeksi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;

import java.util.ArrayList;
import java.util.List;

public class SpinnerStatusAdapter extends BaseAdapter {
    Context context;

    List<StatusResponse.DataStatus> dataStatuss =  new ArrayList<>();
    LayoutInflater inflter;

    public SpinnerStatusAdapter(Context applicationContext, List<StatusResponse.DataStatus> dataStatuss) {
        this.context = applicationContext;
        this.dataStatuss = dataStatuss;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        if (dataStatuss !=  null) {
            return dataStatuss.size();
        } else {
            return 0;
        }
    }

    @Override
    public StatusResponse.DataStatus getItem(int i) {
        return dataStatuss.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_spinner_status, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(dataStatuss.get(i).getNamaStatus());
        return view;
    }
}
