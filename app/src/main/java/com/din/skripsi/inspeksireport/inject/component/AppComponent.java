package com.din.skripsi.inspeksireport.inject.component;

import android.app.Application;

import com.din.skripsi.inspeksireport.App;
import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.inject.module.AppModule;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(App app);

    DataManager getDataManager();

    SchedulerProvider getSchedulerProvider();

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}