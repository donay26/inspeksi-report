package com.din.skripsi.inspeksireport.ui.home.history;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.FragmentHistoryBinding;
import com.din.skripsi.inspeksireport.inject.component.FragmentComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseFragment;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.DetailInspeksiActivity;

import javax.inject.Inject;

public class HistoryFragment extends BaseFragment<FragmentHistoryBinding, HistoryViewModel> implements HistoryNavigator, HistoryAdapter.HistoryInspeksiAdapterListener {

    public static final String TAG = "HistoryFragment";


    FragmentHistoryBinding mFragmentHistoryBinding;
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    HistoryAdapter mInspeksiAdapter;


    public static HistoryFragment newInstance() {
        Bundle args = new Bundle();
        HistoryFragment fragment = new HistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_history;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
        mInspeksiAdapter.setListener(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentHistoryBinding = getViewDataBinding();
        mViewModel.fetchHistorys();
        setUp();
    }


    @Override
    public void performDependencyInjection(FragmentComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void openDetail(String idUniqueInspeksi,int idInspeksi, int statusReport, int idProduct, String namaProduct, String nomorSurat, String serialNumber, String namaPerusahaan, String kodePerusahaan, String alamatPerusahaan, String note, String image_name, String createdDate, String updated_date) {
        Intent intent = DetailInspeksiActivity.newIntent(getContext(),idUniqueInspeksi,idInspeksi,statusReport,idProduct,namaProduct,nomorSurat,namaPerusahaan,kodePerusahaan,serialNumber,note,alamatPerusahaan,createdDate,updated_date,image_name);
        startActivity(intent);

    }



    @Override
    public void isSwipeRefresh(boolean swipe) {
        mFragmentHistoryBinding.swipeLayout.setRefreshing(swipe);
    }

    @Override
    public void onBack() {

    }

    private void setUp() {
        mFragmentHistoryBinding.swipeLayout.setOnRefreshListener(() -> mViewModel.fetchHistorys());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentHistoryBinding.historyRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentHistoryBinding.historyRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentHistoryBinding.historyRecyclerView.setAdapter(mInspeksiAdapter);
    }


    @Override
    public void onRetryClick() {
        mViewModel.fetchHistorys();
    }
}
