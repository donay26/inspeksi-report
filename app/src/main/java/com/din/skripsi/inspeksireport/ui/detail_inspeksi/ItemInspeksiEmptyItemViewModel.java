package com.din.skripsi.inspeksireport.ui.detail_inspeksi;

public class ItemInspeksiEmptyItemViewModel {

    private final ItemInspeksiEmptyItemViewModelListener mListener;

    public ItemInspeksiEmptyItemViewModel(ItemInspeksiEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface ItemInspeksiEmptyItemViewModelListener {

        void onRetryClick();
    }
}
