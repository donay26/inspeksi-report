package com.din.skripsi.inspeksireport.ui.home.history;

import androidx.databinding.ObservableField;


public class HistoryItemViewModel   {

    public final ObservableField<String> _idUniqueInspeksi = new ObservableField<>();
    public final ObservableField<Integer> _idInspeksi = new ObservableField<>();
    public final ObservableField<Integer> _statusReport = new ObservableField<>();
    public final ObservableField<Integer> _idProduct = new ObservableField<>();
    public final ObservableField<String> _nomorSurat = new ObservableField<>();
    public final ObservableField<String> _serialNumber = new ObservableField<>();
    public final ObservableField<String> _namaProduct = new ObservableField<>();
    public final ObservableField<String> _namaPerusahaan = new ObservableField<>();
    public final ObservableField<String> _kodePerusahaan = new ObservableField<>();
    public final ObservableField<String> _imageName = new ObservableField<>();
    public final ObservableField<String> _alamatPerusahaan = new ObservableField<>();
    public final ObservableField<String> _createdDate = new ObservableField<>();
    public final ObservableField<String> _updateDate = new ObservableField<>();
    public final ObservableField<String> _note = new ObservableField<>();

    public final HistoryItemViewModel.HistoryItemViewModelListener mListener;

    public HistoryItemViewModel(String idUniqueInspeksi,
            int idInspeksi,
                                int statusReport,
                                int idProduct,
                                String namaProduct,
                                String nomor_surat,
                                String serial_number,
                                String nama_perusahaan,
                                String kode_perusahaan,
                                String alamat_perusahaan,
                                String note,
                                String created_date,
                                String updated_date,
                                String image_name,
                                HistoryItemViewModel.HistoryItemViewModelListener listener) {
        this.mListener = listener;
        this._idUniqueInspeksi.set(idUniqueInspeksi);
        this._idInspeksi.set(idInspeksi);
        this._statusReport.set(statusReport);
        this._idProduct.set(idProduct);
        this._namaProduct.set(namaProduct);
        this._serialNumber.set(serial_number);
        this._nomorSurat.set(nomor_surat);
        this._namaPerusahaan.set(nama_perusahaan);
        this._kodePerusahaan.set(kode_perusahaan);
        this._alamatPerusahaan.set(alamat_perusahaan);
        this._note.set(note);
        this._createdDate.set(created_date);
        this._updateDate.set(updated_date);
        this._imageName.set(image_name);
    }

    public void onItemClick() {
        mListener.onItemClick(_idUniqueInspeksi.get(),_idInspeksi.get(), _statusReport.get(), _idProduct.get(),_namaProduct.get(), _nomorSurat.get(),_serialNumber.get(), _namaPerusahaan.get(),_kodePerusahaan.get(),_alamatPerusahaan.get(), _createdDate.get(),_updateDate.get(),_note.get(),_imageName.get());
    }

    public interface HistoryItemViewModelListener {

        void onItemClick(
                String idUniqueInspeksi,
                int idInspeksi,
                         int statusReport,
                         int idProduct,
                         String namaProduct,
                         String nomorSurat,
                         String serialnumber,
                         String namaPerusahaan,
                         String kodePerusahaan,
                         String alamatPerusahaan,
                         String createdDate,
                         String updatedDate,
                         String note,
                         String image_name);

    }

}
