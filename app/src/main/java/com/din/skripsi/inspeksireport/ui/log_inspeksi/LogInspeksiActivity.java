package com.din.skripsi.inspeksireport.ui.log_inspeksi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityLogInspeksiBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;


import javax.inject.Inject;

public class LogInspeksiActivity extends BaseActivity<ActivityLogInspeksiBinding, LogInspeksiViewModel> implements LogInspeksiNavigator , LogInspeksiAdapter.LogInspeksiAdapterListener{

    private ActivityLogInspeksiBinding mActivityLogInspeksiBinding;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    LogInspeksiAdapter mLogInspeksiAdapter;


    public static Intent newIntent(Context context) {
        return new Intent(context, LogInspeksiActivity.class);
    }



    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_log_inspeksi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityLogInspeksiBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        mLogInspeksiAdapter.setListener(this);
        setSupportActionBar(mActivityLogInspeksiBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notification");
        mViewModel.fetchLogInspeksis();
        setUp();


    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void isRefresh() {
        mViewModel.fetchLogInspeksis();
    }

    @Override
    public void isSwipeRefresh(boolean swipe) {

    }

    @Override
    public void onBack() {

    }

    private void setUp() {
        mActivityLogInspeksiBinding.swipeLayout.setOnRefreshListener(() -> mViewModel.fetchLogInspeksis());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mActivityLogInspeksiBinding.productRecyclerView.setLayoutManager(mLayoutManager);
        mActivityLogInspeksiBinding.productRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mActivityLogInspeksiBinding.productRecyclerView.setAdapter(mLogInspeksiAdapter);
    }


    @Override
    public void onRetryClick() {
        mViewModel.fetchLogInspeksis();
    }

}