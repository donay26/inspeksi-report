package com.din.skripsi.inspeksireport.ui.create_rekomendasi;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityCreateRekomendasiBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;

public class CreateRekomendasiActivity extends BaseActivity<ActivityCreateRekomendasiBinding, CreateRekomendasiViewModel> implements CreateRekomendasiNavigator {

    private ActivityCreateRekomendasiBinding mActivityCreateRekomendasiBinding;


    public static Intent newIntent(Context context) {
        return new Intent(context, CreateRekomendasiActivity.class);
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_rekomendasi;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCreateRekomendasiBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setSupportActionBar(mActivityCreateRekomendasiBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void createRekomendasi() {
        String namaRekomendasi = mActivityCreateRekomendasiBinding.etRekomendasi.getText().toString();

        if (mViewModel.isValid(namaRekomendasi)) {
            hideKeyboard();
            mViewModel.createRekomendasi(namaRekomendasi);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(CreateRekomendasiActivity.this);
        startActivity(intent);
        finish();
    }


}