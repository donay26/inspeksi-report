package com.din.skripsi.inspeksireport.ui.update_user;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.create_user.CreateUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_inspeksi.UpdateInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_user.UpdateUserRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.ui.update_inspeksi.UpdateInspeksiNavigator;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

public class UpdateUserViewModel extends BaseViewModel<UpdateUserNavigator> {


    public UpdateUserViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama, String nik, String password) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }
        if (TextUtils.isEmpty(nik)) {
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            return false;
        }
        return true;
    }

    public void updateUser(int idUser,String nama, String nik, String password, int role,int status) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postUpdateUserApiCall(new UpdateUserRequest(idUser,nama, nik, password, role,status))
                .map(updateUserResponse -> updateUserResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void onServerUpdateUserClick() {
        getNavigator().updateUser();
    }
}