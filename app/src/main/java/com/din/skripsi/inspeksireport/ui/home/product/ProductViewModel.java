package com.din.skripsi.inspeksireport.ui.home.product;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.delete_product.DeleteProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_user.DeleteUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class ProductViewModel extends BaseViewModel<ProductNavigator> implements ProductItemViewModel.ProductItemViewModelListener {

    private final MutableLiveData<List<ProductItemViewModel>> productItemsLiveData;



    public ProductViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        productItemsLiveData = new MutableLiveData<>();
//        fetchproducts();
    }

    public void fetchproducts() {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);
        getCompositeDisposable().add(getDataManager()
                .getAllProductApiCall()
                .map(productResponse -> productResponse.getListDataProduct())
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(productResponse -> {
                    productItemsLiveData.setValue(productResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public LiveData<List<ProductItemViewModel>> getProductItemsLiveData() {
        return productItemsLiveData;
    }

    private Single<List<ProductItemViewModel>> getViewModelList(List<ProductResponse.DataProduct> repoList) {
        return Observable.fromIterable(repoList)
                .map(repo -> {
                    // masuk
                    return new ProductItemViewModel(
                            repo.getIdProduct(), repo.getNamaProduct(),repo.getStatusProduct(), repo.getCreatedDate(),this);
                }).toList();
    }


    public void deleteProduct(int id_product) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postDeleteProductApiCall(new DeleteProductRequest(id_product))
                .map(deleteProductResponse -> deleteProductResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
//                    setIsLoading(false);
                    getNavigator().isRefresh();
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void toCreateProductClick() {
        getNavigator().openCreateProductActivity();
    }


    public void onBackClick() {
        getNavigator().onBack();
    }


    @Override
    public void onItemClick(int idproduct,String nama) {
                try {
                    getNavigator().openDialog(idproduct,nama,0);
        } catch (Exception e) {
        }

    }
}
