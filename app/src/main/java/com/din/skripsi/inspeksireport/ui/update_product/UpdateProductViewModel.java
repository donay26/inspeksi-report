package com.din.skripsi.inspeksireport.ui.update_product;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.update_product.UpdateProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_user.UpdateUserRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.ui.update_user.UpdateUserNavigator;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class UpdateProductViewModel extends BaseViewModel<UpdateProductNavigator> {


    public UpdateProductViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }
        return true;
    }

    public void updateProduct(int idProduct,String nama) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postUpdateProductApiCall(new UpdateProductRequest(nama,idProduct))
                .map(updateProductResponse -> updateProductResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void onServerUpdateProductClick() {
        getNavigator().updateProduct();
    }
}