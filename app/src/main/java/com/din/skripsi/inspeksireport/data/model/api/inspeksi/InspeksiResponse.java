package com.din.skripsi.inspeksireport.data.model.api.inspeksi;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InspeksiResponse {

    @Expose
    @SerializedName("data")
    private List<DataInspeksi> listDataInspeksi;

    @Expose
    @SerializedName("status_code")
    private boolean statusCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("data_total")
    private DataTotal dataTotal;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InspeksiResponse)) {
            return false;
        }

        InspeksiResponse that = (InspeksiResponse) o;

        if (statusCode != that.statusCode) {
            return false;
        }
        if (!message.equals(that.message)) {
            return false;
        }


        return listDataInspeksi.size() != that.listDataInspeksi.size();
    }

    @Override
    public int hashCode() {

        int result = listDataInspeksi.hashCode();
        result = 31 * result;
        return result;
    }

    public List<DataInspeksi> getListDataInspeksi() {
        return listDataInspeksi;
    }
    public String getMessage(){
        return message;
    }

    public boolean getStatusCode() {
        return statusCode;
    }
    public DataTotal getDataTotal() {
        return dataTotal;
    }

    public void setDataInspeksi(List<DataInspeksi> data) {
        this.listDataInspeksi = data;
    }


    public static class DataInspeksi {

        @Expose
        @SerializedName("id_inspeksi")
        private int id_inspeksi;

        @Expose
        @SerializedName("unique_id_inspeksi")
        private String unique_id_inspeksi;

        @Expose
        @SerializedName("nomor_surat")
        private String nomor_surat;

        @Expose
        @SerializedName("nama_perusahaan")
        private String nama_perusahaan;

        @Expose
        @SerializedName("kode_perusahaan")
        private String kode_perusahaan;

        @Expose
        @SerializedName("image")
        private String image_name;

        @Expose
        @SerializedName("serial_number")
        private String serial_number;

        @Expose
        @SerializedName("note")
        private String note;

        @Expose
        @SerializedName("created_date")
        private String created_date;

        @Expose
        @SerializedName("updated_date")
        private String updated_date;

        @Expose
        @SerializedName("status_report")
        private Integer status_report;

        @Expose
        @SerializedName("alamat_perusahaan")
        private String alamat_perusahaan;

        @Expose
        @SerializedName("id_product")
        private Integer id_product;


        @Expose
        @SerializedName("nama_product")
        private String nama_product;




        public int getIdInspeksi() {
            return id_inspeksi;
        }

        public String getUniqueIdInspeksi() {
            return unique_id_inspeksi;
        }

        public String getNomorSurat() {
            return nomor_surat;
        }

        public String getNamaPerusahaan() {
            return nama_perusahaan;
        }
        public String getKodePerusahaan() {
            return kode_perusahaan;
        }
        public String getImageName() {
            return image_name;
        }

        public String getSerialNumber() {
            return serial_number;
        }

        public String getCreatedDate() {
            return created_date;
        }
        public String getUpdatedDate() {
            return updated_date;
        }

        public Integer getStatusReport() {
            return status_report;
        }

        public String getAlamatPerusahaan() {
            return alamat_perusahaan;
        }

        public Integer getIdProduct() {
            return id_product;
        }

        public String getNamaProduct() {
            return nama_product;
        }
        public String getNote() {
            return note;
        }




    }

    public static class DataTotal {

        @Expose
        @SerializedName("total")
        private int total;


        public int getTotal() {
            return total;
        }




    }

}
