package com.din.skripsi.inspeksireport.ui.update_perusahaan;

public interface UpdatePerusahaanNavigator {

    void handleError(Throwable throwable);

    void updatePerusahaan();

    void openMainActivity();

}
