package com.din.skripsi.inspeksireport.ui.home;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableField;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.CommonUtils;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class MainViewModel extends BaseViewModel<MainNavigator> {


    private static final String TAG = "HomeViewModel";

    public static final int NO_ACTION = -1, ACTION_ADD_ALL = 0, ACTION_DELETE_SINGLE = 1;

    private final ObservableField<String> appVersion = new ObservableField<>();
    
    private final ObservableField<String> userNIK = new ObservableField<>();

    private final ObservableField<String> userUsername = new ObservableField<>();

    private final ObservableField<Integer> userRole = new ObservableField<>();


    private int action = NO_ACTION;

    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
//        questionCardData = new MutableLiveData<>();
//        loadQuestionCards();
    }

    public int getAction() {
        return action;
    }

    public ObservableField<String> getAppVersion() {
        return appVersion;
    }


    public ObservableField<String> getUserNIK() {
        return userNIK;
    }

    public ObservableField<String> getUserName() {
        return userUsername;
    }
    public ObservableField<Integer> getUserRole() {
        return userRole;
    }





    public void onServerLogoutClick() {
        getDataManager().setUserAsLoggedOut();
        getNavigator().openLoginActivity();
    }

    public void onNavMenuCreated() {
        final String currentUserName = getDataManager().getCurrentUserNama();
        if (!TextUtils.isEmpty(currentUserName)) {
            userUsername.set(currentUserName);
        }


        final Integer currentUserrole = getDataManager().getCurrentUserRole();
        userRole.set(currentUserrole);


        final String currentUserNIK = getDataManager().getCurrentUserNIK();
        if (!TextUtils.isEmpty(currentUserNIK)) {
            userNIK.set(currentUserNIK+" - "+CommonUtils.convertRoleString(currentUserrole));
        }

        Log.d("USER",currentUserName);
        Log.d("USER",currentUserNIK);
        Log.d("USER",currentUserrole.toString());

    }

    public void removeQuestionCard() {
        action = ACTION_DELETE_SINGLE;
//        questionCardData.getValue().remove(0);
    }

    public void updateAppVersion(String version) {
        appVersion.set(version);
    }



    public void onServerLoginClick() {
        getNavigator().openLoginActivity();
    }

}
