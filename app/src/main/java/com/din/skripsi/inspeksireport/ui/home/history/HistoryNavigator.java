package com.din.skripsi.inspeksireport.ui.home.history;

public interface HistoryNavigator {
    void handleError(Throwable throwable);
    void openDetail(String idUnique,int idInspeksi, int statusReport, int idProduct, String namaProduct, String nomorSurat, String serialNumber,String namaPerusahaan,String kodePerusahaan, String alamatPerusahaan, String note,String image_name, String createdDate,String updated_date);
    void isSwipeRefresh(boolean swipe);
    void onBack();
}
