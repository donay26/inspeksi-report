package com.din.skripsi.inspeksireport.ui.update_status;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.update_status.UpdateStatusRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.ui.update_status.UpdateStatusNavigator;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class UpdateStatusViewModel extends BaseViewModel<UpdateStatusNavigator> {


    public UpdateStatusViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }
        return true;
    }

    public void updateStatus(int idStatus,String nama) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postUpdateStatusApiCall(new UpdateStatusRequest(nama,idStatus))
                .map(updateStatusResponse -> updateStatusResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void onServerUpdateStatusClick() {
        getNavigator().updateStatus();
    }
}