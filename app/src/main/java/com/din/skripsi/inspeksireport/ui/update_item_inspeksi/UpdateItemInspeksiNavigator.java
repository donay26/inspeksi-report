package com.din.skripsi.inspeksireport.ui.update_item_inspeksi;


import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;

import java.util.List;

public interface UpdateItemInspeksiNavigator {

    void handleError(Throwable throwable);

    void updateInspeksi();

    void openInspeksiActivity();

    void setSpinnerStatus(List<StatusResponse.DataStatus> dataStatuses);

    void setSpinnerRekomendasi(List<RekomendasiResponse.DataRekomendasi> dataRekomendasis);


}
