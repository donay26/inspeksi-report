package com.din.skripsi.inspeksireport.ui.create_perusahaan;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.create_perusahaan.CreatePerusahaanRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.ui.create_perusahaan.CreatePerusahaanNavigator;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class CreatePerusahaanViewModel extends BaseViewModel<CreatePerusahaanNavigator> {



    public CreatePerusahaanViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama,String kode,String alamat) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }

        if (TextUtils.isEmpty(kode)) {
            return false;
        }

        if (TextUtils.isEmpty(alamat)) {
            return false;
        }

        return true;
    }

    public void createPerusahaan(String nama,String kode,String alamat) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postCreatePerusahaanApiCall(new CreatePerusahaanRequest(nama,kode,alamat))
                .map(createPerusahaanResponse -> createPerusahaanResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }




    public void onServerCreatePerusahaanClick() {
        getNavigator().createPerusahaan();
    }


}
