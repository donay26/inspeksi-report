package com.din.skripsi.inspeksireport.data.model.api.update_product;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class UpdateProductRequest extends JSONObject {

    @Expose
    @SerializedName("id_product")
    private Integer id_product;

    @Expose
    @SerializedName("nama")
    private String nama;



    public UpdateProductRequest(String nama,Integer id_product) {

        this.nama = nama;
        this.id_product = id_product;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        UpdateProductRequest that = (UpdateProductRequest) object;



        return nama == "" ? nama == that.nama : that.nama == "";

    }


    @Override
    public int hashCode() {
        int result = nama.hashCode() != 0 ? nama.hashCode() : 0;
        return result;
    }

    public String getNama() {
        return nama;
    }
    public Integer getIdProduct() {
        return id_product;
    }


    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nama", getNama());
            jsonObject.put("id_product", getIdProduct());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, UpdateProductRequest.class);
    }

}
