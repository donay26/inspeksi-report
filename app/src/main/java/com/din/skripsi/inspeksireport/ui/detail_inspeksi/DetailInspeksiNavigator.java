package com.din.skripsi.inspeksireport.ui.detail_inspeksi;

public interface DetailInspeksiNavigator {

    void handleError(Throwable throwable);

    void openCreateItemInspeksi();

    void deleteInspeksi();

    void printInspeksi();

    void openMainActivity();

    void openUpdateInspeksi();

    void refreshListInspeksi();

    void approveInspeksi();

    void rejectInspeksi();

    void pendingInspeksi();

    void doneInspeksi();



    void isSwipeRefresh(boolean swipe);

    void openDialogItemInspeksi(int idItemInspeksi, String nama_product, String rekomendasi,Integer id_rekomendasi ,String remarks, String status,Integer id_status, String created_date);

}
