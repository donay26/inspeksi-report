package com.din.skripsi.inspeksireport.data.model.api.status;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StatusResponse {

    @Expose
    @SerializedName("data")
    private List<DataStatus> listDataStatus;

    @Expose
    @SerializedName("status_code")
    private boolean statusCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StatusResponse)) {
            return false;
        }

        StatusResponse that = (StatusResponse) o;

        if (statusCode != that.statusCode) {
            return false;
        }
        if (!message.equals(that.message)) {
            return false;
        }


        return listDataStatus.size() != that.listDataStatus.size();
    }

    @Override
    public int hashCode() {

        int result = listDataStatus.hashCode();
        result = 31 * result;
        return result;
    }

    public List<DataStatus> getListDataStatus() {
        return listDataStatus;
    }
    public String getMessage(){
        return message;
    }

    public boolean getStatusCode() {
        return statusCode;
    }

    public void setDataStatus(List<DataStatus> data) {
        this.listDataStatus = data;
    }


    public static class DataStatus {

        @Expose
        @SerializedName("id_status")
        private Integer id_status;

        @Expose
        @SerializedName("nama_status")
        private String nama_status;

        @Expose
        @SerializedName("created_date")
        private String created_date;



        public String getNamaStatus() {
            return nama_status;
        }


        public Integer getIdStatus() {
            return id_status;
        }

        public String getCreatedDate() {
            return created_date;
        }




    }
}
