package com.din.skripsi.inspeksireport.ui.create_user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityCreateUserBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.create_inspeksi.SpinnerProductAdapter;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

public class CreateUserActivity extends BaseActivity<ActivityCreateUserBinding, CreateUserViewModel> implements CreateUserNavigator {

    private ActivityCreateUserBinding mActivityCreateUserBinding;


    private String[] germanFeminine = {
            "Staff Lab",
            "Kepala Lab",
            "Officer Lab",
    };
    private Spinner spinnerRole;

    public static Intent newIntent(Context context) {
        return new Intent(context, CreateUserActivity.class);
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_user;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCreateUserBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setSupportActionBar(mActivityCreateUserBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRole();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void createUser() {
        String namaUser = mActivityCreateUserBinding.etNamaUser.getText().toString();
        String nikUser = mActivityCreateUserBinding.etNIK.getText().toString();
        String passwrodUser = mActivityCreateUserBinding.etPassword.getText().toString();
        int role  = CommonUtils.convertRole(mActivityCreateUserBinding.roleSpinner.getSelectedItem().toString());

        if (mViewModel.isValid(namaUser, nikUser,passwrodUser)) {
            hideKeyboard();
            mViewModel.CreateUser(namaUser, nikUser,passwrodUser,role);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }

    private void setRole(){
        spinnerRole = mActivityCreateUserBinding.roleSpinner;
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, germanFeminine);

        // mengeset Array Adapter tersebut ke Spinner
        spinnerRole.setAdapter(adapter);
        spinnerRole.setSelection(0);

        spinnerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                spinnerRole.setSelection(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(CreateUserActivity.this);
        startActivity(intent);
        finish();
    }
}