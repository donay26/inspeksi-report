package com.din.skripsi.inspeksireport.inject.component;


import com.din.skripsi.inspeksireport.inject.module.DialogModule;
import com.din.skripsi.inspeksireport.inject.scope.DialogScope;

import dagger.Component;

@DialogScope
@Component(modules = DialogModule.class, dependencies = AppComponent.class)
public interface DialogComponent {

}
