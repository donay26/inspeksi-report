package com.din.skripsi.inspeksireport.ui.home.status;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.delete_status.DeleteStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class StatusViewModel extends BaseViewModel<StatusNavigator> implements StatusItemViewModel.StatusItemViewModelListener {

    private final MutableLiveData<List<StatusItemViewModel>> productItemsLiveData;



    public StatusViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        productItemsLiveData = new MutableLiveData<>();
//        fetchStatuss();
    }

    public void fetchStatuss() {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);
        getCompositeDisposable().add(getDataManager()
                .getAllStatusApiCall()
                .map(statusResponse -> statusResponse.getListDataStatus())
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(productResponse -> {
                    productItemsLiveData.setValue(productResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public LiveData<List<StatusItemViewModel>> getStatusItemsLiveData() {
        return productItemsLiveData;
    }

    private Single<List<StatusItemViewModel>> getViewModelList(List<StatusResponse.DataStatus> repoList) {
        return Observable.fromIterable(repoList)
                .map(repo -> {
                    // masuk
                    return new StatusItemViewModel(
                            repo.getIdStatus(), repo.getNamaStatus(), repo.getCreatedDate(),this);
                }).toList();
    }


    public void deleteStatus(int id_status) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postDeleteStatusApiCall(new DeleteStatusRequest(id_status))
                .map(deleteStatusResponse -> deleteStatusResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
//                    setIsLoading(false);
                    getNavigator().isRefresh();
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void toCreateStatusClick() {
        getNavigator().openCreateStatusActivity();
    }


    public void onBackClick() {
        getNavigator().onBack();
    }


    @Override
    public void onItemClick(int idproduct,String nama) {
                try {
                    getNavigator().openDialog(idproduct,nama,0);
        } catch (Exception e) {
        }

    }
}
