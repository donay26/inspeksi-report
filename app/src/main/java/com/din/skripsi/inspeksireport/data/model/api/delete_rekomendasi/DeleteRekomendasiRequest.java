package com.din.skripsi.inspeksireport.data.model.api.delete_rekomendasi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class DeleteRekomendasiRequest extends JSONObject {


    @Expose
    @SerializedName("id_rekomendasi")
    private int id_rekomendasi;




    public DeleteRekomendasiRequest(int id_rekomendasi) {
        this.id_rekomendasi = id_rekomendasi;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DeleteRekomendasiRequest that = (DeleteRekomendasiRequest) object;



        return id_rekomendasi == 0 ? id_rekomendasi == that.id_rekomendasi : that.id_rekomendasi == 0;

    }


    @Override
    public int hashCode() {
        int result = id_rekomendasi != 0 ? id_rekomendasi : 0;
        return result;
    }

    public Integer getIdRekomendasi() {
        return id_rekomendasi;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_rekomendasi", getIdRekomendasi());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, DeleteRekomendasiRequest.class);
    }

}
