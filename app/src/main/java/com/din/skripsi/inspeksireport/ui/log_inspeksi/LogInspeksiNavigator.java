package com.din.skripsi.inspeksireport.ui.log_inspeksi;

public interface LogInspeksiNavigator {
    void handleError(Throwable throwable);
    void isRefresh();
    void isSwipeRefresh(boolean swipe);
    void onBack();
}
