package com.din.skripsi.inspeksireport.utils;

import android.content.Context;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.ItemInspeksiAdapter;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.ItemInspeksiItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.dashboard.InspeksiAdapter;
import com.din.skripsi.inspeksireport.ui.home.dashboard.InspeksiItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.history.HistoryAdapter;
import com.din.skripsi.inspeksireport.ui.home.history.HistoryItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanAdapter;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.product.ProductAdapter;
import com.din.skripsi.inspeksireport.ui.home.product.ProductItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.rekomendasi.RekomendasiAdapter;
import com.din.skripsi.inspeksireport.ui.home.rekomendasi.RekomendasiItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.status.StatusAdapter;
import com.din.skripsi.inspeksireport.ui.home.status.StatusItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.user.UserAdapter;
import com.din.skripsi.inspeksireport.ui.home.user.UserItemViewModel;
import com.din.skripsi.inspeksireport.ui.log_inspeksi.LogInspeksiAdapter;
import com.din.skripsi.inspeksireport.ui.log_inspeksi.LogInspeksiItemViewModel;

import java.util.List;

public final class BindingUtils {

    private BindingUtils() {
        // This class is not publicly instantiable
    }

    @BindingAdapter({"adapter"})
    public static void addInspeksiItems(RecyclerView recyclerView, List<InspeksiItemViewModel> candidatList) {
        InspeksiAdapter adapter = (InspeksiAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(candidatList);
        }
    }

    @BindingAdapter({"adapter"})
    public static void addItemInspeksiInspeksiItems(RecyclerView recyclerView, List<ItemInspeksiItemViewModel> candidatList) {
        ItemInspeksiAdapter adapter = (ItemInspeksiAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(candidatList);
        }
    }

    @BindingAdapter({"adapter"})
    public static void addHistoryItems(RecyclerView recyclerView, List<HistoryItemViewModel> candidatList) {
        HistoryAdapter adapter = (HistoryAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(candidatList);
        }
    }

    @BindingAdapter({"adapter"})
    public static void addProductItems(RecyclerView recyclerView, List<ProductItemViewModel> candidatList) {
        ProductAdapter adapter = (ProductAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(candidatList);
        }
    }


    @BindingAdapter({"adapter"})
    public static void addUserItems(RecyclerView recyclerView, List<UserItemViewModel> candidatList) {
        UserAdapter adapter = (UserAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(candidatList);
        }
    }

    @BindingAdapter({"adapter"})
    public static void addStatusItems(RecyclerView recyclerView, List<StatusItemViewModel> candidatList) {
        StatusAdapter adapter = (StatusAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(candidatList);
        }
    }
    @BindingAdapter({"adapter"})
    public static void addRekomendasiItems(RecyclerView recyclerView, List<RekomendasiItemViewModel> candidatList) {
        RekomendasiAdapter adapter = (RekomendasiAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(candidatList);
        }
    }

    @BindingAdapter({"adapter"})
    public static void addPerusahaanItems(RecyclerView recyclerView, List<PerusahaanItemViewModel> candidatList) {
        PerusahaanAdapter adapter = (PerusahaanAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(candidatList);
        }
    }

    @BindingAdapter({"adapter"})
    public static void addLogInspeksiItems(RecyclerView recyclerView, List<LogInspeksiItemViewModel> logInspeksiItemViewModelList) {
        LogInspeksiAdapter adapter = (LogInspeksiAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(logInspeksiItemViewModelList);
        }
    }


    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).into(imageView);
    }


}
