package com.din.skripsi.inspeksireport.ui.home.history;

public class HistoryEmptyItemViewModel {

    private final HistoryEmptyItemViewModelListener mListener;

    public HistoryEmptyItemViewModel(HistoryEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface HistoryEmptyItemViewModelListener {

        void onRetryClick();
    }
}
