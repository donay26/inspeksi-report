package com.din.skripsi.inspeksireport.ui.home.rekomendasi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.FragmentRekomendasiBinding;
import com.din.skripsi.inspeksireport.inject.component.FragmentComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseFragment;
import com.din.skripsi.inspeksireport.ui.create_rekomendasi.CreateRekomendasiActivity;
import com.din.skripsi.inspeksireport.ui.update_rekomendasi.UpdateRekomendasiActivity;

import javax.inject.Inject;

public class RekomendasiFragment extends BaseFragment<FragmentRekomendasiBinding, RekomendasiViewModel> implements RekomendasiNavigator, RekomendasiAdapter.RekomendasiAdapterListener {

    public static final String TAG = "RekomendasiFragment";


    FragmentRekomendasiBinding mFragmentRekomendasiBinding;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    RekomendasiAdapter mRekomendasiAdapter;


    public static RekomendasiFragment newInstance() {
        Bundle args = new Bundle();
        RekomendasiFragment fragment = new RekomendasiFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_rekomendasi;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
        mRekomendasiAdapter.setListener(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentRekomendasiBinding = getViewDataBinding();
        mViewModel.fetchRekomendasis();
        setUp();
    }


    @Override
    public void performDependencyInjection(FragmentComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void openDialog(int idRekomendasi, String nama, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(nama);
        builder.setMessage("Silahkan pilih ?");

        // add the buttons
        builder.setPositiveButton("Update", (dialog, which) -> {
            Intent intent = UpdateRekomendasiActivity.newIntent(getContext(),idRekomendasi,nama);
            startActivityForResult(intent,1);
        });
        builder.setNegativeButton("Delete", (dialog, which) -> {
            dialog.dismiss();
            mViewModel.deleteRekomendasi(idRekomendasi);
        });
        builder.setNeutralButton("Batal",(dialog, which) -> {
            dialog.dismiss();
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void isRefresh() {
        mViewModel.fetchRekomendasis();
    }


    @Override
    public void isSwipeRefresh(boolean swipe) {
        mFragmentRekomendasiBinding.swipeLayout.setRefreshing(swipe);

    }

    @Override
    public void onBack() {

    }

    @Override
    public void openCreateRekomendasiActivity() {
        startActivity(CreateRekomendasiActivity.newIntent(getContext()));

    }

    private void setUp() {
        mFragmentRekomendasiBinding.swipeLayout.setOnRefreshListener(() -> mViewModel.fetchRekomendasis());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentRekomendasiBinding.productRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentRekomendasiBinding.productRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentRekomendasiBinding.productRecyclerView.setAdapter(mRekomendasiAdapter);
    }


    @Override
    public void onRetryClick() {
        mViewModel.fetchRekomendasis();
    }

}
