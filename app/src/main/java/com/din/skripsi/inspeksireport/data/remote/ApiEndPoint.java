/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.din.skripsi.inspeksireport.data.remote;



public final class ApiEndPoint {

    public static final String BASE_URL = "http://192.168.137.1:3000/";
//    public static final String BASE_URL = "https://skripsi-din-mysql.herokuapp.com/";
    public static final String ENDPOINT_LOGIN = BASE_URL+"login";
    public static final String ENDPOINT_GET_ALL_INSPEKSI = BASE_URL+"inspeksis";
    public static final String ENDPOINT_GET_ITEM_INSPEKSI = BASE_URL+"item/inspeksis/";
    public static final String ENDPOINT_GET_ALL_HISTORY = BASE_URL+"inspeksis/history";
    public static final String ENDPOINT_GET_ALL_PRODUCT = BASE_URL+"products";
    public static final String ENDPOINT_GET_ALL_USER = BASE_URL+"users";
    public static final String ENDPOINT_POST_CREATE_INSPEKSI = BASE_URL+"inspeksi";
    public static final String ENDPOINT_POST_APPROVAL_INSPEKSI = BASE_URL+"inspeksi/approval/";
    public static final String ENDPOINT_POST_UPDATE_INSPEKSI = BASE_URL+"inspeksi/";
    public static final String ENDPOINT_POST_DELETE_INSPEKSI = BASE_URL+"inspeksi/";
    public static final String ENDPOINT_POST_CREATE_ITEM_INSPEKSI = BASE_URL+"item/inspeksi";
    public static final String ENDPOINT_POST_UPDATE_ITEM_INSPEKSI = BASE_URL+"item/inspeksi/";
    public static final String ENDPOINT_POST_DELETE_ITEM_INSPEKSI = BASE_URL+"item/inspeksi/";
    public static final String ENDPOINT_POST_CREATE_PRODUCT = BASE_URL+"product/";
    public static final String ENDPOINT_POST_UPDATE_PRODUCT = BASE_URL+"product/";
    public static final String ENDPOINT_POST_DELETE_PRODUCT = BASE_URL+"product/";
    public static final String ENDPOINT_POST_CREATE_USER = BASE_URL+"users";
    public static final String ENDPOINT_POST_UPDATE_USER = BASE_URL+"user/";
    public static final String ENDPOINT_POST_DELETE_USER = BASE_URL+"user/";
    public static final String ENDPOINT_GET_ALL_REKOMENDASI = BASE_URL+"rekomendasis";
    public static final String ENDPOINT_POST_CREATE_REKOMENDASI = BASE_URL+"rekomendasi";
    public static final String ENDPOINT_POST_UPDATE_REKOMENDASI = BASE_URL+"rekomendasi/";
    public static final String ENDPOINT_POST_DELETE_REKOMENDASI = BASE_URL+"rekomendasi/";
    public static final String ENDPOINT_GET_ALL_STATUS = BASE_URL+"statuss";
    public static final String ENDPOINT_POST_CREATE_STATUS = BASE_URL+"status";
    public static final String ENDPOINT_POST_UPDATE_STATUS = BASE_URL+"status/";
    public static final String ENDPOINT_POST_DELETE_STATUS = BASE_URL+"status/";
    public static final String ENDPOINT_GET_ALL_PERUSAHAAN = BASE_URL+"perusahaans";
    public static final String ENDPOINT_POST_CREATE_PERUSAHAAN = BASE_URL+"perusahaan";
    public static final String ENDPOINT_POST_UPDATE_PERUSAHAAN = BASE_URL+"perusahaan/";
    public static final String ENDPOINT_POST_DELETE_PERUSAHAAN = BASE_URL+"perusahaan/";

    public static final String ENDPOINT_GET_LOG_INSPEKSI = BASE_URL+"log_inspeksis";
    public static final String ENDPOINT_GET_MASTER = BASE_URL+"master";


    private ApiEndPoint() {
        // This class is not publicly instantiable
    }
}
