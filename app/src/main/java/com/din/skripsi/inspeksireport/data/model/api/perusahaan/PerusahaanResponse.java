package com.din.skripsi.inspeksireport.data.model.api.perusahaan;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PerusahaanResponse {

    @Expose
    @SerializedName("data")
    private List<DataPerusahaan> listDataPerusahaan;

    @Expose
    @SerializedName("status_code")
    private boolean statusCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PerusahaanResponse)) {
            return false;
        }

        PerusahaanResponse that = (PerusahaanResponse) o;

        if (statusCode != that.statusCode) {
            return false;
        }
        if (!message.equals(that.message)) {
            return false;
        }


        return listDataPerusahaan.size() != that.listDataPerusahaan.size();
    }

    @Override
    public int hashCode() {

        int result = listDataPerusahaan.hashCode();
        result = 31 * result;
        return result;
    }

    public List<DataPerusahaan> getListDataPerusahaan() {
        return listDataPerusahaan;
    }
    public String getMessage(){
        return message;
    }

    public boolean getStatusCode() {
        return statusCode;
    }

    public void setDataPerusahaan(List<DataPerusahaan> data) {
        this.listDataPerusahaan = data;
    }


    public static class DataPerusahaan {

        @Expose
        @SerializedName("id_perusahaan")
        private Integer id_perusahaan;

        @Expose
        @SerializedName("nama_perusahaan")
        private String nama_perusahaan;
        
        @Expose
        @SerializedName("kode_perusahaan")
        private String kode_perusahaan;

        @Expose
        @SerializedName("alamat_perusahaan")
        private String alamat_perusahaan;

        @Expose
        @SerializedName("created_date")
        private String created_date;



        public String getNamaPerusahaan() {
            return nama_perusahaan;
        }

        public String getAlamatPerusahaan() {
            return alamat_perusahaan;
        }

        public String getKodePerusahaan() {
            return kode_perusahaan;
        }


        public Integer getIdPerusahaan() {
            return id_perusahaan;
        }

        public String getCreatedDate() {
            return created_date;
        }




    }
}
