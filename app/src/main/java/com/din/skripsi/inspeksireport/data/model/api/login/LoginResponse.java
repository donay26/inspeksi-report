/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.din.skripsi.inspeksireport.data.model.api.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @Expose
    @SerializedName("data")
    private DataLogin dataLogin;

    @Expose
    @SerializedName("status_code")
    private String statusCode;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoginResponse)) {
            return false;
        }

        LoginResponse that = (LoginResponse) o;

        if (!statusCode.equals(that.statusCode)) {
            return false;
        }

        return dataLogin.equals(that.dataLogin);
    }

    @Override
    public int hashCode() {
        int result = statusCode.hashCode();
        result = 31 * result + dataLogin.hashCode();
        return result;
    }

    public DataLogin getData() {
        return dataLogin;
    }

    public String getStatusCode() {
        return statusCode;
    }


    public static class DataLogin {

        @Expose
        @SerializedName("id_user")
        private Integer id_user;

        @Expose
        @SerializedName("nama")
        private String nama;

        @Expose
        @SerializedName("nik")
        private String nik;

        @Expose
        @SerializedName("role")
        private Integer role;

        @Expose
        @SerializedName("status")
        private Integer status;


        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof DataLogin)) {
                return false;
            }

            DataLogin dataLogin = (DataLogin) o;

            if (id_user != dataLogin.id_user) {
                return false;
            }
            if (!nama.equals(dataLogin.nama)) {
                return false;
            }
            if (!nik.equals(dataLogin.nik)) {
                return false;
            }
            if (role != dataLogin.role) {
                return false;
            }
            if (status != dataLogin.status) {
                return false;
            }


            return dataLogin.equals(dataLogin.id_user);
        }

        @Override
        public int hashCode() {
            int result = id_user != null ? id_user.hashCode() : 0;
            result = 31 * result + (nama != null ? nama.hashCode() : 0);
            result = 31 * result + (id_user != null ? id_user.hashCode() : 0);
            result = 31 * result + (nik != null ? nik.hashCode() : 0);
            result = 31 * result + (role != null ? role.hashCode() : 0);
            result = 31 * result + (status != null ? status.hashCode() : 0);
            return result;
        }

        public String getNIK() {
            return nik;
        }
        public String getNama() {
            return nama;
        }
        public Integer getRole() {
            return role;
        }
        public Integer getStatus() {
            return status;
        }
        public Integer getIdUser() {
            return id_user;
        }

    }
}
