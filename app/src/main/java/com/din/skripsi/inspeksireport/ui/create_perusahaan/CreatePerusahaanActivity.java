package com.din.skripsi.inspeksireport.ui.create_perusahaan;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityCreatePerusahaanBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.create_perusahaan.CreatePerusahaanNavigator;
import com.din.skripsi.inspeksireport.ui.create_perusahaan.CreatePerusahaanViewModel;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;

public class CreatePerusahaanActivity extends BaseActivity<ActivityCreatePerusahaanBinding, CreatePerusahaanViewModel> implements CreatePerusahaanNavigator {

    private ActivityCreatePerusahaanBinding mActivityCreatePerusahaanBinding;



    public static Intent newIntent(Context context) {
        return new Intent(context, CreatePerusahaanActivity.class);
    }




    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_perusahaan;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCreatePerusahaanBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setSupportActionBar(mActivityCreatePerusahaanBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void createPerusahaan() {
        String namaPerusahaan = mActivityCreatePerusahaanBinding.etNama.getText().toString();
        String kodePerusahaan = mActivityCreatePerusahaanBinding.etKode.getText().toString();
        String alamatPerusahaan = mActivityCreatePerusahaanBinding.etAlamat.getText().toString();

        if (mViewModel.isValid(namaPerusahaan,kodePerusahaan,alamatPerusahaan)) {
            hideKeyboard();
            mViewModel.createPerusahaan(namaPerusahaan,kodePerusahaan,alamatPerusahaan);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(CreatePerusahaanActivity.this);
        startActivity(intent);
        finish();
    }


}