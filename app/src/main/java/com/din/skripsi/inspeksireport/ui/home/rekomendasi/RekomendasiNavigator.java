package com.din.skripsi.inspeksireport.ui.home.rekomendasi;

public interface RekomendasiNavigator {
    void handleError(Throwable throwable);
    void openDialog(int idProduct, String nama, int i);
    void isRefresh();
    void isSwipeRefresh(boolean swipe);
    void onBack();
    void openCreateRekomendasiActivity();

}

