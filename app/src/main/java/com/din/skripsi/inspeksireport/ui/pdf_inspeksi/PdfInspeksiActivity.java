package com.din.skripsi.inspeksireport.ui.pdf_inspeksi;

import androidx.annotation.Nullable;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.data.model.api.item_inspeksi.ItemInspeksiResponse;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.ItemInspeksiItemViewModel;
import com.din.skripsi.inspeksireport.utils.CommonUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tejpratapsingh.pdfcreator.activity.PDFCreatorActivity;
import com.tejpratapsingh.pdfcreator.utils.PDFUtil;
import com.tejpratapsingh.pdfcreator.views.PDFBody;
import com.tejpratapsingh.pdfcreator.views.PDFFooterView;
import com.tejpratapsingh.pdfcreator.views.PDFHeaderView;
import com.tejpratapsingh.pdfcreator.views.PDFTableView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFImageView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFLineSeparatorView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFTextView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFVerticalView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PdfInspeksiActivity extends PDFCreatorActivity {

    private static final String PREF_KEY_ID_INSPEKSI = "PREF_KEY_CURRENT_ID_INSPEKSI";

    private static final String PREF_KEY_ID_UNIQUE_INSPEKSI = "PREF_KEY_CURRENT_ID_UNIQUE_INSPEKSI";

    private static final String PREF_KEY_NOMOR_SURAT = "PREF_KEY_CURRENT_ID_NOMOR_SURAT";

    private static final String PREF_KEY_ID_PRODUCT = "PREF_KEY_CURRENT_ID_PRODUCT";

    private static final String PREF_KEY_NAMA_PRODUCT = "PREF_KEY_CURRENT_NAMA_PRODUCT";

    private static final String PREF_KEY_NAMA_PERUSAHAAN = "PREF_KEY_CURRENT_NAMA_PERUSAHAAN";

    private static final String PREF_KEY_KODE_PERUSAHAAN = "PREF_KEY_CURRENT_KODE_PERUSAHAAN";

    private static final String PREF_KEY_SERIAL_NUMBER = "PREF_KEY_CURRENT_SERIAL_NUMBER";

    private static final String PREF_KEY_NOTE = "PREF_KEY_CURRENT_NOTE";

    private static final String PREF_KEY_STATUS_REPORT = "PREF_KEY_CURRENT_STATUS_REPORT";

    private static final String PREF_KEY_ALAMAT_PERUSAHAAN = "PREF_KEY_CURRENT_ALAMAT_PERUSAHAAN";

    private static final String PREF_KEY_CREATED_DATE = "PREF_KEY_CURRENT_CREATED_DATE";

    private static final String PREF_KEY_UPDATED_DATE = "PREF_KEY_CURRENT_UPDATED_DATE";

    private static final String PREF_KEY_IMAGE_NAME = "PREF_KEY_CURRENT_IMAGE_NAME";


    public static Intent newIntent(Context context,
                                   String idUniqueInspeksi,
                                   int idInspeksi,
                                   int statusReport,
                                   int idProduct,
                                   String namaProduct,
                                   String nomorSurat,
                                   String namaPerusahaan,
                                   String kodePerusahaan,
                                   String serialNumber,
                                   String note,
                                   String alamatPerusahaan,
                                   String createDate,
                                   String updateDate,
                                   String imageName, String dataItemInspeksiArrayList) {

        Intent intent = new Intent(context, PdfInspeksiActivity.class);
        intent.putExtra(PREF_KEY_ID_UNIQUE_INSPEKSI, idUniqueInspeksi);
        intent.putExtra(PREF_KEY_ID_INSPEKSI, idInspeksi);
        intent.putExtra(PREF_KEY_ID_PRODUCT, idProduct);
        intent.putExtra(PREF_KEY_NAMA_PRODUCT, namaProduct);
        intent.putExtra(PREF_KEY_NOMOR_SURAT, nomorSurat);
        intent.putExtra(PREF_KEY_NAMA_PERUSAHAAN, namaPerusahaan);
        intent.putExtra(PREF_KEY_KODE_PERUSAHAAN, kodePerusahaan);
        intent.putExtra(PREF_KEY_SERIAL_NUMBER, serialNumber);
        intent.putExtra(PREF_KEY_NOTE, note);
        intent.putExtra(PREF_KEY_STATUS_REPORT, statusReport);
        intent.putExtra(PREF_KEY_ALAMAT_PERUSAHAAN, alamatPerusahaan);
        intent.putExtra(PREF_KEY_CREATED_DATE, createDate);
        intent.putExtra(PREF_KEY_UPDATED_DATE, updateDate);
        intent.putExtra(PREF_KEY_IMAGE_NAME, imageName);
        intent.putExtra("JSON",dataItemInspeksiArrayList);
        return intent;
    }

    public String getListItemInspeksi() throws JSONException {
        Intent intent = getIntent();
        String args = intent.getStringExtra("JSON");
        Log.d("data", args.toString());
        return args;
    }
    public String getPrefKeyIdUniqueInspeksi() {
        Bundle intent = getIntent().getExtras();
        String id_unique_inspeksi = intent.getString(PREF_KEY_ID_UNIQUE_INSPEKSI );
        return id_unique_inspeksi;
    }

    public Integer getPrefKeyIdInspeksi() {
        Bundle intent = getIntent().getExtras();
        Integer id_inspeksi = intent.getInt(PREF_KEY_ID_INSPEKSI, 0);
        return id_inspeksi;
    }

    public Integer getPrefKeyIdProduct() {
        Bundle intent = getIntent().getExtras();
        Integer id_product = intent.getInt(PREF_KEY_ID_PRODUCT, 0);
        return id_product;
    }


    public String getPrefKeyNamaProduct() {
        Bundle intent = getIntent().getExtras();
        String nama_product = intent.getString(PREF_KEY_NAMA_PRODUCT);
        return nama_product;
    }

    public String getPrefKeyNomorSurat() {
        Bundle intent = getIntent().getExtras();
        String nomor_surat = intent.getString(PREF_KEY_NOMOR_SURAT);
        return nomor_surat;
    }

    public String getPrefKeyNamaPerusahaan() {
        Bundle intent = getIntent().getExtras();
        String nama_perusahaan = intent.getString(PREF_KEY_NAMA_PERUSAHAAN);
        return nama_perusahaan;
    }
    public String getPrefKeyKodePerusahaan() {
        Bundle intent = getIntent().getExtras();
        String kode_perusahaan = intent.getString(PREF_KEY_KODE_PERUSAHAAN);
        return kode_perusahaan;
    }
    public String getPrefKeyImageName() {
        Bundle intent = getIntent().getExtras();
        String image_name = intent.getString(PREF_KEY_IMAGE_NAME);
        return image_name;
    }

    public String getPrefKeyAlamatPerusahaan() {
        Bundle intent = getIntent().getExtras();
        String alamat_perusahaan = intent.getString(PREF_KEY_ALAMAT_PERUSAHAAN);
        return alamat_perusahaan;
    }

    public String getPrefKeySerialNumber() {
        Bundle intent = getIntent().getExtras();
        String serial_number = intent.getString(PREF_KEY_SERIAL_NUMBER);
        return serial_number;
    }

    public String getPrefKeyNote() {
        Bundle intent = getIntent().getExtras();
        String note = intent.getString(PREF_KEY_NOTE);
        return note;
    }

    public String getPrefKeyCreatedDate() {
        Bundle intent = getIntent().getExtras();
        String created_date = intent.getString(PREF_KEY_CREATED_DATE);
        return created_date;
    }

    public String getPrefKeyUpdatedDate() {
        Bundle intent = getIntent().getExtras();
        String update_date = intent.getString(PREF_KEY_UPDATED_DATE);
        return update_date;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
//            getSupportActionBar().hide();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("PRINT INSPEKSI");

        }
        createPDF(getPrefKeyIdUniqueInspeksi(), new PDFUtil.PDFUtilListener() {
            @Override
            public void pdfGenerationSuccess(File savedPDFFile) {
                Toast.makeText(PdfInspeksiActivity.this, "PDF Created", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void pdfGenerationFailure(Exception exception) {
                Toast.makeText(PdfInspeksiActivity.this, "PDF NOT Created", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected PDFHeaderView getHeaderView(int forPage) {
        PDFHeaderView headerView = new PDFHeaderView(getApplicationContext());

        PDFVerticalView verticalViewView = new PDFVerticalView(getApplicationContext());
        verticalViewView.getView().setGravity(Gravity.RIGHT);
//        PDFTextView pdfTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.HEADER);
//        SpannableString word = new SpannableString("INSPECTION REPORT");
//        word.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        pdfTextView.setText(word);
//        pdfTextView.setLayout(new LinearLayout.LayoutParams(
//                0,
//                LinearLayout.LayoutParams.MATCH_PARENT, 1));
//        pdfTextView.getView().setGravity(Gravity.CENTER_VERTICAL);
//        pdfTextView.getView().setTypeface(pdfTextView.getView().getTypeface(), Typeface.BOLD);
//
//        horizontalView.addView(pdfTextView);

        PDFImageView imageView = new PDFImageView(getApplicationContext());
        LinearLayout.LayoutParams imageLayoutParam = new LinearLayout.LayoutParams(
                60,
                60, 1);
        imageView.setImageScale(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageResource(R.mipmap.ic_launcher);

        imageLayoutParam.setMargins(0, 0, 10, 0);
        imageView.setLayout(imageLayoutParam);
        verticalViewView.addView(imageView);

        PDFTextView pdfCompanyNameView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        pdfCompanyNameView.setText("PT Saka Tunggal Mandiri Jaya");
        pdfCompanyNameView.getView().setGravity(Gravity.RIGHT);
        pdfCompanyNameView.setTextColor(Color.RED);
        pdfCompanyNameView.setLayout(new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.WRAP_CONTENT, 0));


        verticalViewView.addView(pdfCompanyNameView);
        PDFLineSeparatorView lineSeparatorView1 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        verticalViewView.addView(lineSeparatorView1);

        PDFTextView pdfSloganView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.SMALL);
        pdfSloganView.setText("DISTRIBUTOR OF GAS DETECTION, SAFETY EQUIPMENT AND FIRE EXTINGUISHER");
        pdfSloganView.getView().setGravity(Gravity.RIGHT);
        pdfSloganView.setLayout(new LinearLayout.LayoutParams(

                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0));

        verticalViewView.addView(pdfSloganView);


        headerView.addView(verticalViewView);


        PDFLineSeparatorView lineSeparatorView2 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        lineSeparatorView2.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                8, 0));

        headerView.addView(lineSeparatorView2);

        return headerView;
    }

    @Override
    protected PDFBody getBodyViews() {
        PDFBody pdfBody = new PDFBody();


        PDFLineSeparatorView lineSeparatorView = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        pdfBody.addView(lineSeparatorView);
        PDFTextView pdfTitleTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.HEADER);
        SpannableString word = new SpannableString("INSPECTION REPORT");
        word.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        pdfTitleTextView.setText(word);
        pdfTitleTextView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        pdfTitleTextView.getView().setGravity(Gravity.CENTER);
        pdfTitleTextView.getView().setTypeface(pdfTitleTextView.getView().getTypeface(), Typeface.BOLD);
        pdfBody.addView(pdfTitleTextView);
        lineSeparatorView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                0, 0));



        PDFLineSeparatorView lineSeparatorNomorSuratView = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        pdfBody.addView(lineSeparatorNomorSuratView);
        lineSeparatorNomorSuratView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                10, 0));

        PDFTextView pdfNomorSuratTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        pdfNomorSuratTextView.setText(getPrefKeyNomorSurat());
        pdfNomorSuratTextView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        pdfNomorSuratTextView.getView().setGravity(Gravity.CENTER);
        pdfNomorSuratTextView.getView().setTypeface(pdfNomorSuratTextView.getView().getTypeface(), Typeface.NORMAL);
        pdfBody.addView(pdfNomorSuratTextView);


        PDFLineSeparatorView lineSeparatorToView = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        pdfBody.addView(lineSeparatorToView);
        lineSeparatorToView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                15, 0));

        PDFTextView pdfToTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        pdfToTextView.setText("To");
        pdfToTextView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        pdfToTextView.getView().setGravity(Gravity.CENTER);
        pdfToTextView.getView().setTypeface(pdfToTextView.getView().getTypeface(), Typeface.NORMAL);
        pdfBody.addView(pdfToTextView);


        PDFLineSeparatorView lineSeparatorPerusahaanView = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        pdfBody.addView(lineSeparatorPerusahaanView);
        lineSeparatorPerusahaanView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                0, 0));

        PDFTextView pdfPerusahaanTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
        pdfPerusahaanTextView.setText(getPrefKeyNamaPerusahaan());
        pdfPerusahaanTextView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        pdfPerusahaanTextView.getView().setGravity(Gravity.CENTER);
        pdfPerusahaanTextView.getView().setTypeface(pdfPerusahaanTextView.getView().getTypeface(), Typeface.NORMAL);
        pdfBody.addView(pdfPerusahaanTextView);


        PDFLineSeparatorView lineSeparatorView1 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        pdfBody.addView(lineSeparatorView1);
        lineSeparatorView1.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                20, 0));

        PDFTextView pdfProductView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
        pdfProductView.setText("Nama Product : "+getPrefKeyNamaProduct());
        pdfBody.addView(pdfProductView);

        PDFLineSeparatorView lineSeparatorView2 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        lineSeparatorView2.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                2, 0));
        pdfBody.addView(lineSeparatorView2);

        ///

        PDFTextView pdfSerialNumberView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
        pdfSerialNumberView.setText("Serial Number : "+getPrefKeySerialNumber());
        pdfBody.addView(pdfSerialNumberView);


        ///

        PDFTextView pdfProductViewDate = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
        pdfProductViewDate.setText("Create Date : "+ CommonUtils.convertTimeStamp(getPrefKeyUpdatedDate()));
        pdfBody.addView(pdfProductViewDate);

        PDFLineSeparatorView lineSeparatorViewDate = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        lineSeparatorViewDate.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                2, 0));
        pdfBody.addView(lineSeparatorViewDate);

        ///

        PDFTextView pdfProductViewUpdateDate = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
        pdfProductViewUpdateDate.setText("Valid Date : "+ CommonUtils.convertTwoWeekTimeStamp(getPrefKeyUpdatedDate()));
        pdfBody.addView(pdfProductViewUpdateDate);

        PDFLineSeparatorView lineSeparatorViewUpdateDate = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        lineSeparatorViewUpdateDate.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                2, 0));
        pdfBody.addView(lineSeparatorViewUpdateDate);




        PDFLineSeparatorView lineSeparatorView3 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        pdfBody.addView(lineSeparatorView3);

        PDFLineSeparatorView lineSeparatorView4 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        lineSeparatorView4.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                10, 0));

        pdfBody.addView(lineSeparatorView4);

        try {
            JsonObject jobj = new Gson().fromJson(getListItemInspeksi(), JsonObject.class);
            JsonArray dataArray = jobj.getAsJsonArray("data");
            int[] widthPercent = {40, 10, 30, 20}; // Sum should be equal to 100%
            String[] textInTable = {"Inspected Item", "Status", "Recommendation", "Remarks"};


            PDFTableView.PDFTableRowView tableHeader = new PDFTableView.PDFTableRowView(getApplicationContext());

            for (String s : textInTable) {
                PDFTextView pdfTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
                pdfTextView.setText( s);
                tableHeader.addToRow(pdfTextView);
            }

            PDFTableView.PDFTableRowView tableRowView1 = new PDFTableView.PDFTableRowView(getApplicationContext());
//            for (String s : textInTable) {
//                PDFTextView pdfTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
//                pdfTextView.setText("Row 1 : " + s);
//                tableRowView1.addToRow(pdfTextView);
//            }

            PDFTableView tableView = new PDFTableView(getApplicationContext(), tableHeader, tableRowView1);
            Gson gson = new Gson();

            ItemInspeksiResponse.DataItemInspeksi[] dataArrays = gson.fromJson(dataArray, ItemInspeksiResponse.DataItemInspeksi[].class);

//
            for (ItemInspeksiResponse.DataItemInspeksi dataItemInspeksi : dataArrays) {


                // Create 10 rows
                PDFTableView.PDFTableRowView tableRowView = new PDFTableView.PDFTableRowView(getApplicationContext());
//                for (String s : textInTable) {
                    PDFTextView pdfTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
                    pdfTextView.setText(dataItemInspeksi.getNama());
                    tableRowView.addToRow(pdfTextView);
//                }
                ///
                PDFTextView pdfTextView2 = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
                pdfTextView2.setText(dataItemInspeksi.getStatus());
                tableRowView.addToRow(pdfTextView2);
                ///
                PDFTextView pdfTextView3 = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
                pdfTextView3.setText(dataItemInspeksi.getRekomendasi());
                tableRowView.addToRow(pdfTextView3);
                ///
                PDFTextView pdfTextView4 = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
                pdfTextView4.setText(dataItemInspeksi.getRemarks());
                tableRowView.addToRow(pdfTextView4);


                tableView.addRow(tableRowView);





            }
            tableView.setColumnWidth(widthPercent);
            pdfBody.addView(tableView);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        PDFLineSeparatorView lineNoteView = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.BLACK);
        lineNoteView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                2, 0));
        pdfBody.addView(lineNoteView);

        PDFVerticalView verticalView = new PDFVerticalView(getApplicationContext());
        LinearLayout noteLayout = new LinearLayout(getApplicationContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                100
        );
        params.setMargins(0,20,0,10);
        noteLayout.setLayoutParams(params);
        noteLayout.setOrientation(LinearLayout.VERTICAL);
        noteLayout.setBackground(getResources().getDrawable(R.drawable.border));
        verticalView.setView(noteLayout);
        PDFTextView pdfNoteView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P)
                .setText("Note : ");
        pdfNoteView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0));
        pdfNoteView.setPadding(5,0,0,0);
        verticalView.addView(pdfNoteView);


        PDFTextView pdfNoteValueView2 = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P)
                .setText(getPrefKeyNote());
        pdfNoteValueView2.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0));
        pdfNoteValueView2.setPadding(5,0,0,0);
        verticalView.addView(pdfNoteValueView2);
        pdfBody.addView(verticalView);




        return pdfBody;
    }

    @Override
    protected PDFFooterView getFooterView(int forPage) {
        PDFFooterView footerView = new PDFFooterView(getApplicationContext());

        PDFVerticalView verticalttdView = new PDFVerticalView(getApplicationContext());

        PDFTextView pdfttdView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P)
                .setText("Authorized Signature");
        pdfttdView.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0));
        pdfttdView.setPadding(0,5,0,10);
        verticalttdView.addView(pdfttdView);


        PDFTextView pdfttdValueView2 = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P)
                .setText("Head Manager");
        pdfttdValueView2.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0));
        pdfttdValueView2.setPadding(0,15,0,10);
        verticalttdView.addView(pdfttdValueView2);

        footerView.addView(verticalttdView);


        PDFTextView pdfTextViewPage = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.SMALL);
        pdfTextViewPage.setText(String.format(Locale.getDefault(), "Page: %d", forPage + 1));
        pdfTextViewPage.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0));
        pdfTextViewPage.getView().setGravity(Gravity.RIGHT);

        footerView.addView(pdfTextViewPage);

        return footerView;
    }

    @Nullable
    @Override
    protected PDFImageView getWatermarkView(int forPage) {
        PDFImageView pdfImageView = new PDFImageView(getApplicationContext());
        FrameLayout.LayoutParams childLayoutParams = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                200, Gravity.CENTER);
        pdfImageView.setLayout(childLayoutParams);

        pdfImageView.setImageResource(R.mipmap.ic_launcher_foreground);
        pdfImageView.setImageScale(ImageView.ScaleType.FIT_CENTER);
        pdfImageView.getView().setAlpha(0.3F);

        return pdfImageView;
    }

    @Override
    protected void onNextClicked(File savedPDFFile) {
        Uri pdfUri = Uri.fromFile(savedPDFFile);

        Intent intentPdfViewer = new Intent(PdfInspeksiActivity.this, PdfInspeksiViewerActivity.class);
        intentPdfViewer.putExtra(PdfInspeksiViewerActivity.PDF_FILE_URI, pdfUri);

        startActivity(intentPdfViewer);
    }
}