package com.din.skripsi.inspeksireport.ui.home.perusahaan;

public interface PerusahaanNavigator {
    void handleError(Throwable throwable);
    void openDialog(int idPerusahaan, String nama_perusahaan,String kode_perusahaan,String alamat_perusahaan);
    void isRefresh();
    void isSwipeRefresh(boolean swipe);
    void onBack();
    void openCreatePerusahaanActivity();

}

