package com.din.skripsi.inspeksireport.ui.home.product;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.FragmentProductBinding;
import com.din.skripsi.inspeksireport.inject.component.FragmentComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseFragment;
import com.din.skripsi.inspeksireport.ui.create_product.CreateProductActivity;
import com.din.skripsi.inspeksireport.ui.update_product.UpdateProductActivity;
import com.din.skripsi.inspeksireport.ui.update_user.UpdateUserActivity;

import javax.inject.Inject;

public class ProductFragment extends BaseFragment<FragmentProductBinding, ProductViewModel> implements ProductNavigator, ProductAdapter.ProductAdapterListener {

    public static final String TAG = "ProductFragment";


    FragmentProductBinding mFragmentProductBinding;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    ProductAdapter mProductAdapter;


    public static ProductFragment newInstance() {
        Bundle args = new Bundle();
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_product;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
        mProductAdapter.setListener(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentProductBinding = getViewDataBinding();
        mViewModel.fetchproducts();
        setUp();
    }


    @Override
    public void performDependencyInjection(FragmentComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void openDialog(int idProduct, String nama, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(nama);
        builder.setMessage("Silahkan pilih ?");

        // add the buttons
        builder.setPositiveButton("Update", (dialog, which) -> {
            Intent intent = UpdateProductActivity.newIntent(getContext(),idProduct,nama);
            startActivityForResult(intent,1);
        });
        builder.setNegativeButton("Delete", (dialog, which) -> {
            dialog.dismiss();
            mViewModel.deleteProduct(idProduct);
        });
        builder.setNeutralButton("Batal",(dialog, which) -> {
            dialog.dismiss();
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void isRefresh() {
        mViewModel.fetchproducts();
    }


    @Override
    public void isSwipeRefresh(boolean swipe) {
        mFragmentProductBinding.swipeLayout.setRefreshing(swipe);

    }

    @Override
    public void onBack() {

    }

    @Override
    public void openCreateProductActivity() {
        startActivity(CreateProductActivity.newIntent(getContext()));

    }

    private void setUp() {
        mFragmentProductBinding.swipeLayout.setOnRefreshListener(() -> mViewModel.fetchproducts());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentProductBinding.productRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentProductBinding.productRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentProductBinding.productRecyclerView.setAdapter(mProductAdapter);
    }


    @Override
    public void onRetryClick() {
        mViewModel.fetchproducts();
    }
}
