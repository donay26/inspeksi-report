package com.din.skripsi.inspeksireport.ui.home.user;


import androidx.databinding.ObservableField;

public class UserItemViewModel {

    public final ObservableField<Integer> _idUser = new ObservableField<>();
    public final ObservableField<String> _namaUser = new ObservableField<>();
    public final ObservableField<Integer> _statusUser = new ObservableField<>();
    public final ObservableField<Integer> _roleUser = new ObservableField<>();
    public final ObservableField<String> _nikUser = new ObservableField<>();
    public final ObservableField<String> _passwordUser = new ObservableField<>();
    public final ObservableField<String> _createdDateUser = new ObservableField<>();

    public final UserItemViewModelListener mListener;

    public UserItemViewModel(int idUser, String nama_user, String nik_user,int status_user,int role_user,String password_user, String created_date, UserItemViewModelListener listener) {
        this.mListener = listener;
        this._idUser.set(idUser);
        this._namaUser.set(nama_user);
        this._statusUser.set(status_user);
        this._roleUser.set(role_user);
        this._nikUser.set(nik_user);
        this._passwordUser.set(password_user);
        this._createdDateUser.set(created_date);
    }

    public void onItemClick() {
        mListener.onItemClick(_idUser.get(),_namaUser.get(),_statusUser.get(),_roleUser.get(),_nikUser.get(),_passwordUser.get(),_createdDateUser.get());
    }

    public interface UserItemViewModelListener {

        void onItemClick(int idUser,String nama,int status,int roleUser,String nik,String password,String createdDate);
    }

}
