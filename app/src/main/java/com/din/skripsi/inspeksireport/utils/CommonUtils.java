package com.din.skripsi.inspeksireport.utils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;

import com.din.skripsi.inspeksireport.R;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
    private static final String TAG = "CommonUtils";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName)
            throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public static String getTimeStamp() {
        return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.US).format(new Date());
    }

    public static String convertTwoWeekTimeStamp(String text) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;
        try {
            d = input.parse(text);
            int noOfDays = 14; //i.e two weeks
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);
            calendar.add(Calendar.DAY_OF_YEAR, noOfDays);
            d = calendar.getTime();
            String formatted = output.format(d);
            return formatted;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String convertTimeStamp(String text) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;
        try {
            d = input.parse(text);
            String formatted = output.format(d);
            return formatted;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }


    public static String convertStatus(int status) {
        String statusCondition = "";
        if (status == 0) {
            statusCondition = "NEW";
        } else if (status == 1) {
            statusCondition = "APPROVE";
        } else if (status == 2) {
            statusCondition = "REJECT";
        } else if (status == 3) {
            statusCondition = "PENDING";
        } else if (status == 4) {
            statusCondition = "DONE";
        } else {
            statusCondition = "NEW";
        }


        return statusCondition;
    }


    public static int convertRole(String role) {
        int roleUser = 0;
        if (role.equalsIgnoreCase("Kepala Lab")) {
            roleUser = 1;
        } else if (role.equalsIgnoreCase("Staff Lab")) {
            roleUser = 2;
        } else if (role.equalsIgnoreCase("Officer Lab")) {
            roleUser = 3;
        }

        return roleUser;
    }


    public static String convertRoleString(int role) {
        String roleUser = "";
        if (role == 1) {
            roleUser = "Kepala Lab";
        } else if (role == 2) {
            roleUser = "Staff Lab";
        } else if (role == 3) {
            roleUser = "Officer Lab";
        }else if (role == 0) {
            roleUser = "Super Admin";
        }

        return roleUser;
    }

    public static String converRomawi(int month) {
        String romawi = "";
        if (month == 1) {
            romawi = "I";
        } else if (month == 2) {
            romawi = "II";
        } else if (month == 3) {
            romawi = "III";
        } else if (month == 4) {
            romawi = "IV";
        } else if (month == 5) {
            romawi = "V";
        } else if (month == 6) {
            romawi = "VI";
        } else if (month == 7) {
            romawi = "VII";
        } else if (month == 8) {
            romawi = "VIII";
        } else if (month == 9) {
            romawi = "IX";
        } else if (month == 10) {
            romawi = "X";
        } else if (month == 11) {
            romawi = "XI";
        } else if (month == 12) {
            romawi = "XII";
        }


        return romawi;
    }


}
