package com.din.skripsi.inspeksireport.ui.create_rekomendasi;

public interface CreateRekomendasiNavigator {

    void handleError(Throwable throwable);

    void createRekomendasi();

    void openMainActivity();

}
