package com.din.skripsi.inspeksireport.ui.update_inspeksi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.databinding.ActivityUpdateInspeksiBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class UpdateInspeksiActivity extends BaseActivity<ActivityUpdateInspeksiBinding, UpdateInspeksiViewModel> implements UpdateInspeksiNavigator {

    private ActivityUpdateInspeksiBinding mActivityUpdateInspeksiBinding;


    private static final String PREF_KEY_ID_INSPEKSI = "PREF_KEY_CURRENT_ID_INSPEKSI";

    private static final String PREF_KEY_ID_UNIQUE_INSPEKSI = "PREF_KEY_CURRENT_ID_UNIQUE_INSPEKSI";

    private static final String PREF_KEY_NOMOR_SURAT = "PREF_KEY_CURRENT_ID_NOMOR_SURAT";

    private static final String PREF_KEY_ID_PRODUCT = "PREF_KEY_CURRENT_ID_PRODUCT";

    private static final String PREF_KEY_NAMA_PRODUCT = "PREF_KEY_CURRENT_NAMA_PRODUCT";

    private static final String PREF_KEY_NAMA_PERUSAHAAN = "PREF_KEY_CURRENT_NAMA_PERUSAHAAN";

    private static final String PREF_KEY_KODE_PERUSAHAAN = "PREF_KEY_CURRENT_KODE_PERUSAHAAN";

    private static final String PREF_KEY_SERIAL_NUMBER = "PREF_KEY_CURRENT_SERIAL_NUMBER";

    private static final String PREF_KEY_NOTE = "PREF_KEY_CURRENT_NOTE";

    private static final String PREF_KEY_STATUS_REPORT = "PREF_KEY_CURRENT_STATUS_REPORT";

    private static final String PREF_KEY_ALAMAT_PERUSAHAAN = "PREF_KEY_CURRENT_ALAMAT_PERUSAHAAN";

    private static final String PREF_KEY_CREATED_DATE = "PREF_KEY_CURRENT_CREATED_DATE";
    private SpinnerUpdateProductAdapter productAdapter;
    private SpinnerUpdatePerusahaanAdapter perusahaanAdapter;


    public static Intent newIntent(Context context,
                                   String idUniqueInspeksi,
                                   int idInspeksi,
                                   int statusReport,
                                   int idProduct,
                                   String namaProduct,
                                   String nomorSurat,
                                   String kodePerusahaan,
                                   String namaPerusahaan,
                                   String serialNumber,
                                   String note,
                                   String alamatPerusahaan,String createDate ) {
        Intent intent = new Intent(context, UpdateInspeksiActivity.class);
        intent.putExtra(PREF_KEY_ID_UNIQUE_INSPEKSI, idUniqueInspeksi);
        intent.putExtra(PREF_KEY_ID_INSPEKSI, idInspeksi);
        intent.putExtra(PREF_KEY_ID_PRODUCT, idProduct);
        intent.putExtra(PREF_KEY_NAMA_PRODUCT, namaProduct);
        intent.putExtra(PREF_KEY_NOMOR_SURAT, nomorSurat);
        intent.putExtra(PREF_KEY_NAMA_PERUSAHAAN, namaPerusahaan);
        intent.putExtra(PREF_KEY_KODE_PERUSAHAAN, kodePerusahaan);
        intent.putExtra(PREF_KEY_SERIAL_NUMBER, serialNumber);
        intent.putExtra(PREF_KEY_NOTE, note);
        intent.putExtra(PREF_KEY_STATUS_REPORT, statusReport);
        intent.putExtra(PREF_KEY_ALAMAT_PERUSAHAAN, alamatPerusahaan);
        intent.putExtra(PREF_KEY_CREATED_DATE, createDate);
        return intent;
    }


    public Integer getPrefKeyIdInspeksi() {
        Bundle intent = getIntent().getExtras();
        Integer id_inspeksi = intent.getInt(PREF_KEY_ID_INSPEKSI, 0);
        return id_inspeksi;
    }

    public String getPrefKeyIdUniqueInspeksi() {
        Bundle intent = getIntent().getExtras();
        String id_unique_inspeksi = intent.getString(PREF_KEY_ID_INSPEKSI);
        return id_unique_inspeksi;
    }

    public Integer getPrefKeyIdProduct() {
        Bundle intent = getIntent().getExtras();
        Integer id_product = intent.getInt(PREF_KEY_ID_PRODUCT, 0);
        return id_product;
    }

    public Integer getPrefStatusReport() {
        Bundle intent = getIntent().getExtras();
        Integer status_report = intent.getInt(PREF_KEY_STATUS_REPORT, 0);
        return status_report;
    }

    public String getPrefKeyNamaProduct() {
        Bundle intent = getIntent().getExtras();
        String nama_product = intent.getString(PREF_KEY_NAMA_PRODUCT);
        return nama_product;
    }

    public String getPrefKeyNomorSurat() {
        Bundle intent = getIntent().getExtras();
        String nomor_surat = intent.getString(PREF_KEY_NOMOR_SURAT);
        return nomor_surat;
    }

    public String getPrefKeyKodePerusahaan() {
        Bundle intent = getIntent().getExtras();
        String kode_perusahaan = intent.getString(PREF_KEY_KODE_PERUSAHAAN);
        return kode_perusahaan;
    }

    public String getPrefKeyNamaPerusahaan() {
        Bundle intent = getIntent().getExtras();
        String nama_perusahaan = intent.getString(PREF_KEY_NAMA_PERUSAHAAN);
        return nama_perusahaan;
    }

    public String getPrefKeyAlamatPerusahaan() {
        Bundle intent = getIntent().getExtras();
        String alamat_perusahaan = intent.getString(PREF_KEY_ALAMAT_PERUSAHAAN);
        return alamat_perusahaan;
    }

    public String getPrefKeySerialNumber() {
        Bundle intent = getIntent().getExtras();
        String serial_number = intent.getString(PREF_KEY_SERIAL_NUMBER);
        return serial_number;
    }

    public String getPrefKeyNote() {
        Bundle intent = getIntent().getExtras();
        String note = intent.getString(PREF_KEY_NOTE);
        return note;
    }

    public String getPrefKeyCreatedDate() {
        Bundle intent = getIntent().getExtras();
        String created_date = intent.getString(PREF_KEY_CREATED_DATE);
        return created_date;
    }



    private Spinner spinnerProduct;
    private Spinner spinnerPerusahaan;
    private List<ProductResponse.DataProduct> _dataProducts;
    private List<PerusahaanResponse.DataPerusahaan> _dataPerusahaans;
    private ProductResponse.DataProduct productItem;
    private PerusahaanResponse.DataPerusahaan perusahaanItem;


    public static Intent newIntent(Context context) {
        return new Intent(context, UpdateInspeksiActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_inspeksi;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityUpdateInspeksiBinding = getViewDataBinding();
        mViewModel.setNavigator(this);

        setSupportActionBar(mActivityUpdateInspeksiBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mViewModel.onSetUser();
        setViewByRole();

    }

    private void setViewByRole() {
        mActivityUpdateInspeksiBinding.tvNomorSurat.setText(getPrefKeyNomorSurat());
        mActivityUpdateInspeksiBinding.etSerialNumber.setText(getPrefKeySerialNumber());
//        mActivityUpdateInspeksiBinding.tvAlamatPerusahaan.setText(getPrefKeyAlamatPerusahaan());
//        mActivityUpdateInspeksiBinding.tvNamaPerusahaan.setText(getPrefKeyNamaPerusahaan());
        mActivityUpdateInspeksiBinding.etNote.setText(getPrefKeyNote());

        if (mViewModel.getUserRole().get() == 0) {
//            mActivityUpdateInspeksiBinding.llNamaPerusahaan.setVisibility(View.VISIBLE);
//            mActivityUpdateInspeksiBinding.llAlamatPerusahaan.setVisibility(View.VISIBLE);
        }else if (mViewModel.getUserRole().get() == 1) {
            // Manager
//            mActivityUpdateInspeksiBinding.llNamaPerusahaan.setVisibility(View.VISIBLE);
//            mActivityUpdateInspeksiBinding.llAlamatPerusahaan.setVisibility(View.VISIBLE);
        } else if (mViewModel.getUserRole().get() == 2) {
            // lab
//            mActivityUpdateInspeksiBinding.llNamaPerusahaan.setVisibility(View.GONE);
//            mActivityUpdateInspeksiBinding.llAlamatPerusahaan.setVisibility(View.GONE);
        } else if (mViewModel.getUserRole().get() == 3) {
            // Admin
//            mActivityUpdateInspeksiBinding.llNamaPerusahaan.setVisibility(View.VISIBLE);
//            mActivityUpdateInspeksiBinding.llAlamatPerusahaan.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }


    private  void setUpProduct(){
        for (int position = 0; position < productAdapter.getCount(); position++) {
            if(productAdapter.getItem(position).getIdProduct() == getPrefKeyIdProduct()) {
                spinnerProduct.setSelection(position);
                return;
            }
        }
    }
    private  void setUpPerusahaan(){
        for (int position = 0; position < perusahaanAdapter.getCount(); position++) {
            Log.d("PErusahaan_KEY",perusahaanAdapter.getItem(position).getKodePerusahaan());
            if(perusahaanAdapter.getItem(position).getKodePerusahaan().equals(getPrefKeyKodePerusahaan())  ) {
                Log.d("perusahaan",perusahaanAdapter.getItem(position).getKodePerusahaan());
                Log.d("perusahaan","Masuk");

                spinnerPerusahaan.setSelection(position);
                return;
            }
        }
    }

    @Override
    public void setSpinnerProduct(List<ProductResponse.DataProduct> dataProducts) {
        spinnerProduct = mActivityUpdateInspeksiBinding.productSpinner;
        _dataProducts  = dataProducts;
        productAdapter = new SpinnerUpdateProductAdapter(getApplicationContext(),dataProducts);
        spinnerProduct.setAdapter(productAdapter);
        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                productItem = _dataProducts.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        setUpProduct();
    }

    @Override
    public void setSpinnerPerusahaan(List<PerusahaanResponse.DataPerusahaan> dataPerusahaans) {
        spinnerPerusahaan = mActivityUpdateInspeksiBinding.perusahaanSpinner;
        _dataPerusahaans  = dataPerusahaans;
        perusahaanAdapter = new SpinnerUpdatePerusahaanAdapter(getApplicationContext(),dataPerusahaans);

        spinnerPerusahaan.setAdapter(perusahaanAdapter);
        spinnerPerusahaan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                perusahaanItem = _dataPerusahaans.get(position);
                String kode_perusahaan = _dataPerusahaans.get(position).getKodePerusahaan();

                String spl[]= getPrefKeyNomorSurat().split("/");
                String split1= String.valueOf(spl[0]);
                String split2= String.valueOf(spl[1]);
                String split3= String.valueOf(spl[2]);
                String split4= String.valueOf(spl[3]);
                String split5= String.valueOf(spl[4]);
                String split6= String.valueOf(spl[5]);
                String split7= String.valueOf(spl[6]);



                String nomorSurat= split1+"/"+kode_perusahaan+"/IR/STMJ-20/AS/"+split6+"/"+split7;


                mActivityUpdateInspeksiBinding.tvNomorSurat.setText(nomorSurat);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        setUpPerusahaan();
    }


    @Override
    public void updateInspeksi() {
        String nomorSurat = mActivityUpdateInspeksiBinding.tvNomorSurat.getText().toString();
        String serialNumber = mActivityUpdateInspeksiBinding.etSerialNumber.getText().toString();
//        String alamatPerusahaan = mActivityUpdateInspeksiBinding.tvAlamatPerusahaan.getText().toString();
//        String namaPerusahaan = mActivityUpdateInspeksiBinding.tvNamaPerusahaan.getText().toString();
        String note = mActivityUpdateInspeksiBinding.etNote.getText().toString();

        if (mViewModel.isValid(nomorSurat, perusahaanItem.getIdPerusahaan(),serialNumber,note)) {
            hideKeyboard();
            mViewModel.updateInspeksi(getPrefKeyIdUniqueInspeksi(),nomorSurat, perusahaanItem.getIdPerusahaan(),serialNumber,note,productItem.getIdProduct(),getPrefKeyIdInspeksi());
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(UpdateInspeksiActivity.this);
        startActivity(intent);
        finish();
    }



    private void subscribeToLiveData() {
        mViewModel.getProductData().getValue();
//        mViewModel.getProductData().observe(this, questionCardDatas -> mViewModel.setProductDataList(questionCardDatas));
    }

}