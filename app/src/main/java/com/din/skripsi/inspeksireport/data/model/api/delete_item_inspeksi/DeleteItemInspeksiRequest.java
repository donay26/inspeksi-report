package com.din.skripsi.inspeksireport.data.model.api.delete_item_inspeksi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class DeleteItemInspeksiRequest extends JSONObject {


    @Expose
    @SerializedName("id_item_inspeksi")
    private int id_item_inspeksi;
    @Expose
    @SerializedName("id_user")
    private int id_user;




    public DeleteItemInspeksiRequest(int id_user,int id_item_inspeksi) {
        this.id_user = id_user;
        this.id_item_inspeksi = id_item_inspeksi;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DeleteItemInspeksiRequest that = (DeleteItemInspeksiRequest) object;



        return id_item_inspeksi == 0 ? id_item_inspeksi == that.id_item_inspeksi : that.id_item_inspeksi == 0;

    }


    @Override
    public int hashCode() {
        int result = id_item_inspeksi != 0 ? id_item_inspeksi : 0;
        return result;
    }

    public Integer getIdItemInspeksi() {
        return id_item_inspeksi;
    }
    public Integer getIdUser() {
        return id_user;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_item_inspeksi", getIdItemInspeksi());
            jsonObject.put("id_user", getIdUser());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, DeleteItemInspeksiRequest.class);
    }

}
