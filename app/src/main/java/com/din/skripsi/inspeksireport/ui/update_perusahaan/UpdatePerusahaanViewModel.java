package com.din.skripsi.inspeksireport.ui.update_perusahaan;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.update_perusahaan.UpdatePerusahaanRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.ui.update_perusahaan.UpdatePerusahaanNavigator;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class UpdatePerusahaanViewModel extends BaseViewModel<UpdatePerusahaanNavigator> {


    public UpdatePerusahaanViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama,String kode,String alamat) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }

        if (TextUtils.isEmpty(kode)) {
            return false;
        }

        if (TextUtils.isEmpty(alamat)) {
            return false;
        }
        return true;
    }

    public void updatePerusahaan(int idPerusahaan,String nama,String kode,String alamat) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postUpdatePerusahaanApiCall(new UpdatePerusahaanRequest(nama,kode,alamat,idPerusahaan))
                .map(updatePerusahaanResponse -> updatePerusahaanResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void onServerUpdatePerusahaanClick() {
        getNavigator().updatePerusahaan();
    }
}