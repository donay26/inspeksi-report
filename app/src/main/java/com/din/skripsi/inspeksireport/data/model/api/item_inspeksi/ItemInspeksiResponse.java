package com.din.skripsi.inspeksireport.data.model.api.item_inspeksi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemInspeksiResponse {

    @Expose
    @SerializedName("data")
    private List<DataItemInspeksi> listDataItemInspeksi;

    @Expose
    @SerializedName("status_code")
    private boolean statusCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ItemInspeksiResponse)) {
            return false;
        }

        ItemInspeksiResponse that = (ItemInspeksiResponse) o;

        if (statusCode != that.statusCode) {
            return false;
        }
        if (!message.equals(that.message)) {
            return false;
        }


        return listDataItemInspeksi.size() != that.listDataItemInspeksi.size();
    }

    @Override
    public int hashCode() {

        int result = listDataItemInspeksi.hashCode();
        result = 31 * result;
        return result;
    }

    public List<DataItemInspeksi> getListDataItemInspeksi() {
        return listDataItemInspeksi;
    }
    public String getMessage(){
        return message;
    }

    public boolean getStatusCode() {
        return statusCode;
    }

    public void setDataItemInspeksi(List<DataItemInspeksi> data) {
        this.listDataItemInspeksi = data;
    }


    public static class DataItemInspeksi {

        @Expose
        @SerializedName("id_item_inspeksi")
        private int id_item_inspeksi;

        @Expose
        @SerializedName("id_inspeksi")
        private int id_inspeksi;

        @Expose
        @SerializedName("nama")
        private String nama;

        @Expose
        @SerializedName("rekomendasi")
        private String rekomendasi;

        @Expose
        @SerializedName("id_rekomendasi")
        private Integer id_rekomendasi;

        @Expose
        @SerializedName("remarks")
        private String remarks;

        @Expose
        @SerializedName("created_date")
        private String created_date;

        @Expose
        @SerializedName("status")
        private String status;

        @Expose
        @SerializedName("id_status")
        private Integer id_status;


        public int getIdItemInspeksi() {
            return id_item_inspeksi;
        }

        public int getIdInspeksi() {
            return id_inspeksi;
        }

        public String getNama() {
            return nama;
        }

        public String getRekomendasi() {
            return rekomendasi;
        }
        public Integer getIdRekomendasi() {
            return id_rekomendasi;
        }

        public String getRemarks() {
            return remarks;
        }

        public String getCreatedDate() {
            return created_date;
        }

        public String getStatus() {
            return status;
        }

        public Integer getIdStatus() {
            return id_status;
        }





    }
}
