package com.din.skripsi.inspeksireport.ui.login;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.login.LoginRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {

    public LoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isNIKAndPasswordValid(String nik, String password) {
        // validate email and password
        if (TextUtils.isEmpty(nik)) {
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            return false;
        }
        return true;
    }

    public void login(String nik, String password) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().doLoginApiCall(new LoginRequest(nik,password))
                .doOnSuccess(
                        loginResponse -> getDataManager()
                                .updateUserInfo(loginResponse.getData().getIdUser(),loginResponse.getData().getNIK(),loginResponse.getData().getNama(),loginResponse.getData().getRole()))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }



    public void onServerLoginClick() {
        getNavigator().login();
    }

}
