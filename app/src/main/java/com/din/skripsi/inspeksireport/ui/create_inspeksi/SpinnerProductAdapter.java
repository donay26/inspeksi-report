package com.din.skripsi.inspeksireport.ui.create_inspeksi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;

import java.util.ArrayList;
import java.util.List;

public class SpinnerProductAdapter extends BaseAdapter {
    Context context;

    List<ProductResponse.DataProduct> dataProducts =  new ArrayList<>();
    LayoutInflater inflter;

    public SpinnerProductAdapter(Context applicationContext, List<ProductResponse.DataProduct> dataProducts) {
        this.context = applicationContext;
        this.dataProducts = dataProducts;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        if (dataProducts !=  null) {
            return dataProducts.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_spinner_product, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(dataProducts.get(i).getNamaProduct());
        return view;
    }
}
