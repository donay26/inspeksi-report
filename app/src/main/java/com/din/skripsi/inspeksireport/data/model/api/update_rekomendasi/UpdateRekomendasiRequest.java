package com.din.skripsi.inspeksireport.data.model.api.update_rekomendasi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class UpdateRekomendasiRequest extends JSONObject {

    @Expose
    @SerializedName("id_rekomendasi")
    private Integer id_rekomendasi;

    @Expose
    @SerializedName("title")
    private String title_rekomendasi;



    public UpdateRekomendasiRequest(String title_rekomendasi, Integer id_rekomendasi) {

        this.title_rekomendasi = title_rekomendasi;
        this.id_rekomendasi = id_rekomendasi;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        UpdateRekomendasiRequest that = (UpdateRekomendasiRequest) object;



        return title_rekomendasi == "" ? title_rekomendasi == that.title_rekomendasi : that.title_rekomendasi == "";

    }


    @Override
    public int hashCode() {
        int result = title_rekomendasi.hashCode() != 0 ? title_rekomendasi.hashCode() : 0;
        return result;
    }

    public String getTitleRekomendasi() {
        return title_rekomendasi;
    }
    public Integer getIdRekomendasi() {
        return id_rekomendasi;
    }


    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("title", getTitleRekomendasi());
            jsonObject.put("id_rekomendasi", getIdRekomendasi());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, UpdateRekomendasiRequest.class);
    }

}
