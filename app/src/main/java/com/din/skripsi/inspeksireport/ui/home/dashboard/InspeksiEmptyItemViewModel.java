package com.din.skripsi.inspeksireport.ui.home.dashboard;

public class InspeksiEmptyItemViewModel {

    private final InspeksiEmptyItemViewModelListener mListener;

    public InspeksiEmptyItemViewModel(InspeksiEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface InspeksiEmptyItemViewModelListener {

        void onRetryClick();
    }
}
