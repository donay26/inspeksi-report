package com.din.skripsi.inspeksireport.data.model.api.create_product;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class CreateProductRequest extends JSONObject {

    @Expose
    @SerializedName("nama")
    private String nama;



    public CreateProductRequest(String nama) {
        this.nama = nama;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        CreateProductRequest that = (CreateProductRequest) object;



        return nama == "" ? nama == that.nama : that.nama == "";

    }


    @Override
    public int hashCode() {
        int result = nama.hashCode() != 0 ? nama.hashCode() : 0;
        return result;
    }

    public String getNama() {
        return nama;
    }


    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nama", getNama());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, CreateProductRequest.class);
    }

}
