package com.din.skripsi.inspeksireport.ui.create_inspeksi;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.create_inspeksi.CreateInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CreateInspeksiViewModel extends BaseViewModel<CreateInspeksiNavigator > {

    private final MutableLiveData<List<ProductResponse.DataProduct>> productItemLiveData;

    private final ObservableList<ProductResponse.DataProduct> productDataList = new ObservableArrayList<>();

    public CreateInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        productItemLiveData = new MutableLiveData<>();
        loadMasters();

    }

    public boolean isValid(String nomor_surat, Integer id_perusahaan, String serial_number, String note, File images) {


        if (TextUtils.isEmpty(nomor_surat)) {
            return false;
        }
        if (id_perusahaan == null) {
            return false;
        }
        if (TextUtils.isEmpty(serial_number)) {
            return false;
        }

        if (TextUtils.isEmpty(note)) {
            return false;
        }
        return true;
    }

    public void createInspeksi(String nomor_surat, int id_perusahaan, String serial_number , String note, Integer id_product, File images) throws UnsupportedEncodingException {
        setIsLoading(true);
        String dataImage = "data:image/png;base64," + images;
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyMMddhhmmssMs");
        String UniqueId = ft.format(dNow);

        getCompositeDisposable().add(getDataManager().postCreateInspeksiApiCall(new CreateInspeksiRequest(getDataManager().getCurrentUserId(),nomor_surat,id_perusahaan,UniqueId,serial_number,note,id_product,""),images)
                .map(createInspeksiResponse -> createInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
//                    Log.d("ERROR",throwable.getMessage());
                    getNavigator().handleError(throwable);
                })
        );
    }

    public void loadMasters() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getAllMasterApiCall()
                .map(masterResponse -> masterResponse.getDataMaaster())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(dataMaster -> {
                    setIsLoading(false);
                    if (dataMaster != null) {
//                        Log.d("TAG", "loadQuestionCards: " + dataMaster.size());
//                        productItemLiveData.setValue(questionList);
                        getNavigator().setSpinnerProduct(dataMaster.getDataProduct());
                        getNavigator().setSpinnerPerusahaan(dataMaster.getDataPerusahaan());
                    }
                }, throwable -> {
                    Log.d("TAG", "loadQuestionCards: " + throwable);
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }



    public void onServerCreateInspeksiClick() {
        getNavigator().createInspeksi();
    }

    public void onImagePickerClick() {
        getNavigator().openImagePicker();
    }

    public LiveData<List<ProductResponse.DataProduct>> getProductData() {
        return productItemLiveData;
    }


    public ObservableList<ProductResponse.DataProduct> getProductDataList() {
        return productDataList;
    }

    public void setProductDataList(List<ProductResponse.DataProduct> productDatas) {
        productDataList.clear();
        productDataList.addAll(productDatas);
    }


}
