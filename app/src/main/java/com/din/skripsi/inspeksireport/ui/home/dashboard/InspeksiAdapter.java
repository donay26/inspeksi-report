package com.din.skripsi.inspeksireport.ui.home.dashboard;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.din.skripsi.inspeksireport.databinding.ItemInspeksiEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemInspeksiViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class InspeksiAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<InspeksiItemViewModel> mInspeksiResponseList;

    private InspeksiAdapterListener mListener;

    public InspeksiAdapter() {
        this.mInspeksiResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mInspeksiResponseList.isEmpty()) {
            return mInspeksiResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mInspeksiResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemInspeksiViewBinding openSourceViewBinding = ItemInspeksiViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new InspeksiViewHolder(openSourceViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemInspeksiEmptyViewBinding emptyViewBinding = ItemInspeksiEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<InspeksiItemViewModel> repoList) {
        mInspeksiResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mInspeksiResponseList.clear();
    }

    public void setListener(InspeksiAdapterListener listener) {
        this.mListener = listener;
    }

    public interface InspeksiAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements InspeksiEmptyItemViewModel.InspeksiEmptyItemViewModelListener {

        private final ItemInspeksiEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemInspeksiEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            InspeksiEmptyItemViewModel emptyItemViewModel = new InspeksiEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class InspeksiViewHolder extends BaseViewHolder {

        private final ItemInspeksiViewBinding mBinding;

        public InspeksiViewHolder(ItemInspeksiViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final InspeksiItemViewModel mInspeksiItemViewModel = mInspeksiResponseList.get(position);

            mBinding.setViewModel(mInspeksiItemViewModel);

            mBinding.titleTextView.setText(mInspeksiResponseList.get(position)._nomorSurat.get()+ " - "+CommonUtils.convertStatus(mInspeksiResponseList.get(position)._statusReport.get()));
            mBinding.dateTextView.setText(CommonUtils.convertTimeStamp(mInspeksiResponseList.get(position)._createdDate.get()));
            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}