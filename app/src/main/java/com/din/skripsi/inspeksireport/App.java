package com.din.skripsi.inspeksireport;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.din.skripsi.inspeksireport.inject.component.AppComponent;
import com.din.skripsi.inspeksireport.inject.component.DaggerAppComponent;


public  class  App extends Application {

    public AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .application(this)
                .build();

        appComponent.inject(this);

        AndroidNetworking.initialize(getApplicationContext());
        if (BuildConfig.DEBUG) {
            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BASIC);
        }

    }

}