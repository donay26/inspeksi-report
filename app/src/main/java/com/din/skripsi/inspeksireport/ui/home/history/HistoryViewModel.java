package com.din.skripsi.inspeksireport.ui.home.history;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.history_inspeksi.HistoryInspeksiResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;

import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class HistoryViewModel extends BaseViewModel<HistoryNavigator> implements HistoryItemViewModel.HistoryItemViewModelListener {

    private final MutableLiveData<List<HistoryItemViewModel>> inspeksiItemsLiveData;



    public HistoryViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        inspeksiItemsLiveData = new MutableLiveData<>();
    }

    public void fetchHistorys() {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);
        getCompositeDisposable().add(getDataManager()
                .getAllHistoryApiCall()
                .map(inspeksiResponse -> inspeksiResponse.getListDataInspeksi())
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(inspeksiResponse -> {
                    inspeksiItemsLiveData.setValue(inspeksiResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public LiveData<List<HistoryItemViewModel>> getHistoryItemsLiveData() {
        return inspeksiItemsLiveData;
    }

    private Single<List<HistoryItemViewModel>> getViewModelList(List<HistoryInspeksiResponse.DataInspeksi> repoList) {
        return Observable.fromIterable(repoList)
                .map(repo -> {
                    // masuk
                    return new HistoryItemViewModel(
                            repo.getIdUniqueInspeksi(),
                            repo.getIdInspeksi(),
                            repo.getStatusReport(),
                            repo.getIdProduct(),
                            repo.getNamaProduct(),
                            repo.getNomorSurat(),
                            repo.getSerialNumber(),
                            repo.getNamaPerusahaan(),
                            repo.getKodePerusahaan(),
                            repo.getAlamatPerusahaan(),
                            repo.getNote(),
                            repo.getCreatedDate(),repo.getUpdatedDate(),repo.getImageName(),this);
                }).toList();
    }




    public void onBackClick() {
        getNavigator().onBack();
    }


    @Override
    public void onItemClick(String idUniqueInspeksi,int idInspeksi, int statusReport, int idProduct, String namaProduct, String nomorSurat, String serialnumber, String namaPerusahaan,String kode_perusahaan, String alamatPerusahaan, String createdDate,String updated_date ,String note,String image_name) {
                try {
                    getNavigator().openDetail(idUniqueInspeksi,idInspeksi,statusReport,idProduct,namaProduct,nomorSurat,serialnumber,namaPerusahaan,kode_perusahaan,alamatPerusahaan,note,image_name,createdDate,updated_date);
        } catch (Exception e) {
        }

    }
}
