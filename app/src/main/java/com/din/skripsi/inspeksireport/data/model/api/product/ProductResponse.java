package com.din.skripsi.inspeksireport.data.model.api.product;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductResponse {

    @Expose
    @SerializedName("data")
    private List<DataProduct> listDataProduct;

    @Expose
    @SerializedName("status_code")
    private boolean statusCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductResponse)) {
            return false;
        }

        ProductResponse that = (ProductResponse) o;

        if (statusCode != that.statusCode) {
            return false;
        }
        if (!message.equals(that.message)) {
            return false;
        }


        return listDataProduct.size() != that.listDataProduct.size();
    }

    @Override
    public int hashCode() {

        int result = listDataProduct.hashCode();
        result = 31 * result;
        return result;
    }

    public List<DataProduct> getListDataProduct() {
        return listDataProduct;
    }
    public String getMessage(){
        return message;
    }

    public boolean getStatusCode() {
        return statusCode;
    }

    public void setDataProduct(List<DataProduct> data) {
        this.listDataProduct = data;
    }


    public static class DataProduct {

        @Expose
        @SerializedName("id_product")
        private Integer id_product;

        @Expose
        @SerializedName("nama")
        private String nama_product;

        @Expose
        @SerializedName("status")
        private Integer status_product;

        @Expose
        @SerializedName("created_date")
        private String created_date;



        public String getNamaProduct() {
            return nama_product;
        }

        public Integer getStatusProduct() {
            return status_product;
        }


        public Integer getIdProduct() {
            return id_product;
        }

        public String getCreatedDate() {
            return created_date;
        }




    }
}
