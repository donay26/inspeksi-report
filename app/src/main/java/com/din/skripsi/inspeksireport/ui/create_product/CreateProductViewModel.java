package com.din.skripsi.inspeksireport.ui.create_product;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.create_product.CreateProductRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class CreateProductViewModel extends BaseViewModel<CreateProductNavigator> {



    public CreateProductViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }

        return true;
    }

    public void createProduct(String nama) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postCreateProductApiCall(new CreateProductRequest(nama))
                .map(createInspeksiResponse -> createInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }




    public void onServerCreateProductClick() {
        getNavigator().createProduct();
    }


}
