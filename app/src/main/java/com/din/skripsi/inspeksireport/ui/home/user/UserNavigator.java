package com.din.skripsi.inspeksireport.ui.home.user;

public interface UserNavigator {
    void handleError(Throwable throwable);
    void openDialog(int idUser, String nama, int status, int role, String password, String nik, int i);
    void isSwipeRefresh(boolean swipe);
    void isRefresh();
    void onBack();
    void openCreateUserActivity();

}
