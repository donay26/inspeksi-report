package com.din.skripsi.inspeksireport.ui.update_inspeksi;

import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;

import java.util.List;

public interface UpdateInspeksiNavigator {

    void handleError(Throwable throwable);

    void setSpinnerProduct(List<ProductResponse.DataProduct> dataProducts);

    void setSpinnerPerusahaan(List<PerusahaanResponse.DataPerusahaan> dataPerusahaans);

    void updateInspeksi();

    void openMainActivity();

}
