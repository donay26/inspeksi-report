package com.din.skripsi.inspeksireport.data.model.api.update_status;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class UpdateStatusRequest extends JSONObject {

    @Expose
    @SerializedName("id_status")
    private Integer id_status;

    @Expose
    @SerializedName("nama_status")
    private String nama_status;



    public UpdateStatusRequest(String nama_status, Integer id_status) {

        this.nama_status = nama_status;
        this.id_status = id_status;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        UpdateStatusRequest that = (UpdateStatusRequest) object;



        return nama_status == "" ? nama_status == that.nama_status : that.nama_status == "";

    }


    @Override
    public int hashCode() {
        int result = nama_status.hashCode() != 0 ? nama_status.hashCode() : 0;
        return result;
    }

    public String getnamaStatus() {
        return nama_status;
    }
    public Integer getIdStatus() {
        return id_status;
    }


    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nama_status", getnamaStatus());
            jsonObject.put("id_status", getIdStatus());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, UpdateStatusRequest.class);
    }

}
