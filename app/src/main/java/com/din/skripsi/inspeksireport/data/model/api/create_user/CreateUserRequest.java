package com.din.skripsi.inspeksireport.data.model.api.create_user;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class CreateUserRequest extends JSONObject {

    @Expose
    @SerializedName("nama")
    private String nama;

    @Expose
    @SerializedName("nik")
    private String nik;

    @Expose
    @SerializedName("password")
    private String password;

    @Expose
    @SerializedName("role")
    private Integer role;



    public CreateUserRequest(String nama, String nik, String password, Integer role) {
        this.nama = nama;
        this.nik = nik;
        this.role = role;
        this.password = password;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        CreateUserRequest that = (CreateUserRequest) object;



        return nik == "" ? nik == that.nik : that.nik == "";

    }


    @Override
    public int hashCode() {
        int result = nik.hashCode() != 0 ? nik.hashCode() : 0;
        return result;
    }

    public String getNama() {
        return nama;
    }
    public String getNik() {
        return nik;
    }
    public String getPassword() {
        return password;
    }
    public Integer getRole() {
        return role;
    }



    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nama", getNama());
            jsonObject.put("nik", getNik());
            jsonObject.put("password", getPassword());
            jsonObject.put("role", getRole());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, CreateUserRequest.class);
    }

}
