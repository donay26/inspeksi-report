package com.din.skripsi.inspeksireport.ui.home.dashboard;


import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.inspeksi.InspeksiResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.CommonUtils;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class DashboardViewModel extends BaseViewModel<DashboardNavigator> implements InspeksiItemViewModel.InspeksiItemViewModelListener {

    private final MutableLiveData<List<InspeksiItemViewModel>> inspeksiItemsLiveData;

    private final ObservableField<Integer> total = new ObservableField<>();

    private final ObservableField<Integer> userRole = new ObservableField<>();

    public DashboardViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        inspeksiItemsLiveData = new MutableLiveData<>();
//        fetchInspeksis();
    }

    public ObservableField<Integer> getUserRole() {
        return userRole;
    }

    public void onSetRole() {
        final Integer currentUserrole = getDataManager().getCurrentUserRole();
        userRole.set(currentUserrole);
    }


    public void fetchInspeksis() {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);
        getCompositeDisposable().add(getDataManager()
                .getAllInspeksiApiCall()
                .map(inspeksiResponse -> inspeksiResponse)
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(inspeksiResponse -> {
                    Log.e("Listnya", String.valueOf(inspeksiResponse.size()));
                    inspeksiItemsLiveData.setValue(inspeksiResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public ObservableField<Integer> getTotal() {
        return total;
    }


    public LiveData<List<InspeksiItemViewModel>> getInspeksiItemsLiveData() {
        return inspeksiItemsLiveData;
    }

    private Single<List<InspeksiItemViewModel>> getViewModelList(InspeksiResponse repoList) {
        total.set(repoList.getDataTotal().getTotal());
        return Observable.fromIterable(repoList.getListDataInspeksi())
                .map(repo -> {
                    // masuk
                    return new InspeksiItemViewModel(
                            repo.getUniqueIdInspeksi(),
                            repo.getIdInspeksi(),
                            repo.getStatusReport(),
                            repo.getIdProduct(),
                            repo.getNamaProduct(),
                            repo.getNomorSurat(),
                            repo.getSerialNumber(),
                            repo.getNamaPerusahaan(),
                            repo.getKodePerusahaan(),
                            repo.getAlamatPerusahaan(),
                            repo.getNote(),
                             repo.getCreatedDate(),repo.getUpdatedDate(),repo.getImageName(),this);
                }).toList();
    }




    public void onBackClick() {
        getNavigator().onBack();
    }

    public void toCreateInspeksiClick() {
        getNavigator().openCreateInspeksiActivity();
    }


    @Override
    public void onItemClick(String idUniqueInspeksi,int idInspeksi, int statusReport, int idProduct, String namaProduct, String nomorSurat, String serialnumber, String namaPerusahaan,String kode_perusahaan, String alamatPerusahaan, String createdDate,String updated_date ,String note,String image_name) {
        getNavigator().openDetail(idUniqueInspeksi,idInspeksi,statusReport,idProduct,namaProduct,nomorSurat,serialnumber,namaPerusahaan,kode_perusahaan,alamatPerusahaan,note,image_name,createdDate,updated_date);

    }
}
