package com.din.skripsi.inspeksireport.ui.home.user;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;


import com.din.skripsi.inspeksireport.databinding.ItemUserEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemUserViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<UserItemViewModel> mUserResponseList;

    private UserAdapterListener mListener;

    public UserAdapter() {
        this.mUserResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mUserResponseList.isEmpty()) {
            return mUserResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mUserResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemUserViewBinding userViewBinding = ItemUserViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new UserViewHolder(userViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemUserEmptyViewBinding emptyViewBinding = ItemUserEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<UserItemViewModel> repoList) {
        mUserResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mUserResponseList.clear();
    }

    public void setListener(UserAdapterListener listener) {
        this.mListener = listener;
    }

    public interface UserAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements UserEmptyItemViewModel.UserEmptyItemViewModelListener {

        private final ItemUserEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemUserEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            UserEmptyItemViewModel emptyItemViewModel = new UserEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class UserViewHolder extends BaseViewHolder {

        private final ItemUserViewBinding mBinding;

        public UserViewHolder(ItemUserViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final UserItemViewModel mUserItemViewModel = mUserResponseList.get(position);

            mBinding.setViewModel(mUserItemViewModel);

            mBinding.titleTextView.setText(mUserItemViewModel._nikUser.get()+" - "+CommonUtils.convertRoleString(mUserItemViewModel._roleUser.get()));
            mBinding.createdDateTextView.setText(CommonUtils.convertTimeStamp(mUserResponseList.get(position)._createdDateUser.get()));


            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed i mmediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}