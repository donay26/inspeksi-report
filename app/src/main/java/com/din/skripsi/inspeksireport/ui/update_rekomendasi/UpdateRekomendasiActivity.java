package com.din.skripsi.inspeksireport.ui.update_rekomendasi;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityUpdateRekomendasiBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;

public class UpdateRekomendasiActivity extends BaseActivity<ActivityUpdateRekomendasiBinding, UpdateRekomendasiViewModel> implements UpdateRekomendasiNavigator {

    private ActivityUpdateRekomendasiBinding mActivityUpdateRekomendasiBinding;

    private static final String PREF_KEY_ID_REKOMENDASI = "PREF_KEY_CURRENT_ID_REKOMENDASI";

    private static final String PREF_KEY_TITLE = "PREF_KEY_CURRENT_TITLE";



    public static Intent newIntent(Context context,
                                   int idRekomendasi,
                                   String title
    ) {
        Intent intent = new Intent(context, UpdateRekomendasiActivity.class);
        intent.putExtra(PREF_KEY_ID_REKOMENDASI, idRekomendasi);
        intent.putExtra(PREF_KEY_TITLE, title);
        return intent;
    }


    public Integer getPrefKeyIdRekomendasi() {
        Bundle intent = getIntent().getExtras();
        Integer id_rekomendasi = intent.getInt(PREF_KEY_ID_REKOMENDASI, 0);
        return id_rekomendasi;
    }


    public String getPrefKeyNama() {
        Bundle intent = getIntent().getExtras();
        String title = intent.getString(PREF_KEY_TITLE);
        return title;
    }



    public static Intent newIntent(Context context) {
        return new Intent(context, UpdateRekomendasiActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_rekomendasi;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityUpdateRekomendasiBinding = getViewDataBinding();
        mViewModel.setNavigator(this);

        setSupportActionBar(mActivityUpdateRekomendasiBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUp();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }


    private  void setUp(){
        mActivityUpdateRekomendasiBinding.etNama.setText(getPrefKeyNama());


    }



    @Override
    public void updateRekomendasi() {

        String namaRekomendasi = mActivityUpdateRekomendasiBinding.etNama.getText().toString();

        if (mViewModel.isValid(namaRekomendasi)) {
            hideKeyboard();
            mViewModel.updateRekomendasi(getPrefKeyIdRekomendasi(),namaRekomendasi);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(UpdateRekomendasiActivity.this);
        startActivity(intent);
        finish();
    }





}