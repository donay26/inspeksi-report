package com.din.skripsi.inspeksireport.data.model.api.update_perusahaan;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class UpdatePerusahaanRequest extends JSONObject {

    @Expose
    @SerializedName("id_perusahaan")
    private Integer id_perusahaan;

    @Expose
    @SerializedName("nama_perusahaan")
    private String nama_perusahaan;

    @Expose
    @SerializedName("kode_perusahaan")
    private String kode_perusahaan;

    @Expose
    @SerializedName("alamat_perusahaan")
    private String alamat_perusahaan;



    public UpdatePerusahaanRequest(String nama,String kode,String alamat, Integer id_perusahaan) {

        this.kode_perusahaan = kode;
        this.nama_perusahaan = nama;
        this.alamat_perusahaan = alamat;
        this.id_perusahaan = id_perusahaan;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        UpdatePerusahaanRequest that = (UpdatePerusahaanRequest) object;



        return nama_perusahaan == "" ? nama_perusahaan == that.nama_perusahaan : that.nama_perusahaan == "";

    }


    @Override
    public int hashCode() {
        int result = nama_perusahaan.hashCode() != 0 ? nama_perusahaan.hashCode() : 0;
        return result;
    }

    public String getNamaPerusahaan() {
        return nama_perusahaan;
    }
    public String getKodePerusahaan() {
        return kode_perusahaan;
    }
    public String getAlamatPerusahaan() {
        return alamat_perusahaan;
    }
    public Integer getIdPerusahaan() {
        return id_perusahaan;
    }


    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nama_perusahaan", getNamaPerusahaan());
            jsonObject.put("kode_perusahaan", getKodePerusahaan());
            jsonObject.put("alamat_perusahaan", getAlamatPerusahaan());
            jsonObject.put("id_perusahaan", getIdPerusahaan());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, UpdatePerusahaanRequest.class);
    }

}
