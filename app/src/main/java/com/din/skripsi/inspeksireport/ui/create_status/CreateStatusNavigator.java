package com.din.skripsi.inspeksireport.ui.create_status;

public interface CreateStatusNavigator {

    void handleError(Throwable throwable);

    void createStatus();

    void openMainActivity();

}
