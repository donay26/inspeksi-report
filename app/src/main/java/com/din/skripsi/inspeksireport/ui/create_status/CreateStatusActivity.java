package com.din.skripsi.inspeksireport.ui.create_status;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;


import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityCreateStatusBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;

public class CreateStatusActivity extends BaseActivity<ActivityCreateStatusBinding, CreateStatusViewModel> implements CreateStatusNavigator {

    private ActivityCreateStatusBinding mActivityCreateStatusBinding;


    public static Intent newIntent(Context context) {
        return new Intent(context, CreateStatusActivity.class);
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_status;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCreateStatusBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setSupportActionBar(mActivityCreateStatusBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void createStatus() {
        String namaStatus = mActivityCreateStatusBinding.etStatus.getText().toString();

        if (mViewModel.isValid(namaStatus)) {
            hideKeyboard();
            mViewModel.createStatus(namaStatus);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(CreateStatusActivity.this);
        startActivity(intent);
        finish();
    }


}