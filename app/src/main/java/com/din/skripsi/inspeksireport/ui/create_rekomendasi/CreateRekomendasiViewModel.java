package com.din.skripsi.inspeksireport.ui.create_rekomendasi;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.create_rekomendasi.CreateRekomendasiRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class CreateRekomendasiViewModel extends BaseViewModel<CreateRekomendasiNavigator> {



    public CreateRekomendasiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }

        return true;
    }

    public void createRekomendasi(String nama) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postCreateRekomendasiApiCall(new CreateRekomendasiRequest(nama))
                .map(createRekomendasiResponse -> createRekomendasiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }




    public void onServerCreateRekomendasiClick() {
        getNavigator().createRekomendasi();
    }


}
