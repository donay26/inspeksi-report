package com.din.skripsi.inspeksireport.ui.home.rekomendasi;

import androidx.databinding.ObservableField;


public class RekomendasiItemViewModel {

    public final ObservableField<Integer> _idRekomendasi = new ObservableField<>();
    public final ObservableField<String> _titleRekomendasi = new ObservableField<>();
    public final ObservableField<String> _createdDateRekomendasi = new ObservableField<>();

    public final RekomendasiItemViewModel.RekomendasiItemViewModelListener mListener;

    public RekomendasiItemViewModel(int idRekomendasi, String title_rekomendasi, String created_date, RekomendasiItemViewModel.RekomendasiItemViewModelListener listener) {
        this.mListener = listener;
        this._idRekomendasi.set(idRekomendasi);
        this._titleRekomendasi.set(title_rekomendasi);
        this._createdDateRekomendasi.set(created_date);
    }

    public void onItemClick() {
        mListener.onItemClick(_idRekomendasi.get(),_titleRekomendasi.get());
    }

    public interface RekomendasiItemViewModelListener {

        void onItemClick(int idRekomendasi,String title);
    }

}
