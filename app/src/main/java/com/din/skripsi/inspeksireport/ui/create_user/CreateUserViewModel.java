package com.din.skripsi.inspeksireport.ui.create_user;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.create_user.CreateUserRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class CreateUserViewModel extends BaseViewModel<CreateUserNavigator> {



    public CreateUserViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama, String nik,String password) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }
        if (TextUtils.isEmpty(nik)) {
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            return false;
        }
        return true;
    }

    public void CreateUser(String nama, String nik,String password,int role) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postCreateUserApiCall(new CreateUserRequest(nama,nik,password,role))
                .map(createInspeksiResponse -> createInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }




    public void onServerCreateUserClick() {
        getNavigator().createUser();
    }


}
