package com.din.skripsi.inspeksireport.ui.create_item_inspeksi;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;

import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;
import com.din.skripsi.inspeksireport.databinding.ActivityCreateItemInspeksiBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.create_inspeksi.SpinnerProductAdapter;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class CreateItemInspeksiActivity extends BaseActivity<ActivityCreateItemInspeksiBinding, CreateItemInspeksiViewModel> implements CreateItemInspeksiNavigator{

    private ActivityCreateItemInspeksiBinding mActivityCreateItemInspeksiBinding;

    private Spinner spinnerStatus;
    private Spinner spinnerRekomendasi;
    private List<StatusResponse.DataStatus> _dataStatuss;
    private List<RekomendasiResponse.DataRekomendasi> _dataRekomendasis;
    private StatusResponse.DataStatus StatusItem;
    private RekomendasiResponse.DataRekomendasi RekomendasiItem;


    public static final String PREF_KEY_ID_INSPEKSI = "PREF_KEY_CURRENT_ID_INSPEKSI";
    private static final String PREF_KEY_NOMOR_SURAT = "PREF_KEY_CURRENT_NOMOR_SURAT";


    public static Intent newIntent(Context context,int id_inspeksi, String nomor_surat) {
        Intent intent = new Intent(context, CreateItemInspeksiActivity.class);
        intent.putExtra(PREF_KEY_ID_INSPEKSI, id_inspeksi);
        intent.putExtra(PREF_KEY_NOMOR_SURAT, nomor_surat);
        return intent;
    }

    public Integer getPrefKeyItemInspeksi() {
        Bundle intent = getIntent().getExtras();

        Integer id_inspeksi = intent.getInt(PREF_KEY_ID_INSPEKSI,0);
        return id_inspeksi;
    }

    public String getPrefKeyNomorSurat() {
        Bundle intent = getIntent().getExtras();

        String nomor_surat = intent.getString(PREF_KEY_NOMOR_SURAT);
        return nomor_surat;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_item_inspeksi;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCreateItemInspeksiBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setSupportActionBar(mActivityCreateItemInspeksiBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUp();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void setUp() {
        mActivityCreateItemInspeksiBinding.tvNomorSurat.setText(getPrefKeyNomorSurat());
    }

    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void createInspeksi() {
        String namaItemInspeksi = mActivityCreateItemInspeksiBinding.etnamaItem.getText().toString();
        String remarks = mActivityCreateItemInspeksiBinding.etRemarks.getText().toString();

        if (mViewModel.isValid(namaItemInspeksi,remarks)) {
            hideKeyboard();
            mViewModel.createItemInspeksi(getPrefKeyItemInspeksi(), namaItemInspeksi,StatusItem.getIdStatus(),RekomendasiItem.getIdRekomendasi(),remarks);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openInspeksiActivity() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(PREF_KEY_ID_INSPEKSI, getPrefKeyItemInspeksi());
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void setSpinnerStatus(List<StatusResponse.DataStatus> dataStatuss) {
        spinnerStatus = mActivityCreateItemInspeksiBinding.statusSpinner;
        _dataStatuss  = dataStatuss;
        SpinnerStatusAdapter customAdapter=new SpinnerStatusAdapter(getApplicationContext(),dataStatuss);

        spinnerStatus.setAdapter(customAdapter);
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StatusItem = _dataStatuss.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setSpinnerRekomendasi(List<RekomendasiResponse.DataRekomendasi> dataRekomendasis) {
        spinnerRekomendasi = mActivityCreateItemInspeksiBinding.rekomendasiSpinner;
        _dataRekomendasis  = dataRekomendasis;
        SpinnerRekomendasiAdapter customAdapter=new SpinnerRekomendasiAdapter(getApplicationContext(),dataRekomendasis);

        spinnerRekomendasi.setAdapter(customAdapter);
        spinnerRekomendasi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                RekomendasiItem = _dataRekomendasis.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


}