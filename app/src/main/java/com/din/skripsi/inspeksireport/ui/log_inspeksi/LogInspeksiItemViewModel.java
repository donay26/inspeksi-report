package com.din.skripsi.inspeksireport.ui.log_inspeksi;

import androidx.databinding.ObservableField;

public class LogInspeksiItemViewModel  {

    public final ObservableField<String> _nama = new ObservableField<>();
    public final ObservableField<String> _role = new ObservableField<>();
    public final ObservableField<String> _note = new ObservableField<>();
    public final ObservableField<String> _serialNumber = new ObservableField<>();
    public final ObservableField<String> _createdDate = new ObservableField<>();

    public final LogInspeksiItemViewModelListener mListener;

    public LogInspeksiItemViewModel(String nama,String role,String note,String serial_number,String  createdDate,LogInspeksiItemViewModelListener listener) {
        this.mListener = listener;
        this._nama.set(nama);
        this._role.set(role);
        this._note.set(note);
        this._serialNumber.set(serial_number);
        this._createdDate.set(createdDate);
    }

    public void onItemClick() {
        mListener.onItemClick(_nama.get(),_role.get(),_note.get(),_serialNumber.get(),_createdDate.get());
    }

    public interface LogInspeksiItemViewModelListener {

        void onItemClick(String nama,String role,String note,String serial_number,String  createdDate);
    }

}

