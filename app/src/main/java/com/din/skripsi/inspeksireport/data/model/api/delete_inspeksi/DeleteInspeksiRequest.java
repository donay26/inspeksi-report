package com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class DeleteInspeksiRequest extends JSONObject {


    @Expose
    @SerializedName("id_inspeksi")
    private int id_inspeksi;




    public DeleteInspeksiRequest(int id_inspeksi) {
        this.id_inspeksi = id_inspeksi;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DeleteInspeksiRequest that = (DeleteInspeksiRequest) object;



        return id_inspeksi == 0 ? id_inspeksi == that.id_inspeksi : that.id_inspeksi == 0;

    }


    @Override
    public int hashCode() {
        int result = id_inspeksi != 0 ? id_inspeksi : 0;
        return result;
    }

    public Integer getIdInspeksi() {
        return id_inspeksi;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_inspeksi", getIdInspeksi());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, DeleteInspeksiRequest.class);
    }

}
