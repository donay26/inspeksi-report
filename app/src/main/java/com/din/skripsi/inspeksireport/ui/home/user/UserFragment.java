package com.din.skripsi.inspeksireport.ui.home.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.FragmentUserBinding;
import com.din.skripsi.inspeksireport.inject.component.FragmentComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseFragment;
import com.din.skripsi.inspeksireport.ui.create_user.CreateUserActivity;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.DetailInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.update_item_inspeksi.UpdateItemInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.update_user.UpdateUserActivity;

import javax.inject.Inject;

public class UserFragment extends BaseFragment<FragmentUserBinding, UserViewModel> implements UserNavigator, UserAdapter.UserAdapterListener {

    public static final String TAG = "UserFragment";


    FragmentUserBinding mFragmentUserBinding;
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    UserAdapter mUserAdapter;


    public static UserFragment newInstance() {
        Bundle args = new Bundle();
        UserFragment fragment = new UserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
        mUserAdapter.setListener(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentUserBinding = getViewDataBinding();
        mViewModel.fetchusers();
        setUp();
    }


    @Override
    public void performDependencyInjection(FragmentComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

        @Override
    public void openDialog(int idUser, String nama, int status, int role, String password, String nik, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(nama);
        builder.setMessage("Silahkan pilih ?");

        // add the buttons
        builder.setPositiveButton("Update", (dialog, which) -> {
            Intent intent = UpdateUserActivity.newIntent(getContext(),idUser,nama,status,role,password,nik);
            startActivityForResult(intent,1);
        });
        builder.setNegativeButton("Delete", (dialog, which) -> {
            dialog.dismiss();
            mViewModel.deleteUser(idUser);
        });
        builder.setNeutralButton("Batal",(dialog, which) -> {
            dialog.dismiss();
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }



    @Override
    public void isSwipeRefresh(boolean swipe) {
        mFragmentUserBinding.swipeLayout.setRefreshing(swipe);

    }

    @Override
    public void isRefresh() {
        mViewModel.fetchusers();
    }

    @Override
    public void onBack() {

    }

    @Override
    public void openCreateUserActivity() {
        startActivity(CreateUserActivity.newIntent(getContext()));
    }

    private void setUp() {
        mFragmentUserBinding.swipeLayout.setOnRefreshListener(() -> mViewModel.fetchusers());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentUserBinding.userRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentUserBinding.userRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentUserBinding.userRecyclerView.setAdapter(mUserAdapter);
    }


    @Override
    public void onRetryClick() {
        mViewModel.fetchusers();
    }
}
