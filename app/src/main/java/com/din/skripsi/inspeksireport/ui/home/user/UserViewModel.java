package com.din.skripsi.inspeksireport.ui.home.user;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi.DeleteInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_user.DeleteUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.user.UserResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;


public class UserViewModel extends BaseViewModel<UserNavigator> implements UserItemViewModel.UserItemViewModelListener {

    private final MutableLiveData<List<UserItemViewModel>> userItemsLiveData;



    public UserViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        userItemsLiveData = new MutableLiveData<>();
//        fetchusers();
    }

    public void fetchusers() {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);
        getCompositeDisposable().add(getDataManager()
                .getAllUserApiCall()
                .map(userResponse -> userResponse.getListDataUser())
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(userResponse -> {
                    userItemsLiveData.setValue(userResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }


    public void deleteUser(int id_user) {
//        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postDeleteUserApiCall(new DeleteUserRequest(id_user))
                .map(deleteUserResponse -> deleteUserResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
//                    setIsLoading(false);
                    getNavigator().isRefresh();
                }, throwable -> {
//                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public LiveData<List<UserItemViewModel>> getUserItemsLiveData() {
        return userItemsLiveData;
    }

    private Single<List<UserItemViewModel>> getViewModelList(List<UserResponse.DataUser> repoList) {
        return Observable.fromIterable(repoList)
                .map(repo -> {
                    // masuk
                    return new UserItemViewModel(
                            repo.getIdUser(), repo.getNamaUser(),
                            repo.getNikUser(),repo.getStatusUser(),repo.getRoleUser(),repo.getPasswordUser(), repo.getCreatedDate(),this);
                }).toList();
    }



    public void toCreateUserClick() {
        getNavigator().openCreateUserActivity();
    }


    public void onBackClick() {
        getNavigator().onBack();
    }


    @Override
    public void onItemClick(int idUser, String nama, int status,int role ,String nik, String password, String createdDate) {
        if(getDataManager().getCurrentUserRole() == 0) {
            try {
                getNavigator().openDialog(idUser, nama, status, role, password, nik, role);
            } catch (Exception e) {
            }
        }

    }
}
