package com.din.skripsi.inspeksireport.data.model.api.approval_inspeksi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class ApprovalInspeksiRequest extends JSONObject {


    @Expose
    @SerializedName("id_inspeksi")
    private int id_inspeksi;

    @Expose
    @SerializedName("id_user")
    private int id_user;

    @Expose
    @SerializedName("unique_id_inspeksi")
    private String unique_id_inspeksi;

    @Expose
    @SerializedName("status_report")
    private int status_report;





    public ApprovalInspeksiRequest(String unique_id_inspeksi,int id_user,int id_inspeksi,int status_report) {
        this.id_user = id_user;
        this.unique_id_inspeksi = unique_id_inspeksi;
        this.id_inspeksi = id_inspeksi;
        this.status_report = status_report;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        ApprovalInspeksiRequest that = (ApprovalInspeksiRequest) object;



        return id_inspeksi == 0 ? id_inspeksi == that.id_inspeksi : that.id_inspeksi == 0;

    }



    public Integer getIdInspeksi() {
        return id_inspeksi;
    }
    public Integer getIdUser() {
        return id_user;
    }
    public Integer getStatusReport() {
        return status_report;
    }
    public String getUniqueIdInspeksi() {
        return unique_id_inspeksi;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_inspeksi", getIdInspeksi());
            jsonObject.put("status_report", getStatusReport());
            jsonObject.put("id_user", getIdUser());
            jsonObject.put("unique_id_inspeksi", getUniqueIdInspeksi());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, ApprovalInspeksiRequest.class);
    }

}
