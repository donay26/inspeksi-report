package com.din.skripsi.inspeksireport.ui.update_rekomendasi;

public interface UpdateRekomendasiNavigator {

    void handleError(Throwable throwable);

    void updateRekomendasi();

    void openMainActivity();

}
