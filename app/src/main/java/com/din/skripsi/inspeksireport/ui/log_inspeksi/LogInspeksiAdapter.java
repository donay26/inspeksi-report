package com.din.skripsi.inspeksireport.ui.log_inspeksi;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.din.skripsi.inspeksireport.databinding.ItemLogInspeksiEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemLogInspeksiViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class LogInspeksiAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<LogInspeksiItemViewModel> mLogInspeksiResponseList;

    private LogInspeksiAdapter.LogInspeksiAdapterListener mListener;

    public LogInspeksiAdapter() {
        this.mLogInspeksiResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mLogInspeksiResponseList.isEmpty()) {
            return mLogInspeksiResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mLogInspeksiResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemLogInspeksiViewBinding itemLogInspeksiViewBinding = ItemLogInspeksiViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new LogInspeksiAdapter.LogInspeksiViewHolder(itemLogInspeksiViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemLogInspeksiEmptyViewBinding emptyViewBinding = ItemLogInspeksiEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new LogInspeksiAdapter.EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<LogInspeksiItemViewModel> repoList) {
        mLogInspeksiResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mLogInspeksiResponseList.clear();
    }

    public void setListener(LogInspeksiAdapter.LogInspeksiAdapterListener listener) {
        this.mListener = listener;
    }

    public interface LogInspeksiAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements LogInspeksiEmptyItemViewModel.LogInspeksiEmptyItemViewModelListener {

        private final ItemLogInspeksiEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemLogInspeksiEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            LogInspeksiEmptyItemViewModel emptyItemViewModel = new LogInspeksiEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class LogInspeksiViewHolder extends BaseViewHolder {

        private final ItemLogInspeksiViewBinding mBinding;

        public LogInspeksiViewHolder(ItemLogInspeksiViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final LogInspeksiItemViewModel mLogInspeksiItemViewModel = mLogInspeksiResponseList.get(position);

            mBinding.setViewModel(mLogInspeksiItemViewModel);
            mBinding.titleTextView.setText(CommonUtils.convertRoleString(Integer.parseInt(mLogInspeksiItemViewModel._role.get())));
            mBinding.contentTextView.setText(mLogInspeksiItemViewModel._note.get());
            mBinding.dateTextView.setText(CommonUtils.convertTimeStamp(mLogInspeksiItemViewModel._createdDate.get()));


            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}
