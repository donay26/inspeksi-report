package com.din.skripsi.inspeksireport.ui.home;

public interface MainNavigator {
    void handleError(Throwable throwable);

    void openLoginActivity();

}
