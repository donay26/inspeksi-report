package com.din.skripsi.inspeksireport.data.model.api.delete_product;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class DeleteProductRequest extends JSONObject {


    @Expose
    @SerializedName("id_product")
    private int id_product;




    public DeleteProductRequest(int id_product) {
        this.id_product = id_product;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DeleteProductRequest that = (DeleteProductRequest) object;



        return id_product == 0 ? id_product == that.id_product : that.id_product == 0;

    }


    @Override
    public int hashCode() {
        int result = id_product != 0 ? id_product : 0;
        return result;
    }

    public Integer getIdProduct() {
        return id_product;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_product", getIdProduct());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, DeleteProductRequest.class);
    }

}
