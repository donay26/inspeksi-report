package com.din.skripsi.inspeksireport.ui.home.history;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.din.skripsi.inspeksireport.databinding.ItemHistoryInspeksiEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemHistoryInspeksiViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<HistoryItemViewModel> mHistoryInspeksiResponseList;

    private HistoryInspeksiAdapterListener mListener;

    public HistoryAdapter() {
        this.mHistoryInspeksiResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mHistoryInspeksiResponseList.isEmpty()) {
            return mHistoryInspeksiResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mHistoryInspeksiResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemHistoryInspeksiViewBinding openSourceViewBinding = ItemHistoryInspeksiViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new HistoryAdapter.HistoryInspeksiViewHolder(openSourceViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemHistoryInspeksiEmptyViewBinding emptyViewBinding = ItemHistoryInspeksiEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new HistoryAdapter.EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<HistoryItemViewModel> repoList) {
        mHistoryInspeksiResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mHistoryInspeksiResponseList.clear();
    }

    public void setListener(HistoryAdapter.HistoryInspeksiAdapterListener listener) {
        this.mListener = listener;
    }

    public interface HistoryInspeksiAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements HistoryEmptyItemViewModel.HistoryEmptyItemViewModelListener {

        private final ItemHistoryInspeksiEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemHistoryInspeksiEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            HistoryEmptyItemViewModel emptyItemViewModel = new HistoryEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class HistoryInspeksiViewHolder extends BaseViewHolder {

        private final ItemHistoryInspeksiViewBinding mBinding;

        public HistoryInspeksiViewHolder(ItemHistoryInspeksiViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final HistoryItemViewModel mHistoryInspeksiItemViewModel = mHistoryInspeksiResponseList.get(position);

            mBinding.setViewModel(mHistoryInspeksiItemViewModel);

            mBinding.titleTextView.setText(mHistoryInspeksiResponseList.get(position)._nomorSurat.get()+ " - "+ CommonUtils.convertStatus(mHistoryInspeksiResponseList.get(position)._statusReport.get()));
            mBinding.dateTextView.setText(CommonUtils.convertTimeStamp(mHistoryInspeksiResponseList.get(position)._createdDate.get()));

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}