
package com.din.skripsi.inspeksireport.data.model.api.update_perusahaan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatePerusahaanResponse {

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("status_code")
    private String statusCode;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UpdatePerusahaanResponse)) {
            return false;
        }

        UpdatePerusahaanResponse that = (UpdatePerusahaanResponse) o;

        if (!statusCode.equals(that.statusCode)) {
            return false;
        }

        return message.equals(that.message);
    }

    @Override
    public int hashCode() {
        int result = statusCode.hashCode();
        result = 31 * result + message.hashCode();
        return result;
    }

    public String getMessage() {
        return message;
    }

    public String getStatusCode() {
        return statusCode;
    }
}
