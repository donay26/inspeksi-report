package com.din.skripsi.inspeksireport.ui.home.product;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;


import com.din.skripsi.inspeksireport.databinding.ItemProductEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemProductViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;
import com.din.skripsi.inspeksireport.utils.CommonUtils;


import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<ProductItemViewModel> mProductResponseList;

    private ProductAdapter.ProductAdapterListener mListener;

    public ProductAdapter() {
        this.mProductResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mProductResponseList.isEmpty()) {
            return mProductResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mProductResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemProductViewBinding productViewBinding = ItemProductViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new ProductAdapter.ProductViewHolder(productViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemProductEmptyViewBinding emptyViewBinding = ItemProductEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new ProductAdapter.EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<ProductItemViewModel> repoList) {
        mProductResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mProductResponseList.clear();
    }

    public void setListener(ProductAdapter.ProductAdapterListener listener) {
        this.mListener = listener;
    }

    public interface ProductAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements ProductEmptyItemViewModel.ProductEmptyItemViewModelListener {

        private final ItemProductEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemProductEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            ProductEmptyItemViewModel emptyItemViewModel = new ProductEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class ProductViewHolder extends BaseViewHolder {

        private final ItemProductViewBinding mBinding;

        public ProductViewHolder(ItemProductViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final ProductItemViewModel mProductItemViewModel = mProductResponseList.get(position);

            mBinding.setViewModel(mProductItemViewModel);
            mBinding.createdDateTextView.setText(CommonUtils.convertTimeStamp(mProductResponseList.get(position)._createdDateProduct.get()));

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}