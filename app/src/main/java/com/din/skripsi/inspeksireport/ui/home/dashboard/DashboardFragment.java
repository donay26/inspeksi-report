package com.din.skripsi.inspeksireport.ui.home.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.FragmentDashboardBinding;
import com.din.skripsi.inspeksireport.inject.component.FragmentComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseFragment;
import com.din.skripsi.inspeksireport.ui.create_inspeksi.CreateInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.DetailInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.DetailInspeksiActivity_MembersInjector;

import javax.inject.Inject;

public class DashboardFragment extends BaseFragment<FragmentDashboardBinding, DashboardViewModel> implements DashboardNavigator,InspeksiAdapter.InspeksiAdapterListener {

    public static final String TAG = "DashboardFragment";


    FragmentDashboardBinding mFragmentDashboardBinding;
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    InspeksiAdapter mInspeksiAdapter;


    public static DashboardFragment newInstance() {
        Bundle args = new Bundle();
        DashboardFragment fragment = new DashboardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_dashboard;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
        mInspeksiAdapter.setListener(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentDashboardBinding = getViewDataBinding();
        mViewModel.fetchInspeksis();
        mViewModel.onSetRole();

        if(mViewModel.getUserRole().get() == 0){
            mFragmentDashboardBinding.fab.show();
        } else if(mViewModel.getUserRole().get() == 3) {
            mFragmentDashboardBinding.fab.show();
        }else {
            mFragmentDashboardBinding.fab.hide();
        }

        setUp();
    }


    @Override
    public void performDependencyInjection(FragmentComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void openDetail(String idUnique,int idInspeksi, int statusReport, int idProduct, String namaProduct, String nomorSurat, String serialNumber, String namaPerusahaan, String kodePerusahaan, String alamatPerusahaan, String note, String image_name, String createdDate,String updatedDate) {
        Intent intent = DetailInspeksiActivity.newIntent(getContext(),idUnique,idInspeksi,statusReport,idProduct,namaProduct,nomorSurat,namaPerusahaan,kodePerusahaan,serialNumber,note,alamatPerusahaan,createdDate,updatedDate,image_name);
        startActivity(intent);

    }


    @Override
    public void isSwipeRefresh(boolean swipe) {
        mFragmentDashboardBinding.swipe.setRefreshing(swipe);
    }

    @Override
    public void onBack() {

    }

    @Override
    public void openCreateInspeksiActivity() {
        startActivity(CreateInspeksiActivity.newIntent(getContext(),mViewModel.getTotal().get()));
    }

    private void setUp() {
        mFragmentDashboardBinding.swipe.setOnRefreshListener(() -> mViewModel.fetchInspeksis());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentDashboardBinding.dashboardRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentDashboardBinding.dashboardRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentDashboardBinding.dashboardRecyclerView.setAdapter(mInspeksiAdapter);
    }


    @Override
    public void onRetryClick() {
        mViewModel.fetchInspeksis();
    }


}
