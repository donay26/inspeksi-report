package com.din.skripsi.inspeksireport.ui.update_status;

public interface UpdateStatusNavigator {

    void handleError(Throwable throwable);

    void updateStatus();

    void openMainActivity();

}
