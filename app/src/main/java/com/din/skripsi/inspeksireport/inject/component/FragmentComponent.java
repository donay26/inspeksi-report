package com.din.skripsi.inspeksireport.inject.component;




import com.din.skripsi.inspeksireport.inject.module.FragmentModule;
import com.din.skripsi.inspeksireport.inject.scope.FragmentScope;
import com.din.skripsi.inspeksireport.ui.home.about.AboutFragment;
import com.din.skripsi.inspeksireport.ui.home.dashboard.DashboardFragment;
import com.din.skripsi.inspeksireport.ui.home.history.HistoryFragment;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanFragment;
import com.din.skripsi.inspeksireport.ui.home.product.ProductFragment;
import com.din.skripsi.inspeksireport.ui.home.rekomendasi.RekomendasiFragment;
import com.din.skripsi.inspeksireport.ui.home.status.StatusFragment;
import com.din.skripsi.inspeksireport.ui.home.user.UserFragment;

import dagger.Component;

@FragmentScope
@Component(modules = FragmentModule.class, dependencies = AppComponent.class)
public interface FragmentComponent {
    void inject(AboutFragment fragment);
    void inject(DashboardFragment fragment);
    void inject(UserFragment fragment);
    void inject(ProductFragment fragment);
    void inject(HistoryFragment fragment);
    void inject(RekomendasiFragment fragment);
    void inject(StatusFragment fragment);
    void inject(PerusahaanFragment fragment);
}
