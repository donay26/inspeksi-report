package com.din.skripsi.inspeksireport.ui.detail_inspeksi;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.din.skripsi.inspeksireport.databinding.ItemInspeksiEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemInspeksiViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemItemInspeksiEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemItemInspeksiViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class ItemInspeksiAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<ItemInspeksiItemViewModel> mItemInspeksiResponseList;

    private ItemInspeksiAdapterListener mListener;

    public ItemInspeksiAdapter() {
        this.mItemInspeksiResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mItemInspeksiResponseList.isEmpty()) {
            return mItemInspeksiResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mItemInspeksiResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemItemInspeksiViewBinding openSourceViewBinding = ItemItemInspeksiViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new ItemInspeksiAdapter.ItemItemInspeksiViewHolder(openSourceViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemItemInspeksiEmptyViewBinding emptyViewBinding = ItemItemInspeksiEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new ItemInspeksiAdapter.EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<ItemInspeksiItemViewModel> repoList) {
        mItemInspeksiResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mItemInspeksiResponseList.clear();
    }

    public void setListener(ItemInspeksiAdapter.ItemInspeksiAdapterListener listener) {
        this.mListener = listener;
    }

    public interface ItemInspeksiAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements ItemInspeksiEmptyItemViewModel.ItemInspeksiEmptyItemViewModelListener {

        private final ItemItemInspeksiEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemItemInspeksiEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            ItemInspeksiEmptyItemViewModel emptyItemViewModel = new ItemInspeksiEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class ItemItemInspeksiViewHolder extends BaseViewHolder {

        private final ItemItemInspeksiViewBinding mBinding;

        public ItemItemInspeksiViewHolder(ItemItemInspeksiViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final ItemInspeksiItemViewModel mItemInspeksiItemViewModel = mItemInspeksiResponseList.get(position);

            mBinding.setViewModel(mItemInspeksiItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}