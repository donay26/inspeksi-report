package com.din.skripsi.inspeksireport.data.model.api.item_inspeksi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class ItemInspeksiRequest extends JSONObject {

    @Expose
    @SerializedName("id_inspeksi")
    private String id_inspeksi;


    public ItemInspeksiRequest(String id_inspeksi) {
        this.id_inspeksi = id_inspeksi;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        ItemInspeksiRequest that = (ItemInspeksiRequest) object;

        
        return id_inspeksi == "" ? id_inspeksi == that.id_inspeksi : that.id_inspeksi == "";

    }


    @Override
    public int hashCode() {
        int result = id_inspeksi.hashCode() != 0 ? id_inspeksi.hashCode() : 0;
        return result;
    }

    public String getIdInspeksi() {
        return id_inspeksi;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_inspeksi", getIdInspeksi());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, ItemInspeksiRequest.class);
    }

}
