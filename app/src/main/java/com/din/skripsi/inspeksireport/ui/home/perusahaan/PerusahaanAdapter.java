package com.din.skripsi.inspeksireport.ui.home.perusahaan;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.din.skripsi.inspeksireport.databinding.ItemPerusahaanEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemPerusahaanViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanEmptyItemViewModel;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanItemViewModel;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class PerusahaanAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<PerusahaanItemViewModel> mPerusahaanResponseList;

    private PerusahaanAdapter.PerusahaanAdapterListener mListener;

    public PerusahaanAdapter() {
        this.mPerusahaanResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mPerusahaanResponseList.isEmpty()) {
            return mPerusahaanResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mPerusahaanResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemPerusahaanViewBinding perusahaanViewBinding = ItemPerusahaanViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new PerusahaanAdapter.PerusahaanViewHolder(perusahaanViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemPerusahaanEmptyViewBinding emptyViewBinding = ItemPerusahaanEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new PerusahaanAdapter.EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<PerusahaanItemViewModel> repoList) {
        mPerusahaanResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mPerusahaanResponseList.clear();
    }

    public void setListener(PerusahaanAdapter.PerusahaanAdapterListener listener) {
        this.mListener = listener;
    }

    public interface PerusahaanAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements PerusahaanEmptyItemViewModel.PerusahaanEmptyItemViewModelListener {

        private final ItemPerusahaanEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemPerusahaanEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            PerusahaanEmptyItemViewModel emptyItemViewModel = new PerusahaanEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class PerusahaanViewHolder extends BaseViewHolder {

        private final ItemPerusahaanViewBinding mBinding;

        public PerusahaanViewHolder(ItemPerusahaanViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final PerusahaanItemViewModel mPerusahaanItemViewModel = mPerusahaanResponseList.get(position);

            mBinding.setViewModel(mPerusahaanItemViewModel);

            mBinding.createdDateTextView.setText(CommonUtils.convertTimeStamp(mPerusahaanResponseList.get(position)._createdDatePerusahaan.get()));

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}