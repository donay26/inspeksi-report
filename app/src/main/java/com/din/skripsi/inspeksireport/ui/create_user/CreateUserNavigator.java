package com.din.skripsi.inspeksireport.ui.create_user;

public interface CreateUserNavigator {
    void handleError(Throwable throwable);

    void createUser();

    void openMainActivity();

}
