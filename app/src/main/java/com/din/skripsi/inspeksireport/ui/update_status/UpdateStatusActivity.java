package com.din.skripsi.inspeksireport.ui.update_status;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityUpdateStatusBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.ui.update_status.UpdateStatusActivity;
import com.din.skripsi.inspeksireport.ui.update_status.UpdateStatusNavigator;
import com.din.skripsi.inspeksireport.ui.update_status.UpdateStatusViewModel;

public class UpdateStatusActivity extends BaseActivity<ActivityUpdateStatusBinding, UpdateStatusViewModel> implements UpdateStatusNavigator {

    private ActivityUpdateStatusBinding mActivityUpdateStatusBinding;

    private static final String PREF_KEY_ID_STATUS = "PREF_KEY_CURRENT_ID_STATUS";

    private static final String PREF_KEY_NAMA = "PREF_KEY_CURRENT_NAMA";



    public static Intent newIntent(Context context,
                                   int idStatus,
                                   String nama
    ) {
        Intent intent = new Intent(context, UpdateStatusActivity.class);
        intent.putExtra(PREF_KEY_ID_STATUS, idStatus);
        intent.putExtra(PREF_KEY_NAMA, nama);
        return intent;
    }


    public Integer getPrefKeyIdStatus() {
        Bundle intent = getIntent().getExtras();
        Integer id_status = intent.getInt(PREF_KEY_ID_STATUS, 0);
        return id_status;
    }


    public String getPrefKeyNama() {
        Bundle intent = getIntent().getExtras();
        String nama = intent.getString(PREF_KEY_NAMA);
        return nama;
    }



    public static Intent newIntent(Context context) {
        return new Intent(context, UpdateStatusActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_status;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityUpdateStatusBinding = getViewDataBinding();
        mViewModel.setNavigator(this);

        setSupportActionBar(mActivityUpdateStatusBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUp();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }


    private  void setUp(){
        mActivityUpdateStatusBinding.etNama.setText(getPrefKeyNama());


    }



    @Override
    public void updateStatus() {

        String namaStatus = mActivityUpdateStatusBinding.etNama.getText().toString();

        if (mViewModel.isValid(namaStatus)) {
            hideKeyboard();
            mViewModel.updateStatus(getPrefKeyIdStatus(),namaStatus);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(UpdateStatusActivity.this);
        startActivity(intent);
        finish();
    }





}