package com.din.skripsi.inspeksireport.data.model.api.create_inspeksi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class CreateInspeksiRequest extends JSONObject {

    @Expose
    @SerializedName("nomor_surat")
    private String nomor_surat;

    @Expose
    @SerializedName("id_user")
    private Integer id_user;

    @Expose
    @SerializedName("id_perusahaan")
    private Integer id_perusahaan;

    @Expose
    @SerializedName("unique_id_inspeksi")
    private String unique_id_inspeksi;

    @Expose
    @SerializedName("serial_number")
    private String serial_number;

    @Expose
    @SerializedName("note")
    private String note;

    @Expose
    @SerializedName("image")
    private String image;

    @Expose
    @SerializedName("id_product")
    private Integer id_product;



    public CreateInspeksiRequest(Integer id_user,String nomor_surat, Integer id_perusahaan,String unique_id_inspeksi, String serial_number, String note, Integer id_product,String image) {
        this.id_user = id_user;
        this.nomor_surat = nomor_surat;
        this.id_perusahaan = id_perusahaan;
        this.unique_id_inspeksi = unique_id_inspeksi;
        this.serial_number = serial_number;
        this.note = note;
        this.image = image;
        this.id_product = id_product;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        CreateInspeksiRequest that = (CreateInspeksiRequest) object;



        return nomor_surat == "" ? nomor_surat == that.nomor_surat : that.nomor_surat == "";

    }


    @Override
    public int hashCode() {
        int result = nomor_surat.hashCode() != 0 ? nomor_surat.hashCode() : 0;
        return result;
    }

    public String getNomorSurat() {
        return nomor_surat;
    }
    public String getImage() {
        return image;
    }
    public Integer getIdUser() {
        return id_user;
    }
    public Integer getIdPerusahaan() {
        return id_perusahaan;
    }
    public String getUniqueIdInspeksi() {
        return unique_id_inspeksi;
    }
    public String getSerialNumber() {
        return serial_number;
    }
    public String getNote() {
        return note;
    }
    public Integer getIdProduct() {
        return id_product;
    }



    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nomor_surat", getNomorSurat());
            jsonObject.put("id_user", getIdUser());
            jsonObject.put("id_perusahaan", getIdPerusahaan());
            jsonObject.put("unique_id_inspeksi", getUniqueIdInspeksi());
            jsonObject.put("image", getImage());
            jsonObject.put("serial_number", getSerialNumber());
            jsonObject.put("note", getNote());
            jsonObject.put("status_report", 0);
            jsonObject.put("id_product", getIdProduct());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, CreateInspeksiRequest.class);
    }

}
