package com.din.skripsi.inspeksireport.ui.detail_inspeksi;


import static com.din.skripsi.inspeksireport.data.remote.ApiEndPoint.BASE_URL;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityDetailInspeksiBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.create_item_inspeksi.CreateItemInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.ui.pdf_inspeksi.PdfInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.update_inspeksi.UpdateInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.update_item_inspeksi.UpdateItemInspeksiActivity;
import com.din.skripsi.inspeksireport.utils.CommonUtils;


import javax.inject.Inject;


public class  DetailInspeksiActivity extends BaseActivity<ActivityDetailInspeksiBinding, DetailInspeksiViewModel> implements DetailInspeksiNavigator {

    private ActivityDetailInspeksiBinding mActivityDetailInspeksiBinding;

    private static final String PREF_KEY_ID_INSPEKSI = "PREF_KEY_CURRENT_ID_INSPEKSI";

    private static final String PREF_KEY_ID_UNIQUE_INSPEKSI = "PREF_KEY_CURRENT_ID_UNIQUE_INSPEKSI";

    private static final String PREF_KEY_NOMOR_SURAT = "PREF_KEY_CURRENT_ID_NOMOR_SURAT";

    private static final String PREF_KEY_ID_PRODUCT = "PREF_KEY_CURRENT_ID_PRODUCT";

    private static final String PREF_KEY_NAMA_PRODUCT = "PREF_KEY_CURRENT_NAMA_PRODUCT";

    private static final String PREF_KEY_NAMA_PERUSAHAAN = "PREF_KEY_CURRENT_NAMA_PERUSAHAAN";

    private static final String PREF_KEY_KODE_PERUSAHAAN = "PREF_KEY_CURRENT_KODE_PERUSAHAAN";

    private static final String PREF_KEY_SERIAL_NUMBER = "PREF_KEY_CURRENT_SERIAL_NUMBER";

    private static final String PREF_KEY_NOTE = "PREF_KEY_CURRENT_NOTE";

    private static final String PREF_KEY_STATUS_REPORT = "PREF_KEY_CURRENT_STATUS_REPORT";

    private static final String PREF_KEY_ALAMAT_PERUSAHAAN = "PREF_KEY_CURRENT_ALAMAT_PERUSAHAAN";

    private static final String PREF_KEY_CREATED_DATE = "PREF_KEY_CURRENT_CREATED_DATE";

    private static final String PREF_KEY_UPDATED_DATE = "PREF_KEY_CURRENT_UPDATED_DATE";

    private static final String PREF_KEY_IMAGE_NAME = "PREF_KEY_CURRENT_IMAGE_NAME";


    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    ItemInspeksiAdapter itemInspeksiAdapter;

    private int status_report = 0;


    public static Intent newIntent(Context context,
                                   String idUniqueInspeksi,
                                   int idInspeksi,
                                   int statusReport,
                                   int idProduct,
                                   String namaProduct,
                                   String nomorSurat,
                                   String namaPerusahaan,
                                   String kodePerusahaan,
                                   String serialNumber,
                                   String note,
                                   String alamatPerusahaan,
                                   String createDate,
                                   String updateDate,
                                   String imageName) {
        Intent intent = new Intent(context, DetailInspeksiActivity.class);
        intent.putExtra(PREF_KEY_ID_UNIQUE_INSPEKSI, idUniqueInspeksi);
        intent.putExtra(PREF_KEY_ID_INSPEKSI, idInspeksi);
        intent.putExtra(PREF_KEY_ID_PRODUCT, idProduct);
        intent.putExtra(PREF_KEY_NAMA_PRODUCT, namaProduct);
        intent.putExtra(PREF_KEY_NOMOR_SURAT, nomorSurat);
        intent.putExtra(PREF_KEY_NAMA_PERUSAHAAN, namaPerusahaan);
        intent.putExtra(PREF_KEY_KODE_PERUSAHAAN, kodePerusahaan);
        intent.putExtra(PREF_KEY_SERIAL_NUMBER, serialNumber);
        intent.putExtra(PREF_KEY_NOTE, note);
        intent.putExtra(PREF_KEY_STATUS_REPORT, statusReport);
        intent.putExtra(PREF_KEY_ALAMAT_PERUSAHAAN, alamatPerusahaan);
        intent.putExtra(PREF_KEY_CREATED_DATE, createDate);
        intent.putExtra(PREF_KEY_UPDATED_DATE, updateDate);
        intent.putExtra(PREF_KEY_IMAGE_NAME, imageName);
        return intent;
    }

    public String getPrefKeyIdUniqueInspeksi() {
        Bundle intent = getIntent().getExtras();
        String id_unique_inspeksi = intent.getString(PREF_KEY_ID_UNIQUE_INSPEKSI );
        return id_unique_inspeksi;
    }

    public Integer getPrefKeyIdInspeksi() {
        Bundle intent = getIntent().getExtras();
        Integer id_inspeksi = intent.getInt(PREF_KEY_ID_INSPEKSI, 0);
        return id_inspeksi;
    }

    public Integer getPrefKeyIdProduct() {
        Bundle intent = getIntent().getExtras();
        Integer id_product = intent.getInt(PREF_KEY_ID_PRODUCT, 0);
        return id_product;
    }

    public Integer getPrefStatusReport() {
        Bundle intent = getIntent().getExtras();
        status_report = intent.getInt(PREF_KEY_STATUS_REPORT, 0);
        return status_report;
    }

    public String getPrefKeyNamaProduct() {
        Bundle intent = getIntent().getExtras();
        String nama_product = intent.getString(PREF_KEY_NAMA_PRODUCT);
        return nama_product;
    }

    public String getPrefKeyNomorSurat() {
        Bundle intent = getIntent().getExtras();
        String nomor_surat = intent.getString(PREF_KEY_NOMOR_SURAT);
        return nomor_surat;
    }

    public String getPrefKeyNamaPerusahaan() {
        Bundle intent = getIntent().getExtras();
        String nama_perusahaan = intent.getString(PREF_KEY_NAMA_PERUSAHAAN);
        return nama_perusahaan;
    }
    public String getPrefKeyKodePerusahaan() {
        Bundle intent = getIntent().getExtras();
        String kode_perusahaan = intent.getString(PREF_KEY_KODE_PERUSAHAAN);
        return kode_perusahaan;
    }
    public String getPrefKeyImageName() {
        Bundle intent = getIntent().getExtras();
        String image_name = intent.getString(PREF_KEY_IMAGE_NAME);
        return image_name;
    }

    public String getPrefKeyAlamatPerusahaan() {
        Bundle intent = getIntent().getExtras();
        String alamat_perusahaan = intent.getString(PREF_KEY_ALAMAT_PERUSAHAAN);
        return alamat_perusahaan;
    }

    public String getPrefKeySerialNumber() {
        Bundle intent = getIntent().getExtras();
        String serial_number = intent.getString(PREF_KEY_SERIAL_NUMBER);
        Log.d("SERIAL_NUMBER",serial_number);
        return serial_number;
    }

    public String getPrefKeyNote() {
        Bundle intent = getIntent().getExtras();
        String note = intent.getString(PREF_KEY_NOTE);
        return note;
    }

    public String getPrefKeyCreatedDate() {
        Bundle intent = getIntent().getExtras();
        String created_date = intent.getString(PREF_KEY_CREATED_DATE);
        return created_date;
    }

    public String getPrefKeyUpdatedDate() {
        Bundle intent = getIntent().getExtras();
        String update_date = intent.getString(PREF_KEY_UPDATED_DATE);
        return update_date;
    }




    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_detail_inspeksi;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityDetailInspeksiBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setSupportActionBar(mActivityDetailInspeksiBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mViewModel.onNavMenuCreated("http://192.168.0.109:3000/images/"+getPrefKeyImageName());
        mViewModel.fetchItemInspeksis(getPrefKeyIdInspeksi());
        Log.d("status" , getPrefStatusReport().toString());
        setUp();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("masuk","1");
        if (requestCode == 1) {
            Log.d("masuk","2");

            if(resultCode == RESULT_OK) {
                Log.d("masuk","3");
                int id_inspeksi = data.getIntExtra(CreateItemInspeksiActivity.PREF_KEY_ID_INSPEKSI,0);
                // TODO Update your TextView.
                mViewModel.fetchItemInspeksis(id_inspeksi);
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    private void setUp() {
        mActivityDetailInspeksiBinding.swipeLayout.setOnRefreshListener(() -> mViewModel.fetchItemInspeksis(getPrefKeyIdInspeksi()));
        mActivityDetailInspeksiBinding.tvStatus.setText(CommonUtils.convertStatus(status_report));
        mActivityDetailInspeksiBinding.tvNomorSurat.setText(getPrefKeyNomorSurat());
        mActivityDetailInspeksiBinding.tvNamaPerusahaan.setText(getPrefKeyNamaPerusahaan());
        mActivityDetailInspeksiBinding.tvAlamatPerusahaan.setText(getPrefKeyAlamatPerusahaan());
        mActivityDetailInspeksiBinding.tvNote.setText(getPrefKeyNote());
        mActivityDetailInspeksiBinding.tvProduct.setText(getPrefKeyNamaProduct());
        mActivityDetailInspeksiBinding.tvSerialNumber.setText(getPrefKeySerialNumber());
        mActivityDetailInspeksiBinding.tvTanggalPembuatan.setText(getPrefKeyCreatedDate());
        mActivityDetailInspeksiBinding.tvValidDate.setText(CommonUtils.convertTwoWeekTimeStamp(getPrefKeyUpdatedDate()));
        mActivityDetailInspeksiBinding.tvKodePerusahaan.setText(getPrefKeyKodePerusahaan());

        Glide.with(this).load(BASE_URL+"images/"+getPrefKeyImageName()).into(mActivityDetailInspeksiBinding.coverImageView);


        if(getPrefStatusReport() !=  4 ){
            // NEW

            if (mViewModel.getUserRole().get() == 0) {
                mActivityDetailInspeksiBinding.llUpdateInspeksi.setVisibility(View.VISIBLE);
                mActivityDetailInspeksiBinding.llAddItemInspeksi.setVisibility(View.VISIBLE);
                mActivityDetailInspeksiBinding.llAprovalInspeksi.setVisibility(View.VISIBLE);

            }else if (mViewModel.getUserRole().get() == 1) {
                // Manager
                mActivityDetailInspeksiBinding.llAprovalInspeksi.setVisibility(View.VISIBLE);
            } else if (mViewModel.getUserRole().get() == 2) {
                // lab
                mActivityDetailInspeksiBinding.llUpdateInspeksi.setVisibility(View.VISIBLE);
                mActivityDetailInspeksiBinding.llAddItemInspeksi.setVisibility(View.VISIBLE);
                mActivityDetailInspeksiBinding.btnDeleteItemInspeksi.setVisibility(View.GONE);
                mActivityDetailInspeksiBinding.llAlamatPerusahaan.setVisibility(View.GONE);
                mActivityDetailInspeksiBinding.llNamaPerusahaan.setVisibility(View.GONE);
                mActivityDetailInspeksiBinding.btnRejectItemInspeksi.setVisibility(View.GONE);
                mActivityDetailInspeksiBinding.btnUpdateItemInspeksi.setVisibility(View.GONE);
                mActivityDetailInspeksiBinding.llUpdateInspeksiSecondary.setVisibility(View.GONE);
                mActivityDetailInspeksiBinding.btnPrintInspeksi.setVisibility(View.GONE);

            } else if (mViewModel.getUserRole().get() == 3) {
                mActivityDetailInspeksiBinding.llUpdateInspeksiSecondary.setVisibility(View.VISIBLE);
                mActivityDetailInspeksiBinding.btnPrintInspeksi.setVisibility(View.VISIBLE);

                // Admin
            }





        }/*else if(getPrefStatusReport() ==  1 ){
            // APPROVE
            mActivityDetailInspeksiBinding.llUpdateInspeksi.setVisibility(View.VISIBLE);
        }else if(getPrefStatusReport() ==  2 ){
            // REJECT
            mActivityDetailInspeksiBinding.llUpdateInspeksi.setVisibility(View.VISIBLE);
        }else if(getPrefStatusReport() ==  3 ){
            // PENDING
            mActivityDetailInspeksiBinding.llAddItemInspeksi.setVisibility(View.VISIBLE);
        }else if(getPrefStatusReport() ==  4 ){
            // PENDING
            mActivityDetailInspeksiBinding.llAddItemInspeksi.setVisibility(View.VISIBLE);
        }*/

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mActivityDetailInspeksiBinding.inspeksiRecyclerView.setLayoutManager(mLayoutManager);
        mActivityDetailInspeksiBinding.inspeksiRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mActivityDetailInspeksiBinding.inspeksiRecyclerView.setAdapter(itemInspeksiAdapter);
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {
        status_report  = getPrefStatusReport();

    }

    @Override
    public void openCreateItemInspeksi() {
        Intent intent = CreateItemInspeksiActivity.newIntent(DetailInspeksiActivity.this,getPrefKeyIdInspeksi(),getPrefKeyNomorSurat());
        startActivityForResult(intent,1);
    }

    @Override
    public void deleteInspeksi() {
        mViewModel.deleteInspeksi(getPrefKeyIdInspeksi());
    }

    @Override
    public void printInspeksi() {
        Intent intent = PdfInspeksiActivity.newIntent(getBaseContext(),
                getPrefKeyIdUniqueInspeksi(),
                getPrefKeyIdInspeksi(),
                0,
                getPrefKeyIdProduct(),
                getPrefKeyNamaProduct(),
                getPrefKeyNomorSurat(),
                getPrefKeyNamaPerusahaan(),
                getPrefKeyKodePerusahaan(),
                getPrefKeySerialNumber(),

                getPrefKeyNote(),getPrefKeyAlamatPerusahaan(),getPrefKeyCreatedDate(),getPrefKeyUpdatedDate(),"",mViewModel.jsonResponse.get());
        startActivity(intent);
    }

    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(DetailInspeksiActivity.this);
        startActivity(intent);
        finish();

    }


    @Override
    public void openUpdateInspeksi() {
        Intent intent = UpdateInspeksiActivity.newIntent(DetailInspeksiActivity.this,
                getPrefKeyIdUniqueInspeksi(),
                getPrefKeyIdInspeksi(),
                getPrefStatusReport(),
                getPrefKeyIdProduct(),
                getPrefKeyNamaProduct(),
                getPrefKeyNomorSurat(),
                getPrefKeyKodePerusahaan(),
                getPrefKeyNamaPerusahaan(),
                getPrefKeySerialNumber(),
                getPrefKeyNote(),
                getPrefKeyAlamatPerusahaan(),
                getPrefKeyCreatedDate()
        );
        startActivity(intent);

    }

    @Override
    public void refreshListInspeksi() {
        mViewModel.fetchItemInspeksis(getPrefKeyIdInspeksi());
    }

    @Override
    public void approveInspeksi() {
        mViewModel.statusInspeksi(getPrefKeyIdUniqueInspeksi(),getPrefKeyIdInspeksi(),1);
        status_report  = 1;
        mActivityDetailInspeksiBinding.tvStatus.setText(CommonUtils.convertStatus(status_report));

    }

    @Override
    public void rejectInspeksi() {
        mViewModel.statusInspeksi(getPrefKeyIdUniqueInspeksi(),getPrefKeyIdInspeksi(),2);
        status_report  = 2;
        mActivityDetailInspeksiBinding.tvStatus.setText(CommonUtils.convertStatus(status_report));

    }

    @Override
    public void pendingInspeksi() {
        mViewModel.statusInspeksi(getPrefKeyIdUniqueInspeksi(),getPrefKeyIdInspeksi(),3);
        status_report  = 3;
        mActivityDetailInspeksiBinding.tvStatus.setText(CommonUtils.convertStatus(status_report));

    }

    @Override
    public void doneInspeksi() {
        mViewModel.statusInspeksi(getPrefKeyIdUniqueInspeksi(),getPrefKeyIdInspeksi(),4);
        status_report  = 4;
        mActivityDetailInspeksiBinding.tvStatus.setText(CommonUtils.convertStatus(status_report));
    }


    @Override
    public void isSwipeRefresh(boolean swipe) {
        mActivityDetailInspeksiBinding.swipeLayout.setRefreshing(swipe);
    }

    @Override
    public void openDialogItemInspeksi(int idItemInspeksi, String nama_product, String rekomendasi,Integer id_rekomendasi ,String remarks, String status,Integer id_status ,String created_date) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(nama_product);
        builder.setMessage("Silahkan pilih ?");

        // add the buttons
        builder.setPositiveButton("Update", (dialog, which) -> {
                Intent intent = UpdateItemInspeksiActivity.newIntent(DetailInspeksiActivity.this,getPrefKeyIdInspeksi(),idItemInspeksi,nama_product,id_status,id_rekomendasi,remarks,created_date);
                startActivityForResult(intent,1);
        });
        builder.setNegativeButton("Delete", (dialog, which) -> {
            mViewModel.deleteItemInspeksi(idItemInspeksi);
            dialog.dismiss();
        });
        builder.setNeutralButton("Batal",(dialog, which) -> {
            dialog.dismiss();
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }




}