package com.din.skripsi.inspeksireport.ui.log_inspeksi;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.log_inspeksi.LogInspeksiResponse;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class LogInspeksiViewModel extends BaseViewModel<LogInspeksiNavigator> implements LogInspeksiItemViewModel.LogInspeksiItemViewModelListener {

    private final MutableLiveData<List<LogInspeksiItemViewModel>> logInspeksiItemsLiveData;



    public LogInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        logInspeksiItemsLiveData = new MutableLiveData<>();
//        fetchLogInspeksis();
    }

    public void fetchLogInspeksis() {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);
        getCompositeDisposable().add(getDataManager()
                .getAllLogInspeksiApiCall()
                .map(LogInspeksiResponse -> LogInspeksiResponse.getListItemLogInspeksi())
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(logInspeksiResponse -> {
                    logInspeksiItemsLiveData.setValue(logInspeksiResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public LiveData<List<LogInspeksiItemViewModel>> getLogInspeksiItemsLiveData() {
        return logInspeksiItemsLiveData;
    }

    private Single<List<LogInspeksiItemViewModel>> getViewModelList(List<LogInspeksiResponse.ItemLogInspeksi> repoList) {
        return Observable.fromIterable(repoList)
                .map(repo -> {
                    // masuk
                    return new LogInspeksiItemViewModel(
                            repo.getNama(), repo.getRole(), repo.getNote(), repo.getSerialNumber(), repo.getCreatedDate(), this);
                }).toList();
    }





    public void onBackClick() {
        getNavigator().onBack();
    }

    @Override
    public void onItemClick(String nama, String role, String note, String serial_number, String createdDate) {

    }
}
