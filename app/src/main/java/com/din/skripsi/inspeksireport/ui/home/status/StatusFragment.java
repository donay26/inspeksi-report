package com.din.skripsi.inspeksireport.ui.home.status;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.FragmentStatusBinding;
import com.din.skripsi.inspeksireport.inject.component.FragmentComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseFragment;
import com.din.skripsi.inspeksireport.ui.create_status.CreateStatusActivity;
import com.din.skripsi.inspeksireport.ui.update_status.UpdateStatusActivity;


import javax.inject.Inject;

public class StatusFragment extends BaseFragment<FragmentStatusBinding, StatusViewModel> implements StatusNavigator, StatusAdapter.StatusAdapterListener {

    public static final String TAG = "StatusFragment";


    FragmentStatusBinding mFragmentStatusBinding;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    StatusAdapter mStatusAdapter;


    public static StatusFragment newInstance() {
        Bundle args = new Bundle();
        StatusFragment fragment = new StatusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_status;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
        mStatusAdapter.setListener(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentStatusBinding = getViewDataBinding();
        mViewModel.fetchStatuss();
        setUp();
    }


    @Override
    public void performDependencyInjection(FragmentComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void openDialog(int idStatus, String nama, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(nama);
        builder.setMessage("Silahkan pilih ?");

        // add the buttons
        builder.setPositiveButton("Update", (dialog, which) -> {
            Intent intent = UpdateStatusActivity.newIntent(getContext(),idStatus,nama);
            startActivityForResult(intent,1);
        });
        builder.setNegativeButton("Delete", (dialog, which) -> {
            dialog.dismiss();
            mViewModel.deleteStatus(idStatus);
        });
        builder.setNeutralButton("Batal",(dialog, which) -> {
            dialog.dismiss();
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void isRefresh() {
        mViewModel.fetchStatuss();
    }


    @Override
    public void isSwipeRefresh(boolean swipe) {
        mFragmentStatusBinding.swipeLayout.setRefreshing(swipe);

    }

    @Override
    public void onBack() {

    }

    @Override
    public void openCreateStatusActivity() {
        startActivity(CreateStatusActivity.newIntent(getContext()));

    }

    private void setUp() {
        mFragmentStatusBinding.swipeLayout.setOnRefreshListener(() -> mViewModel.fetchStatuss());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentStatusBinding.productRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentStatusBinding.productRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentStatusBinding.productRecyclerView.setAdapter(mStatusAdapter);
    }


    @Override
    public void onRetryClick() {
        mViewModel.fetchStatuss();
    }

}
