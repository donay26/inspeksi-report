package com.din.skripsi.inspeksireport.data.model.api.update_item_inspeksi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class UpdateItemInspeksiRequest extends JSONObject {


    @Expose
    @SerializedName("id_item_inspeksi")
    private int id_item_inspeksi;

    @Expose
    @SerializedName("nama")
    private String nama;

    @Expose
    @SerializedName("rekomendasi")
    private Integer rekomendasi;

    @Expose
    @SerializedName("remarks")
    private String remarks;

    @Expose
    @SerializedName("status")
    private Integer status;




    public UpdateItemInspeksiRequest(int id_item_inspeksi, String nama, Integer rekomendasi, String remarks, Integer status) {
        this.id_item_inspeksi = id_item_inspeksi;
        this.nama = nama;
        this.rekomendasi = rekomendasi;
        this.remarks = remarks;
        this.status = status;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        UpdateItemInspeksiRequest that = (UpdateItemInspeksiRequest) object;



        return id_item_inspeksi == 0 ? id_item_inspeksi == that.id_item_inspeksi : that.id_item_inspeksi == 0;

    }


    @Override
    public int hashCode() {
        int result = nama.hashCode() != 0 ? nama.hashCode() : 0;
        return result;
    }

    public String getNama() {
        return nama;
    }
    public Integer getRekomendasi() {
        return rekomendasi;
    }
    public String getRemarks() {
        return remarks;
    }
    public Integer getIdItemInspeksi() {
        return id_item_inspeksi;
    }
    public Integer getStatus() {
        return status;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nama", getNama());
            jsonObject.put("id_rekomendasi", getRekomendasi());
            jsonObject.put("remarks", getRemarks());
            jsonObject.put("id_item_inspeksi", getIdItemInspeksi());
            jsonObject.put("id_status", getStatus());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, UpdateItemInspeksiRequest.class);
    }

}
