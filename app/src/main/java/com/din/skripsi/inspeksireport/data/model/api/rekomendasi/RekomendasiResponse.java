package com.din.skripsi.inspeksireport.data.model.api.rekomendasi;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RekomendasiResponse {

    @Expose
    @SerializedName("data")
    private List<DataRekomendasi> listDataRekomendasi;

    @Expose
    @SerializedName("status_code")
    private boolean statusCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RekomendasiResponse)) {
            return false;
        }

        RekomendasiResponse that = (RekomendasiResponse) o;

        if (statusCode != that.statusCode) {
            return false;
        }
        if (!message.equals(that.message)) {
            return false;
        }


        return listDataRekomendasi.size() != that.listDataRekomendasi.size();
    }

    @Override
    public int hashCode() {

        int result = listDataRekomendasi.hashCode();
        result = 31 * result;
        return result;
    }

    public List<DataRekomendasi> getListDataRekomendasi() {
        return listDataRekomendasi;
    }
    public String getMessage(){
        return message;
    }

    public boolean getStatusCode() {
        return statusCode;
    }

    public void setDataRekomendasi(List<DataRekomendasi> data) {
        this.listDataRekomendasi = data;
    }


    public static class DataRekomendasi {

        @Expose
        @SerializedName("id_rekomendasi")
        private Integer id_product;

        @Expose
        @SerializedName("title")
        private String title_rekomendasi;

        @Expose
        @SerializedName("created_date")
        private String created_date;



        public String getTilteRekomendasi() {
            return title_rekomendasi;
        }


        public Integer getIdRekomendasi() {
            return id_product;
        }

        public String getCreatedDate() {
            return created_date;
        }




    }
}
