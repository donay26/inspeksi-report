package com.din.skripsi.inspeksireport.ui.home.rekomendasi;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.din.skripsi.inspeksireport.databinding.ItemRekomendasiEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemRekomendasiViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class RekomendasiAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<RekomendasiItemViewModel> mRekomendasiResponseList;

    private RekomendasiAdapter.RekomendasiAdapterListener mListener;

    public RekomendasiAdapter() {
        this.mRekomendasiResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mRekomendasiResponseList.isEmpty()) {
            return mRekomendasiResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mRekomendasiResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemRekomendasiViewBinding rekomendasiViewBinding = ItemRekomendasiViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new RekomendasiAdapter.RekomendasiViewHolder(rekomendasiViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemRekomendasiEmptyViewBinding emptyViewBinding = ItemRekomendasiEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new RekomendasiAdapter.EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<RekomendasiItemViewModel> repoList) {
        mRekomendasiResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mRekomendasiResponseList.clear();
    }

    public void setListener(RekomendasiAdapter.RekomendasiAdapterListener listener) {
        this.mListener = listener;
    }

    public interface RekomendasiAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements RekomendasiEmptyItemViewModel.RekomendasiEmptyItemViewModelListener {

        private final ItemRekomendasiEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemRekomendasiEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            RekomendasiEmptyItemViewModel emptyItemViewModel = new RekomendasiEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class RekomendasiViewHolder extends BaseViewHolder {

        private final ItemRekomendasiViewBinding mBinding;

        public RekomendasiViewHolder(ItemRekomendasiViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final RekomendasiItemViewModel mRekomendasiItemViewModel = mRekomendasiResponseList.get(position);

            mBinding.setViewModel(mRekomendasiItemViewModel);
            mBinding.createdDateTextView.setText(CommonUtils.convertTimeStamp(mRekomendasiResponseList.get(position)._createdDateRekomendasi.get()));

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}