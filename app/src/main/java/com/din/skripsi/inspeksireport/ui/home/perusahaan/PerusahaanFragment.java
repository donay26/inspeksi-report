package com.din.skripsi.inspeksireport.ui.home.perusahaan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.FragmentPerusahaanBinding;
import com.din.skripsi.inspeksireport.inject.component.FragmentComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseFragment;
import com.din.skripsi.inspeksireport.ui.create_perusahaan.CreatePerusahaanActivity;
import com.din.skripsi.inspeksireport.ui.update_perusahaan.UpdatePerusahaanActivity;

import javax.inject.Inject;

public class PerusahaanFragment extends BaseFragment<FragmentPerusahaanBinding, PerusahaanViewModel> implements PerusahaanNavigator, PerusahaanAdapter.PerusahaanAdapterListener {

    public static final String TAG = "PerusahaanFragment";


    FragmentPerusahaanBinding mFragmentPerusahaanBinding;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    PerusahaanAdapter mPerusahaanAdapter;


    public static PerusahaanFragment newInstance() {
        Bundle args = new Bundle();
        PerusahaanFragment fragment = new PerusahaanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_perusahaan;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel.setNavigator(this);
        mPerusahaanAdapter.setListener(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentPerusahaanBinding = getViewDataBinding();
        mViewModel.fetchperusahaans();
        setUp();
    }


    @Override
    public void performDependencyInjection(FragmentComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void openDialog(int idPerusahaan, String nama_perusahaan, String kode_perusahaan, String alamat_perusahaan) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(nama_perusahaan);
        builder.setMessage("Silahkan pilih ?");

        // add the buttons
        builder.setPositiveButton("Update", (dialog, which) -> {
            Intent intent = UpdatePerusahaanActivity.newIntent(getContext(),idPerusahaan,nama_perusahaan,kode_perusahaan,alamat_perusahaan);
            startActivityForResult(intent,1);
        });
        builder.setNegativeButton("Delete", (dialog, which) -> {
            dialog.dismiss();
            mViewModel.deletePerusahaan(idPerusahaan);
        });
        builder.setNeutralButton("Batal",(dialog, which) -> {
            dialog.dismiss();
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }


    @Override
    public void isRefresh() {
        mViewModel.fetchperusahaans();
    }


    @Override
    public void isSwipeRefresh(boolean swipe) {
        mFragmentPerusahaanBinding.swipeLayout.setRefreshing(swipe);

    }

    @Override
    public void onBack() {

    }

    @Override
    public void openCreatePerusahaanActivity() {
        startActivity(CreatePerusahaanActivity.newIntent(getContext()));

    }

    private void setUp() {
        mFragmentPerusahaanBinding.swipeLayout.setOnRefreshListener(() -> mViewModel.fetchperusahaans());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentPerusahaanBinding.perusahaanRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentPerusahaanBinding.perusahaanRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentPerusahaanBinding.perusahaanRecyclerView.setAdapter(mPerusahaanAdapter);
    }


    @Override
    public void onRetryClick() {
        mViewModel.fetchperusahaans();
    }
}
