package com.din.skripsi.inspeksireport.ui.create_status;

import android.text.TextUtils;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.create_status.create_product.CreateStatusRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.ui.create_status.CreateStatusNavigator;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class CreateStatusViewModel extends BaseViewModel<CreateStatusNavigator> {



    public CreateStatusViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isValid(String nama) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }

        return true;
    }

    public void createStatus(String nama) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postCreateStatusApiCall(new CreateStatusRequest(nama))
                .map(createStatusResponse -> createStatusResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }




    public void onServerCreateStatusClick() {
        getNavigator().createStatus();
    }


}
