package com.din.skripsi.inspeksireport.ui.update_perusahaan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityUpdatePerusahaanBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.ui.update_perusahaan.UpdatePerusahaanNavigator;
import com.din.skripsi.inspeksireport.ui.update_perusahaan.UpdatePerusahaanViewModel;

public class UpdatePerusahaanActivity extends BaseActivity<ActivityUpdatePerusahaanBinding, UpdatePerusahaanViewModel> implements UpdatePerusahaanNavigator {

    private ActivityUpdatePerusahaanBinding mActivityUpdatePerusahaanBinding;

    private static final String PREF_KEY_ID_PERUSAHAAN = "PREF_KEY_CURRENT_ID_PERUSAHAAN";

    private static final String PREF_KEY_NAMA = "PREF_KEY_CURRENT_NAMA";
    private static final String PREF_KEY_KODE = "PREF_KEY_CURRENT_KODE";
    private static final String PREF_KEY_ALAMAT = "PREF_KEY_CURRENT_ALAMAT";



    public static Intent newIntent(Context context,
                                   int idPerusahaan,
                                   String nama,
                                   String kode,
                                   String alamat
                                  ) {
        Intent intent = new Intent(context, UpdatePerusahaanActivity.class);
        intent.putExtra(PREF_KEY_ID_PERUSAHAAN, idPerusahaan);
        intent.putExtra(PREF_KEY_KODE, kode);
        intent.putExtra(PREF_KEY_ALAMAT, alamat);
        intent.putExtra(PREF_KEY_NAMA, nama);
        return intent;
    }


    public Integer getPrefKeyIdPerusahaan() {
        Bundle intent = getIntent().getExtras();
        Integer id_perusahaan = intent.getInt(PREF_KEY_ID_PERUSAHAAN, 0);
        return id_perusahaan;
    }


    public String getPrefKeyKode() {
        Bundle intent = getIntent().getExtras();
        String kode = intent.getString(PREF_KEY_KODE);
        return kode;
    }

    public String getPrefKeyAlamat() {
        Bundle intent = getIntent().getExtras();
        String alamat = intent.getString(PREF_KEY_ALAMAT);
        return alamat;
    }

    public String getPrefKeyNama() {
        Bundle intent = getIntent().getExtras();
        String nama = intent.getString(PREF_KEY_NAMA);
        return nama;
    }



    public static Intent newIntent(Context context) {
        return new Intent(context, UpdatePerusahaanActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_perusahaan;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityUpdatePerusahaanBinding = getViewDataBinding();
        mViewModel.setNavigator(this);

        setSupportActionBar(mActivityUpdatePerusahaanBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUp();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }


    private  void setUp(){
        mActivityUpdatePerusahaanBinding.etNama.setText(getPrefKeyNama());
        mActivityUpdatePerusahaanBinding.etAlamat.setText(getPrefKeyAlamat());
        mActivityUpdatePerusahaanBinding.etKode.setText(getPrefKeyKode());


    }



    @Override
    public void updatePerusahaan() {

        String namaPerusahaan = mActivityUpdatePerusahaanBinding.etNama.getText().toString();
        String kodePerusahaan = mActivityUpdatePerusahaanBinding.etKode.getText().toString();
        String alamatPerusahaan = mActivityUpdatePerusahaanBinding.etAlamat.getText().toString();

        if (mViewModel.isValid(namaPerusahaan,kodePerusahaan,alamatPerusahaan)) {
            hideKeyboard();
            mViewModel.updatePerusahaan(getPrefKeyIdPerusahaan(),namaPerusahaan,kodePerusahaan,alamatPerusahaan);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(UpdatePerusahaanActivity.this);
        startActivity(intent);
        finish();
    }





}