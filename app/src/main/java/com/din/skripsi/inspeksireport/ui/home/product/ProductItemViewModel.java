package com.din.skripsi.inspeksireport.ui.home.product;

import androidx.databinding.ObservableField;



public class ProductItemViewModel {

    public final ObservableField<Integer> _idProduct = new ObservableField<>();
    public final ObservableField<String> _namaProduct = new ObservableField<>();
    public final ObservableField<Integer> _statusProduct = new ObservableField<>();
    public final ObservableField<String> _createdDateProduct = new ObservableField<>();

    public final ProductItemViewModel.ProductItemViewModelListener mListener;

    public ProductItemViewModel(int idProduct, String nama_product, Integer status_product, String created_date, ProductItemViewModel.ProductItemViewModelListener listener) {
        this.mListener = listener;
        this._idProduct.set(idProduct);
        this._namaProduct.set(nama_product);
        this._statusProduct.set(status_product);
        this._createdDateProduct.set(created_date);
    }

    public void onItemClick() {
        mListener.onItemClick(_idProduct.get(),_namaProduct.get());
    }

    public interface ProductItemViewModelListener {

        void onItemClick(int idProduct,String nama);
    }

}
