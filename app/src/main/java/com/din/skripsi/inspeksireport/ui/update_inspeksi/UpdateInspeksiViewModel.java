package com.din.skripsi.inspeksireport.ui.update_inspeksi;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_inspeksi.UpdateInspeksiRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

public class UpdateInspeksiViewModel extends BaseViewModel<UpdateInspeksiNavigator> {

    private final MutableLiveData<List<ProductResponse.DataProduct>> productItemLiveData;

    private final ObservableField<Integer> userRole = new ObservableField<>();

    private final ObservableList<ProductResponse.DataProduct> productDataList = new ObservableArrayList<>();

    public UpdateInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        productItemLiveData = new MutableLiveData<>();
        loadMasters();

    }
    public ObservableField<Integer> getUserRole() {
        return userRole;
    }


    public void onSetUser() {


        final Integer currentUserrole = getDataManager().getCurrentUserRole();
        userRole.set(currentUserrole);


    }



    public boolean isValid(String nomor_surat, Integer id_perusahaan,String serial_number, String note) {


        if (TextUtils.isEmpty(nomor_surat)) {
            return false;
        }
        if (id_perusahaan == null) {
            return false;
        }
        if (TextUtils.isEmpty(serial_number)) {
            return false;
        }
        if (TextUtils.isEmpty(note)) {
            return false;
        }
        return true;
    }

    public void updateInspeksi(String idUnique,String nomor_surat, Integer id_perusahaan,String serial_number, String note,Integer id_product,int id_inspeksi) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postUpdateInspeksiApiCall(new UpdateInspeksiRequest(idUnique,nomor_surat,id_perusahaan,serial_number,note,id_product,id_inspeksi))
                .map(UpdateInspeksiResponse -> UpdateInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(UpdateInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }

    public void loadMasters() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getAllMasterApiCall()
                .map(masterResponse -> masterResponse.getDataMaaster())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(dataMaster -> {
                    setIsLoading(false);
                    if (dataMaster != null) {
//                        Log.d("TAG", "loadQuestionCards: " + dataMaster.size());
//                        productItemLiveData.setValue(questionList);
                        getNavigator().setSpinnerProduct(dataMaster.getDataProduct());
                        getNavigator().setSpinnerPerusahaan(dataMaster.getDataPerusahaan());
                    }
                }, throwable -> {
                    Log.d("TAG", "loadQuestionCards: " + throwable);
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }



    public void onServerUpdateInspeksiClick() {
        getNavigator().updateInspeksi();
    }

    public LiveData<List<ProductResponse.DataProduct>> getProductData() {
        return productItemLiveData;
    }


    public ObservableList<ProductResponse.DataProduct> getProductDataList() {
        return productDataList;
    }

    public void setProductDataList(List<ProductResponse.DataProduct> productDatas) {
        productDataList.clear();
        productDataList.addAll(productDatas);
    }


}
