package com.din.skripsi.inspeksireport.data.model.api.delete_user;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class DeleteUserRequest extends JSONObject {


    @Expose
    @SerializedName("id_user")
    private int id_user;




    public DeleteUserRequest(int id_user) {
        this.id_user = id_user;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DeleteUserRequest that = (DeleteUserRequest) object;



        return id_user == 0 ? id_user == that.id_user : that.id_user == 0;

    }


    @Override
    public int hashCode() {
        int result = id_user != 0 ? id_user : 0;
        return result;
    }

    public Integer getIdUser() {
        return id_user;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_user", getIdUser());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, DeleteUserRequest.class);
    }

}
