/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.din.skripsi.inspeksireport.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;


import com.din.skripsi.inspeksireport.inject.PreferenceInfo;

import javax.inject.Inject;

/**
 * Created by amitshekhar on 07/07/17.
 */

public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_CURRENT_USER_NAMA = "PREF_KEY_CURRENT_USER_NAMA";
    private static final String PREF_KEY_CURRENT_USER_ROLE = "PREF_KEY_CURRENT_USER_ROLE";
    private static final String PREF_KEY_CURRENT_USER_NIK = "PREF_KEY_CURRENT_USER_NIK";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(Context context, @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public Integer getCurrentUserId() {
        return mPrefs.getInt(PREF_KEY_CURRENT_USER_ID, 0);
    }

    @Override
    public String getCurrentUserNama() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_NAMA, null);
    }

    @Override
    public String getCurrentUserNIK() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_NIK, null);

    }

    @Override
    public Integer getCurrentUserRole() {
        return mPrefs.getInt(PREF_KEY_CURRENT_USER_ROLE, 0);
    }

    @Override
    public void setCurrentUserNIK(String userNIK) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NIK, userNIK).apply();

    }

    @Override
    public void setCurrentUserNama(String userNama) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAMA, userNama).apply();

    }

    @Override
    public void setCurrentUserId(Integer userId) {
        mPrefs.edit().putInt(PREF_KEY_CURRENT_USER_ID, userId).apply();
    }

    @Override
    public void setCurrentUserRole(Integer userRole) {
        mPrefs.edit().putInt(PREF_KEY_CURRENT_USER_ROLE, userRole).apply();
    }




}
