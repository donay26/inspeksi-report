package com.din.skripsi.inspeksireport.ui.create_item_inspeksi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;

import java.util.ArrayList;
import java.util.List;

public class SpinnerRekomendasiAdapter extends BaseAdapter {
    Context context;

    List<RekomendasiResponse.DataRekomendasi> dataRekomendasis =  new ArrayList<>();
    LayoutInflater inflter;

    public SpinnerRekomendasiAdapter(Context applicationContext, List<RekomendasiResponse.DataRekomendasi> dataRekomendasis) {
        this.context = applicationContext;
        this.dataRekomendasis = dataRekomendasis;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        if (dataRekomendasis !=  null) {
            return dataRekomendasis.size();
        } else {
            return 0;
        }
    }

    @Override
    public RekomendasiResponse.DataRekomendasi getItem(int i) {
        return dataRekomendasis.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_spinner_rekomendasi, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(dataRekomendasis.get(i).getTilteRekomendasi());
        return view;
    }
}
