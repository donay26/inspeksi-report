package com.din.skripsi.inspeksireport.ui.create_inspeksi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;

import java.util.ArrayList;
import java.util.List;

public class SpinnerPerusahaanAdapter extends BaseAdapter {
    Context context;

    List<PerusahaanResponse.DataPerusahaan> dataPerusahaans =  new ArrayList<>();
    LayoutInflater inflter;

    public SpinnerPerusahaanAdapter(Context applicationContext, List<PerusahaanResponse.DataPerusahaan> dataPerusahaans) {
        this.context = applicationContext;
        this.dataPerusahaans = dataPerusahaans;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        if (dataPerusahaans !=  null) {
            return dataPerusahaans.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_spinner_perusahaan, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(dataPerusahaans.get(i).getNamaPerusahaan());
        return view;
    }
}
