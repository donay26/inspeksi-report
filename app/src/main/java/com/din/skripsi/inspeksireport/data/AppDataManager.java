/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.din.skripsi.inspeksireport.data;

import android.content.Context;

import com.din.skripsi.inspeksireport.data.local.prefs.PreferencesHelper;
import com.din.skripsi.inspeksireport.data.model.api.approval_inspeksi.ApprovalInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.approval_inspeksi.ApprovalInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_inspeksi.CreateInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_inspeksi.CreateInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_item_inspeksi.CreateItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_item_inspeksi.CreateItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_perusahaan.CreatePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_perusahaan.CreatePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_product.CreateProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_product.CreateProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_rekomendasi.CreateRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_rekomendasi.CreateRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_status.create_product.CreateStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_status.create_product.CreateStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_user.CreateUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_user.CreateUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi.DeleteInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi.DeleteInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_item_inspeksi.DeleteItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_item_inspeksi.DeleteItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_perusahaan.DeletePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_perusahaan.DeletePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_product.DeleteProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_product.DeleteProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_rekomendasi.DeleteRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_rekomendasi.DeleteRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_status.DeleteStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_status.DeleteStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_user.DeleteUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_user.DeleteUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.history_inspeksi.HistoryInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.inspeksi.InspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.item_inspeksi.ItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.log_inspeksi.LogInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.login.LoginRequest;
import com.din.skripsi.inspeksireport.data.model.api.login.LoginResponse;
import com.din.skripsi.inspeksireport.data.model.api.master.MasterResponse;
import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_inspeksi.UpdateInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_inspeksi.UpdateInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_item_inspeksi.UpdateItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_item_inspeksi.UpdateItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_perusahaan.UpdatePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_perusahaan.UpdatePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_product.UpdateProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_product.UpdateProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_rekomendasi.UpdateRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_rekomendasi.UpdateRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_status.UpdateStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_status.UpdateStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_user.UpdateUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_user.UpdateUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.user.UserResponse;
import com.din.skripsi.inspeksireport.data.remote.ApiHelper;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;


@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;

    private final ApiHelper mApiHelper;

    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public AppDataManager(Context context, PreferencesHelper mPreferencesHelper, ApiHelper apiHelper) {
        this.mPreferencesHelper = mPreferencesHelper;
        this.mApiHelper = apiHelper;
        this.mContext = context;
    }


    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(0,
                null, null,0);
    }

    @Override
    public void updateUserInfo(Integer id_user,String userNik, String userNama,Integer role) {
        setCurrentUserNama(userNama);
        setCurrentUserId(id_user);
        setCurrentUserNIK(userNik);
        setCurrentUserRole(role);
    }


    @Override
    public Integer getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public String getCurrentUserNama() {
        return mPreferencesHelper.getCurrentUserNama();
    }

    @Override
    public String getCurrentUserNIK() {
        return mPreferencesHelper.getCurrentUserNIK();    }

    @Override
    public Integer getCurrentUserRole() {
        return mPreferencesHelper.getCurrentUserRole();
    }

    @Override
    public void setCurrentUserNIK(String userNik) {
        mPreferencesHelper.setCurrentUserNIK(userNik);
    }

    @Override
    public void setCurrentUserNama(String userNama) {
        mPreferencesHelper.setCurrentUserNama(userNama);
    }

    @Override
    public void setCurrentUserId(Integer userId) {
        mPreferencesHelper.setCurrentUserId(userId);

    }

    @Override
    public void setCurrentUserRole(Integer userRole) {
        mPreferencesHelper.setCurrentUserRole(userRole);
    }


    @Override
    public Single<LoginResponse> doLoginApiCall(LoginRequest request) {
        return mApiHelper.doLoginApiCall(request);
    }

    @Override
    public Single<InspeksiResponse> getAllInspeksiApiCall() {
        return mApiHelper.getAllInspeksiApiCall();
    }

    @Override
    public Single<ItemInspeksiResponse> getItemInspeksiApiCall(int id_inspeksi) {
        return mApiHelper.getItemInspeksiApiCall(id_inspeksi);
    }

    @Override
    public Single<HistoryInspeksiResponse> getAllHistoryApiCall() {
        return mApiHelper.getAllHistoryApiCall();
    }

    @Override
    public Single<ProductResponse> getAllProductApiCall() {
        return mApiHelper.getAllProductApiCall();
    }

    @Override
    public Single<UserResponse> getAllUserApiCall() {
        return mApiHelper.getAllUserApiCall();
    }

    @Override
    public Single<CreateInspeksiResponse> postCreateInspeksiApiCall(CreateInspeksiRequest request, File file) {
        return mApiHelper.postCreateInspeksiApiCall(request,file);
    }

    @Override
    public Single<UpdateInspeksiResponse> postUpdateInspeksiApiCall(UpdateInspeksiRequest request) {
        return mApiHelper.postUpdateInspeksiApiCall(request);
    }

    @Override
    public Single<ApprovalInspeksiResponse> postApprovalInspeksiApiCall(ApprovalInspeksiRequest request) {
        return mApiHelper.postApprovalInspeksiApiCall(request);

    }

    @Override
    public Single<DeleteInspeksiResponse> postDeleteInspeksiApiCall(DeleteInspeksiRequest request) {
        return mApiHelper.postDeleteInspeksiApiCall(request);
    }

    @Override
    public Single<CreateItemInspeksiResponse> postCreateItemInspeksiApiCall(CreateItemInspeksiRequest request) {
        return mApiHelper.postCreateItemInspeksiApiCall(request);
    }

    @Override
    public Single<UpdateItemInspeksiResponse> postUpdateItemInspeksiApiCall(UpdateItemInspeksiRequest request) {
        return mApiHelper.postUpdateItemInspeksiApiCall(request);

    }

    @Override
    public Single<DeleteItemInspeksiResponse> postDeleteItemInspeksiApiCall(DeleteItemInspeksiRequest request) {
        return mApiHelper.postDeleteItemInspeksiApiCall(request);
    }

    @Override
    public Single<CreateProductResponse> postCreateProductApiCall(CreateProductRequest request) {
        return mApiHelper.postCreateProductApiCall(request);
    }

    @Override
    public Single<UpdateProductResponse> postUpdateProductApiCall(UpdateProductRequest request) {
        return mApiHelper.postUpdateProductApiCall(request);
    }

    @Override
    public Single<DeleteProductResponse> postDeleteProductApiCall(DeleteProductRequest request) {
        return mApiHelper.postDeleteProductApiCall(request);

    }

    @Override
    public Single<CreateUserResponse> postCreateUserApiCall(CreateUserRequest request) {
        return mApiHelper.postCreateUserApiCall(request);
    }

    @Override
    public Single<UpdateUserResponse> postUpdateUserApiCall(UpdateUserRequest request) {
        return mApiHelper.postUpdateUserApiCall(request);
    }

    @Override
    public Single<DeleteUserResponse> postDeleteUserApiCall(DeleteUserRequest request) {
        return mApiHelper.postDeleteUserApiCall(request);

    }

    @Override
    public Single<RekomendasiResponse> getAllRekomendasiApiCall() {
        return mApiHelper.getAllRekomendasiApiCall();
    }

    @Override
    public Single<CreateRekomendasiResponse> postCreateRekomendasiApiCall(CreateRekomendasiRequest request) {
        return mApiHelper.postCreateRekomendasiApiCall(request);
    }

    @Override
    public Single<UpdateRekomendasiResponse> postUpdateRekomendasiApiCall(UpdateRekomendasiRequest request) {
        return mApiHelper.postUpdateRekomendasiApiCall(request);
    }

    @Override
    public Single<DeleteRekomendasiResponse> postDeleteRekomendasiApiCall(DeleteRekomendasiRequest request) {
        return mApiHelper.postDeleteRekomendasiApiCall(request);

    }

    @Override
    public Single<StatusResponse> getAllStatusApiCall() {
        return mApiHelper.getAllStatusApiCall();
    }

    @Override
    public Single<CreateStatusResponse> postCreateStatusApiCall(CreateStatusRequest request) {
        return mApiHelper.postCreateStatusApiCall(request);
    }

    @Override
    public Single<UpdateStatusResponse> postUpdateStatusApiCall(UpdateStatusRequest request) {
        return mApiHelper.postUpdateStatusApiCall(request);
    }

    @Override
    public Single<DeleteStatusResponse> postDeleteStatusApiCall(DeleteStatusRequest request) {
        return mApiHelper.postDeleteStatusApiCall(request);
    }

    @Override
    public Single<PerusahaanResponse> getAllPerusahaanApiCall() {
        return mApiHelper.getAllPerusahaanApiCall();
    }

    @Override
    public Single<CreatePerusahaanResponse> postCreatePerusahaanApiCall(CreatePerusahaanRequest request) {
        return mApiHelper.postCreatePerusahaanApiCall(request);
    }

    @Override
    public Single<UpdatePerusahaanResponse> postUpdatePerusahaanApiCall(UpdatePerusahaanRequest request) {
        return mApiHelper.postUpdatePerusahaanApiCall(request);
    }

    @Override
    public Single<DeletePerusahaanResponse> postDeletePerusahaanApiCall(DeletePerusahaanRequest request) {
        return mApiHelper.postDeletePerusahaanApiCall(request);
    }

    @Override
    public Single<MasterResponse> getAllMasterApiCall() {
        return mApiHelper.getAllMasterApiCall();
    }

    @Override
    public Single<LogInspeksiResponse> getAllLogInspeksiApiCall() {
        return mApiHelper.getAllLogInspeksiApiCall();
    }

}

