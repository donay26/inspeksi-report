package com.din.skripsi.inspeksireport.ui.home.rekomendasi;


public class RekomendasiEmptyItemViewModel {

    private final RekomendasiEmptyItemViewModelListener mListener;

    public RekomendasiEmptyItemViewModel(RekomendasiEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface RekomendasiEmptyItemViewModelListener {

        void onRetryClick();
    }
}
