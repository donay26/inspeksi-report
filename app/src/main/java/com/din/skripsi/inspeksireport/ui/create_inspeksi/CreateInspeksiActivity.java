package com.din.skripsi.inspeksireport.ui.create_inspeksi;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;


import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.databinding.ActivityCreateInspeksiBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.utils.CommonUtils;
import com.din.skripsi.inspeksireport.utils.FilePath;
import com.github.drjacky.imagepicker.ImagePicker;


import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Observer;
import java.util.TimeZone;

import javax.inject.Inject;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

public class CreateInspeksiActivity extends BaseActivity<ActivityCreateInspeksiBinding, CreateInspeksiViewModel> implements CreateInspeksiNavigator {

    private ActivityCreateInspeksiBinding mActivityCreateInspeksiBinding;

    private static final String PREF_KEY_COUNT_INSPEKSI = "PREF_KEY_CURRENT_COUNT_INSPEKSI";

    private Spinner spinnerProduct;
    private Spinner spinnerPerusahaan;
    private List<ProductResponse.DataProduct> _dataProducts;
    private List<PerusahaanResponse.DataPerusahaan> _dataPerusahaans;
    private ProductResponse.DataProduct productItem;
    private PerusahaanResponse.DataPerusahaan perusahaanItem;
    private ActivityResultLauncher<Intent> launcher;
    private File fileImage;
    private String kode_perusahaan;


    public static Intent newIntent(Context context, int countInspeksi

                                   ) {
        Intent intent = new Intent(context, CreateInspeksiActivity.class);
        intent.putExtra(PREF_KEY_COUNT_INSPEKSI, countInspeksi);
        return intent;
    }


    public Integer getPrefKeyCountInspeksi() {
        Bundle intent = getIntent().getExtras();
        Integer count = intent.getInt(PREF_KEY_COUNT_INSPEKSI, 0);
        return count;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_inspeksi;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCreateInspeksiBinding = getViewDataBinding();
        mViewModel.setNavigator(this);

        setSupportActionBar(mActivityCreateInspeksiBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        launcher =
                registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Uri uri = result.getData().getData();

                        final InputStream imageStream;
                        //                            imageStream = getContentResolver().openInputStream(uri);
//                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                        String selectedFilePath = getFilePathForN(uri,this);
                       fileImage = new File(selectedFilePath);
                        mActivityCreateInspeksiBinding.imageIcon.setVisibility(View.GONE);
                        mActivityCreateInspeksiBinding.imageUpload.setVisibility(View.VISIBLE);
                        mActivityCreateInspeksiBinding.imageUpload.setImageURI(uri);

//                            encodeImage = getEncoded64ImageStringFromBitmap(selectedImage);


//                            Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));
//                            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 512, 512, false);
//                            encodeImage = ConvertBitmapToString(resizedBitmap);

                        // Use the uri to load the image
                    } else if (result.getResultCode() == ImagePicker.RESULT_ERROR) {
                        // Use ImagePicker.Companion.getError(result.getData()) to show an error
                    }
                });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void setSpinnerProduct(List<ProductResponse.DataProduct> dataProducts) {
        spinnerProduct = mActivityCreateInspeksiBinding.productSpinner;
        _dataProducts  = dataProducts;
        SpinnerProductAdapter customAdapter=new SpinnerProductAdapter(getApplicationContext(),dataProducts);

        spinnerProduct.setAdapter(customAdapter);
        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            productItem = _dataProducts.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void setSpinnerPerusahaan(List<PerusahaanResponse.DataPerusahaan> dataPerusahaans) {
        spinnerPerusahaan = mActivityCreateInspeksiBinding.perusahaanSpinner;
        this._dataPerusahaans  = dataPerusahaans;
        SpinnerPerusahaanAdapter customAdapter=new SpinnerPerusahaanAdapter(getApplicationContext(),dataPerusahaans);
        spinnerPerusahaan.setAdapter(customAdapter);
        spinnerPerusahaan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                perusahaanItem = _dataPerusahaans.get(position);
                kode_perusahaan = _dataPerusahaans.get(position).getKodePerusahaan();
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
//            SimpleDateFormat dateMonthFormat = new SimpleDateFormat("MM");
//            SimpleDateFormat dateYearFormat = new SimpleDateFormat("yyyy");
//            String dateMonth = dateMonthFormat.format(calendar.getTime());
//            String dateYear = dateYearFormat.format(calendar.getTime());

                int  dateMonth = calendar.get(Calendar.MONTH)+1;
                int  dateYear = calendar.get(Calendar.YEAR);
                String nomorSurat= (getPrefKeyCountInspeksi()+1)+"/"+kode_perusahaan+"/IR/STMJ-20/AS/"+CommonUtils.converRomawi(dateMonth)+"/"+dateYear;


                mActivityCreateInspeksiBinding.tvNomorSurat.setText(nomorSurat);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }


    public static String ConvertBitmapToString(Bitmap bitmap){
        String encodedImage = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 70, byteArrayOutputStream);
        try {
            encodedImage= URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return encodedImage;
    }


    @Override
    public void createInspeksi() {
        String nomorSurat = mActivityCreateInspeksiBinding.tvNomorSurat.getText().toString();
        String serialNumber = mActivityCreateInspeksiBinding.etSerialNumber.getText().toString();

        String note = mActivityCreateInspeksiBinding.etNote.getText().toString();

        if (mViewModel.isValid(nomorSurat, perusahaanItem.getIdPerusahaan(),serialNumber,note,fileImage)) {
            hideKeyboard();
            try {
                mViewModel.createInspeksi(nomorSurat, perusahaanItem.getIdPerusahaan(),serialNumber,note,productItem.getIdProduct(),fileImage);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(CreateInspeksiActivity.this);
        startActivity(intent);
        finish();
    }

    private static String getFilePathForN(Uri uri, Context context) {
        Uri returnUri = uri;
        Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(context.getFilesDir(), name);
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            Log.e("File Size", "Size " + file.length());
            inputStream.close();
            outputStream.close();
            Log.e("File Path", "Path " + file.getPath());
            Log.e("File Size", "Size " + file.length());
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }

    @Override
    public void openImagePicker() {
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            ImagePicker.Companion.with(this)
                    .maxResultSize(512, 512, true)
                    .createIntentFromDialog((Function1) (new Function1() {
                        public Object invoke(Object var1) {
                            this.invoke((Intent) var1);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(@NotNull Intent it) {
                            Intrinsics.checkNotNullParameter(it, "it");
                            launcher.launch(it);
                        }
                    }));
//        } else {
            // Request permission from the user
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);

//        }
    }






//    private void subscribeToLiveData() {
//        mViewModel.getProductData().getValue();
//        mViewModel.getProductData().observe(this, questionCardDatas -> mViewModel.setProductDataList(questionCardDatas));
//    }


    private String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }
}