package com.din.skripsi.inspeksireport.inject.component;




import com.din.skripsi.inspeksireport.inject.module.ActivityModule;
import com.din.skripsi.inspeksireport.inject.scope.ActivityScope;
import com.din.skripsi.inspeksireport.ui.create_inspeksi.CreateInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.create_item_inspeksi.CreateItemInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.create_perusahaan.CreatePerusahaanActivity;
import com.din.skripsi.inspeksireport.ui.create_product.CreateProductActivity;
import com.din.skripsi.inspeksireport.ui.create_rekomendasi.CreateRekomendasiActivity;
import com.din.skripsi.inspeksireport.ui.create_status.CreateStatusActivity;
import com.din.skripsi.inspeksireport.ui.create_user.CreateUserActivity;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.DetailInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.ui.log_inspeksi.LogInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.login.LoginActivity;
import com.din.skripsi.inspeksireport.ui.splash.SplashActivity;
import com.din.skripsi.inspeksireport.ui.update_inspeksi.UpdateInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.update_item_inspeksi.UpdateItemInspeksiActivity;
import com.din.skripsi.inspeksireport.ui.update_perusahaan.UpdatePerusahaanActivity;
import com.din.skripsi.inspeksireport.ui.update_product.UpdateProductActivity;
import com.din.skripsi.inspeksireport.ui.update_rekomendasi.UpdateRekomendasiActivity;
import com.din.skripsi.inspeksireport.ui.update_status.UpdateStatusActivity;
import com.din.skripsi.inspeksireport.ui.update_user.UpdateUserActivity;

import dagger.Component;

@ActivityScope
@Component(modules = ActivityModule.class, dependencies = AppComponent.class)
public interface ActivityComponent {

    void inject(SplashActivity splashActivity);

    void inject(LoginActivity loginActivity);

    void inject(MainActivity mainActivity);

    void inject(CreateInspeksiActivity createInspeksiActivity);

    void inject(UpdateInspeksiActivity updateInspeksiActivity);

    void inject(CreateItemInspeksiActivity createInspeksiActivity);

    void inject(CreateProductActivity createProductActivity);

    void inject(CreateUserActivity createUserActivity);

    void inject(DetailInspeksiActivity detailInspeksiActivity);

    void inject(UpdateItemInspeksiActivity updateItemInspeksiActivity);

    void inject(UpdateUserActivity updateUserActivity);

    void inject(UpdateProductActivity updateProductActivity);

    void inject(CreateRekomendasiActivity createRekomendasiActivity);

    void inject(UpdateRekomendasiActivity updateRekomendasiActivity);

    void inject(CreateStatusActivity createStatusActivity);

    void inject(UpdateStatusActivity updateStatusActivity);

    void inject(CreatePerusahaanActivity createPerusahaanActivity);

    void inject(UpdatePerusahaanActivity updatePerusahaanActivity);

    void inject(LogInspeksiActivity logInspeksiActivity);

}