package com.din.skripsi.inspeksireport.data.model.api.create_status.create_product;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class CreateStatusRequest extends JSONObject {

    @Expose
    @SerializedName("nama_status")
    private String nama_status;



    public CreateStatusRequest(String nama) {
        this.nama_status = nama;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        CreateStatusRequest that = (CreateStatusRequest) object;



        return nama_status == "" ? nama_status == that.nama_status : that.nama_status == "";

    }


    @Override
    public int hashCode() {
        int result = nama_status.hashCode() != 0 ? nama_status.hashCode() : 0;
        return result;
    }

    public String getNamaStatus() {
        return nama_status;
    }


    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nama_status", getNamaStatus());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, CreateStatusRequest.class);
    }

}
