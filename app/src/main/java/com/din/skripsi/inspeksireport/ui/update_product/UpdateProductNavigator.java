package com.din.skripsi.inspeksireport.ui.update_product;

public interface UpdateProductNavigator {

    void handleError(Throwable throwable);

    void updateProduct();

    void openMainActivity();

}
