package com.din.skripsi.inspeksireport.ui.home.status;


public class StatusEmptyItemViewModel {

    private final StatusEmptyItemViewModelListener mListener;

    public StatusEmptyItemViewModel(StatusEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface StatusEmptyItemViewModelListener {

        void onRetryClick();
    }
}
