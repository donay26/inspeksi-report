package com.din.skripsi.inspeksireport.data.model.api.delete_perusahaan;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class DeletePerusahaanRequest extends JSONObject {


    @Expose
    @SerializedName("id_perusahaan")
    private int id_perusahaan;




    public DeletePerusahaanRequest(int id_perusahaan) {
        this.id_perusahaan = id_perusahaan;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DeletePerusahaanRequest that = (DeletePerusahaanRequest) object;



        return id_perusahaan == 0 ? id_perusahaan == that.id_perusahaan : that.id_perusahaan == 0;

    }


    @Override
    public int hashCode() {
        int result = id_perusahaan != 0 ? id_perusahaan : 0;
        return result;
    }

    public Integer getIdPerusahaan() {
        return id_perusahaan;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_perusahaan", getIdPerusahaan());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, DeletePerusahaanRequest.class);
    }

}
