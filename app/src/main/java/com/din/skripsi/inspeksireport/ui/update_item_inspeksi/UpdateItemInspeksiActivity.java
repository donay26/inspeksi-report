package com.din.skripsi.inspeksireport.ui.update_item_inspeksi;



import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;
import com.din.skripsi.inspeksireport.databinding.ActivityUpdateItemInspeksiBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.create_item_inspeksi.SpinnerRekomendasiAdapter;
import com.din.skripsi.inspeksireport.ui.create_item_inspeksi.SpinnerStatusAdapter;
import com.din.skripsi.inspeksireport.ui.update_inspeksi.UpdateInspeksiActivity;

import java.util.List;

public class UpdateItemInspeksiActivity extends BaseActivity<ActivityUpdateItemInspeksiBinding, UpdateItemInspeksiViewModel> implements UpdateItemInspeksiNavigator {

    private ActivityUpdateItemInspeksiBinding mActivityUpdateItemInspeksiBinding;

    public static final String PREF_KEY_ID_ITEM_INSPEKSI = "PREF_KEY_CURRENT_ID_ITEM_INSPEKSI";

    public static final String PREF_KEY_ID_INSPEKSI = "PREF_KEY_CURRENT_ID_INSPEKSI";

    private static final String PREF_KEY_NAMA = "PREF_KEY_CURRENT_NAMA";

    private static final String PREF_KEY_STATUS = "PREF_KEY_CURRENT_STATUS";

    private static final String PREF_KEY_REKOMENDASI = "PREF_KEY_CURRENT_REKOMENDASI";

    private static final String PREF_KEY_REMARKS = "PREF_KEY_CURRENT_REMARKS";


    private static final String PREF_KEY_CREATED_DATE = "PREF_KEY_CURRENT_CREATED_DATE";


    private Spinner spinnerStatus;
    private Spinner spinnerRekomendasi;
    private List<StatusResponse.DataStatus> _dataStatuss;
    private List<RekomendasiResponse.DataRekomendasi> _dataRekomendasis;
    private StatusResponse.DataStatus StatusItem;
    private RekomendasiResponse.DataRekomendasi RekomendasiItem;


    public static Intent newIntent(Context context,
                                   int idInspeksi,
                                   int idItemInspeksi,
                                   String nama,
                                   Integer status,
                                   Integer rekomendasi,
                                   String remarks,
                                   String createDate ) {
        Intent intent = new Intent(context, UpdateItemInspeksiActivity.class);
        intent.putExtra(PREF_KEY_ID_INSPEKSI, idInspeksi);
        intent.putExtra(PREF_KEY_ID_ITEM_INSPEKSI, idItemInspeksi);
        intent.putExtra(PREF_KEY_NAMA, nama);
        intent.putExtra(PREF_KEY_STATUS, status);
        intent.putExtra(PREF_KEY_REKOMENDASI, rekomendasi);
        intent.putExtra(PREF_KEY_REMARKS, remarks);
        intent.putExtra(PREF_KEY_CREATED_DATE, createDate);
        return intent;
    }

    public Integer getPrefKeyIdItemInspeksi() {
        Bundle intent = getIntent().getExtras();

        Integer id_item_inspeksi = intent.getInt(PREF_KEY_ID_ITEM_INSPEKSI,0);
        return id_item_inspeksi;
    }

    public Integer getPrefKeyIdInspeksi() {
        Bundle intent = getIntent().getExtras();

        Integer id_inspeksi = intent.getInt(PREF_KEY_ID_INSPEKSI,0);
        return id_inspeksi;
    }

    public String getPrefKeyNama() {
        Bundle intent = getIntent().getExtras();

        String nama = intent.getString(PREF_KEY_NAMA);
        return nama;
    }

    public Integer getPrefKeyStatus() {
        Bundle intent = getIntent().getExtras();

        Integer status = intent.getInt(PREF_KEY_STATUS,0);
        return status;
    }

    public Integer getPrefKeyRekomendasi() {
        Bundle intent = getIntent().getExtras();

        Integer rekomendasi = intent.getInt(PREF_KEY_REKOMENDASI,0);
        return rekomendasi;
    }

    public String getPrefKeyRemarks() {
        Bundle intent = getIntent().getExtras();

        String remarks = intent.getString(PREF_KEY_REMARKS);
        return remarks;
    }


    public String getPrefKeyCreatedDate() {
        Bundle intent = getIntent().getExtras();

        String created_date = intent.getString(PREF_KEY_CREATED_DATE);
        return created_date;
    }



    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_item_inspeksi;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityUpdateItemInspeksiBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setSupportActionBar(mActivityUpdateItemInspeksiBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUp();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private  void setUp(){
        mActivityUpdateItemInspeksiBinding.etNama.setText(getPrefKeyNama());
        mActivityUpdateItemInspeksiBinding.etRemarks.setText(getPrefKeyRemarks());

    }

    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void updateInspeksi() {
        String namaItemInspeksi = mActivityUpdateItemInspeksiBinding.etNama.getText().toString();
        String remarks = mActivityUpdateItemInspeksiBinding.etRemarks.getText().toString();

        if (mViewModel.isValid(namaItemInspeksi,remarks)) {
            hideKeyboard();
            mViewModel.updateItemInspeksi(getPrefKeyIdItemInspeksi(), namaItemInspeksi,StatusItem.getIdStatus(),RekomendasiItem.getIdRekomendasi(),remarks);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openInspeksiActivity() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(PREF_KEY_ID_INSPEKSI, getPrefKeyIdInspeksi());
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void setSpinnerStatus(List<StatusResponse.DataStatus> dataStatuses) {

        spinnerStatus = mActivityUpdateItemInspeksiBinding.statusSpinner;
        _dataStatuss  = dataStatuses;
        SpinnerStatusAdapter customAdapter=new SpinnerStatusAdapter(getApplicationContext(),dataStatuses);

        spinnerStatus.setAdapter(customAdapter);
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StatusItem = _dataStatuss.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int position = 0; position < customAdapter.getCount(); position++) {
            if(customAdapter.getItem(position).getIdStatus() == getPrefKeyStatus()) {
                spinnerStatus.setSelection(position);
                return;
            }
        }

    }

    @Override
    public void setSpinnerRekomendasi(List<RekomendasiResponse.DataRekomendasi> dataRekomendasis) {
        spinnerRekomendasi = mActivityUpdateItemInspeksiBinding.rekomendasiSpinner;
        _dataRekomendasis  = dataRekomendasis;
        SpinnerRekomendasiAdapter customAdapter=new SpinnerRekomendasiAdapter(getApplicationContext(),dataRekomendasis);

        spinnerRekomendasi.setAdapter(customAdapter);
        spinnerRekomendasi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                RekomendasiItem = _dataRekomendasis.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        for (int position = 0; position < customAdapter.getCount(); position++) {
            if(customAdapter.getItem(position).getIdRekomendasi() == getPrefKeyRekomendasi()) {
                spinnerRekomendasi.setSelection(position);
                return;
            }
        }


    }


}