package com.din.skripsi.inspeksireport.ui.home.product;

public interface ProductNavigator {
    void handleError(Throwable throwable);
    void openDialog(int idProduct, String nama, int i);
    void isRefresh();
    void isSwipeRefresh(boolean swipe);
    void onBack();
    void openCreateProductActivity();

}

