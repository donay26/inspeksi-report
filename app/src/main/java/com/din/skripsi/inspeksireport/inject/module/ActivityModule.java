package com.din.skripsi.inspeksireport.inject.module;

import androidx.core.util.Supplier;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.din.skripsi.inspeksireport.ViewModelProviderFactory;
import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.create_inspeksi.CreateInspeksiViewModel;
import com.din.skripsi.inspeksireport.ui.create_item_inspeksi.CreateItemInspeksiViewModel;
import com.din.skripsi.inspeksireport.ui.create_perusahaan.CreatePerusahaanViewModel;
import com.din.skripsi.inspeksireport.ui.create_product.CreateProductViewModel;
import com.din.skripsi.inspeksireport.ui.create_rekomendasi.CreateRekomendasiViewModel;
import com.din.skripsi.inspeksireport.ui.create_status.CreateStatusViewModel;
import com.din.skripsi.inspeksireport.ui.create_user.CreateUserViewModel;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.DetailInspeksiViewModel;
import com.din.skripsi.inspeksireport.ui.detail_inspeksi.ItemInspeksiAdapter;
import com.din.skripsi.inspeksireport.ui.home.MainViewModel;
import com.din.skripsi.inspeksireport.ui.log_inspeksi.LogInspeksiAdapter;
import com.din.skripsi.inspeksireport.ui.log_inspeksi.LogInspeksiViewModel;
import com.din.skripsi.inspeksireport.ui.login.LoginViewModel;
import com.din.skripsi.inspeksireport.ui.splash.SplashViewModel;
import com.din.skripsi.inspeksireport.ui.update_inspeksi.UpdateInspeksiViewModel;
import com.din.skripsi.inspeksireport.ui.update_item_inspeksi.UpdateItemInspeksiViewModel;
import com.din.skripsi.inspeksireport.ui.update_perusahaan.UpdatePerusahaanViewModel;
import com.din.skripsi.inspeksireport.ui.update_product.UpdateProductViewModel;
import com.din.skripsi.inspeksireport.ui.update_rekomendasi.UpdateRekomendasiViewModel;
import com.din.skripsi.inspeksireport.ui.update_status.UpdateStatusViewModel;
import com.din.skripsi.inspeksireport.ui.update_user.UpdateUserViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private BaseActivity<?, ?> activity;

    public ActivityModule(BaseActivity<?, ?> activity) {
        this.activity = activity;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager() {
        return new LinearLayoutManager(activity);
    }


    @Provides
    SplashViewModel provideSplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<SplashViewModel> supplier = () -> new SplashViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<SplashViewModel> factory = new ViewModelProviderFactory<>(SplashViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(SplashViewModel.class);
    }


    @Provides
    MainViewModel provideMainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<MainViewModel> supplier = () -> new MainViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<MainViewModel> factory = new ViewModelProviderFactory<>(MainViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(MainViewModel.class);
    }

    @Provides
    LoginViewModel provideLoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<LoginViewModel> supplier = () -> new LoginViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<LoginViewModel> factory = new ViewModelProviderFactory<>(LoginViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(LoginViewModel.class);
    }

    @Provides
    CreateInspeksiViewModel provideCreateInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<CreateInspeksiViewModel> supplier = () -> new CreateInspeksiViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<CreateInspeksiViewModel> factory = new ViewModelProviderFactory<>(CreateInspeksiViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(CreateInspeksiViewModel.class);
    }

    @Provides
    UpdateInspeksiViewModel provideUpdateInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<UpdateInspeksiViewModel> supplier = () -> new UpdateInspeksiViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<UpdateInspeksiViewModel> factory = new ViewModelProviderFactory<>(UpdateInspeksiViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(UpdateInspeksiViewModel.class);
    }

    @Provides
    CreateItemInspeksiViewModel provideCreateItemInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<CreateItemInspeksiViewModel> supplier = () -> new CreateItemInspeksiViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<CreateItemInspeksiViewModel> factory = new ViewModelProviderFactory<>(CreateItemInspeksiViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(CreateItemInspeksiViewModel.class);
    }


    @Provides
    CreateProductViewModel provideCreateIProductViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<CreateProductViewModel> supplier = () -> new CreateProductViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<CreateProductViewModel> factory = new ViewModelProviderFactory<>(CreateProductViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(CreateProductViewModel.class);
    }
    @Provides
    CreateUserViewModel provideCreteUserViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<CreateUserViewModel> supplier = () -> new CreateUserViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<CreateUserViewModel> factory = new ViewModelProviderFactory<>(CreateUserViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(CreateUserViewModel.class);
    }

    @Provides
    DetailInspeksiViewModel providDetailInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<DetailInspeksiViewModel> supplier = () -> new DetailInspeksiViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<DetailInspeksiViewModel> factory = new ViewModelProviderFactory<>(DetailInspeksiViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(DetailInspeksiViewModel.class);
    }

    @Provides
    UpdateItemInspeksiViewModel provideUpdateItemInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<UpdateItemInspeksiViewModel> supplier = () -> new UpdateItemInspeksiViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<UpdateItemInspeksiViewModel> factory = new ViewModelProviderFactory<>(UpdateItemInspeksiViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(UpdateItemInspeksiViewModel.class);
    }

    @Provides
    UpdateUserViewModel provideUpdateUserViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<UpdateUserViewModel> supplier = () -> new UpdateUserViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<UpdateUserViewModel> factory = new ViewModelProviderFactory<>(UpdateUserViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(UpdateUserViewModel.class);
    }

    @Provides
    UpdateProductViewModel provideUpdateProductViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<UpdateProductViewModel> supplier = () -> new UpdateProductViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<UpdateProductViewModel> factory = new ViewModelProviderFactory<>(UpdateProductViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(UpdateProductViewModel.class);
    }


    @Provides
    CreateRekomendasiViewModel provideCreateRekomendasiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<CreateRekomendasiViewModel> supplier = () -> new CreateRekomendasiViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<CreateRekomendasiViewModel> factory = new ViewModelProviderFactory<>(CreateRekomendasiViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(CreateRekomendasiViewModel.class);
    }

    @Provides
    UpdateRekomendasiViewModel provideUpdateRekomendasiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<UpdateRekomendasiViewModel> supplier = () -> new UpdateRekomendasiViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<UpdateRekomendasiViewModel> factory = new ViewModelProviderFactory<>(UpdateRekomendasiViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(UpdateRekomendasiViewModel.class);
    }

    @Provides
    CreateStatusViewModel provideCreateStatusViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<CreateStatusViewModel> supplier = () -> new CreateStatusViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<CreateStatusViewModel> factory = new ViewModelProviderFactory<>(CreateStatusViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(CreateStatusViewModel.class);
    }

    @Provides
    UpdateStatusViewModel provideUpdateStatusViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<UpdateStatusViewModel> supplier = () -> new UpdateStatusViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<UpdateStatusViewModel> factory = new ViewModelProviderFactory<>(UpdateStatusViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(UpdateStatusViewModel.class);
    }


    @Provides
    CreatePerusahaanViewModel provideCreatePerusahaanViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<CreatePerusahaanViewModel> supplier = () -> new CreatePerusahaanViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<CreatePerusahaanViewModel> factory = new ViewModelProviderFactory<>(CreatePerusahaanViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(CreatePerusahaanViewModel.class);
    }

    @Provides
    UpdatePerusahaanViewModel provideUpdatePerusahaanViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<UpdatePerusahaanViewModel> supplier = () -> new UpdatePerusahaanViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<UpdatePerusahaanViewModel> factory = new ViewModelProviderFactory<>(UpdatePerusahaanViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(UpdatePerusahaanViewModel.class);
    }

    @Provides
    LogInspeksiViewModel provideLogInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<LogInspeksiViewModel> supplier = () -> new LogInspeksiViewModel(dataManager,schedulerProvider);
        ViewModelProviderFactory<LogInspeksiViewModel> factory = new ViewModelProviderFactory<>(LogInspeksiViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(LogInspeksiViewModel.class);
    }


    @Provides
    ItemInspeksiAdapter provideItemInspeksiAdapter() {
        return new ItemInspeksiAdapter();
    }

    @Provides
    LogInspeksiAdapter provideLogInspeksiAdapter() {
        return new LogInspeksiAdapter();
    }

}
