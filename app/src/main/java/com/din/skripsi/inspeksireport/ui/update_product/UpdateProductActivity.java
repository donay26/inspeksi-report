package com.din.skripsi.inspeksireport.ui.update_product;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityUpdateProductBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

public class UpdateProductActivity extends BaseActivity<ActivityUpdateProductBinding, UpdateProductViewModel> implements UpdateProductNavigator {

    private ActivityUpdateProductBinding mActivityUpdateProductBinding;

    private static final String PREF_KEY_ID_PRODUCT = "PREF_KEY_CURRENT_ID_PRODUCT";

    private static final String PREF_KEY_NAMA = "PREF_KEY_CURRENT_NAMA";



    public static Intent newIntent(Context context,
                                   int idProduct,
                                   String nama
                                  ) {
        Intent intent = new Intent(context, UpdateProductActivity.class);
        intent.putExtra(PREF_KEY_ID_PRODUCT, idProduct);
        intent.putExtra(PREF_KEY_NAMA, nama);
        return intent;
    }


    public Integer getPrefKeyIdProduct() {
        Bundle intent = getIntent().getExtras();
        Integer id_product = intent.getInt(PREF_KEY_ID_PRODUCT, 0);
        return id_product;
    }


    public String getPrefKeyNama() {
        Bundle intent = getIntent().getExtras();
        String nama = intent.getString(PREF_KEY_NAMA);
        return nama;
    }



    public static Intent newIntent(Context context) {
        return new Intent(context, UpdateProductActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_product;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityUpdateProductBinding = getViewDataBinding();
        mViewModel.setNavigator(this);

        setSupportActionBar(mActivityUpdateProductBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUp();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }


    private  void setUp(){
        mActivityUpdateProductBinding.etNama.setText(getPrefKeyNama());


    }



    @Override
    public void updateProduct() {

        String namaProduct = mActivityUpdateProductBinding.etNama.getText().toString();

        if (mViewModel.isValid(namaProduct)) {
            hideKeyboard();
            mViewModel.updateProduct(getPrefKeyIdProduct(),namaProduct);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(UpdateProductActivity.this);
        startActivity(intent);
        finish();
    }





}