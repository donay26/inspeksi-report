package com.din.skripsi.inspeksireport.data.model.api.update_user;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class UpdateUserRequest extends JSONObject {

    @Expose
    @SerializedName("id_user")
    private Integer id_user;

    @Expose
    @SerializedName("nama")
    private String nama;

    @Expose
    @SerializedName("nik")
    private String nik;

    @Expose
    @SerializedName("password")
    private String password;

    @Expose
    @SerializedName("role")
    private Integer role;

    @Expose
    @SerializedName("status")
    private Integer status;



    public UpdateUserRequest(Integer id_user,String nama, String nik, String password, Integer role, Integer status) {
        this.id_user = id_user;
        this.nama = nama;
        this.password = password;
        this.nik = nik;
        this.role = role;
        this.status = status;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        UpdateUserRequest that = (UpdateUserRequest) object;



        return nik == "" ? nik == that.nik : that.nik == "";

    }


    @Override
    public int hashCode() {
        int result = nik.hashCode() != 0 ? nik.hashCode() : 0;
        return result;
    }

    public Integer getIdUser() {
        return id_user;
    }
    public String getNama() {
        return nama;
    }
    public String getNik() {
        return nik;
    }
    public String getPassword() {
        return password;
    }
    public Integer getRole() {
        return role;
    }
    public Integer getStatus() {
        return status;
    }



    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_user", getIdUser());
            jsonObject.put("nama", getNama());
            jsonObject.put("nik", getNik());
            jsonObject.put("password", getPassword());
            jsonObject.put("role", getRole());
            jsonObject.put("status", getStatus());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, UpdateUserRequest.class);
    }

}
