package com.din.skripsi.inspeksireport.ui.create_item_inspeksi;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.create_item_inspeksi.CreateItemInspeksiRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.List;

public class CreateItemInspeksiViewModel extends BaseViewModel<CreateItemInspeksiNavigator> {



    public CreateItemInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        loadMasters();
    }

    public boolean isValid(String nama, String remarks) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }

        if (TextUtils.isEmpty(remarks)) {
            return false;
        }
        return true;
    }

    public void createItemInspeksi(int id_inspeksi,String nama, int id_status,int id_rekomendasi,String remarks) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postCreateItemInspeksiApiCall(new CreateItemInspeksiRequest(id_inspeksi,nama,id_rekomendasi,remarks,id_status))
                .map(createInspeksiResponse -> createInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openInspeksiActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }

    public void loadMasters() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getAllMasterApiCall()
                .map(masterResponse -> masterResponse.getDataMaaster())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(dataMaster -> {
                    setIsLoading(false);
                    if (dataMaster != null) {
//                        Log.d("TAG", "loadQuestionCards: " + dataMaster.getDataRekomendasi().size());
//                        productItemLiveData.setValue(questionList);
                        getNavigator().setSpinnerRekomendasi(dataMaster.getDataRekomendasi());
                        getNavigator().setSpinnerStatus(dataMaster.getDataStatus());
                    }
                }, throwable -> {
                    Log.d("TAG", "loadQuestionCards: " + throwable);
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }




    public void onServerCreateItemInspeksiClick() {
        getNavigator().createInspeksi();
    }


}
