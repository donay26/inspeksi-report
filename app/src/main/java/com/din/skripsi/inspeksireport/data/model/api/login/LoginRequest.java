package com.din.skripsi.inspeksireport.data.model.api.login;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class LoginRequest extends JSONObject {

    @Expose
    @SerializedName("nik")
    private String nik;

    @Expose
    @SerializedName("password")
    private String password;


    public LoginRequest(String nik,String password) {
        this.nik = nik;
        this.password = password;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        LoginRequest that = (LoginRequest) object;

        if (nik != null ? !nik.equals(that.nik) : that.nik != null) {
            return false;
        }


        return password == "" ? password == that.password : that.password == "";

    }


    @Override
    public int hashCode() {
        int result = nik.hashCode() != 0 ? nik.hashCode() : 0;
        return result;
    }

    public String getNIK() {
        return nik;
    }
    public String getPassword() {
        return password;
    }



    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nik", getNIK());
            jsonObject.put("password", getPassword());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, LoginRequest.class);
    }

}
