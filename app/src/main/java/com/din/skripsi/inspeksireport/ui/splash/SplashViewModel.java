package com.din.skripsi.inspeksireport.ui.splash;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import java.util.concurrent.TimeUnit;

public class SplashViewModel extends BaseViewModel<SplashNavigator> {
    public SplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void startSplash(){
        getSchedulerProvider().ui().scheduleDirect(new Runnable() {
            @Override
            public void run() {
                decideNextActivity();
            }
        },1000, TimeUnit.MILLISECONDS);
    }



    private void decideNextActivity() {
        if (getDataManager().getCurrentUserNIK() == null) {
            getNavigator().openLoginActivity();
        } else {
            getNavigator().openMainActivity();
        }
    }


}
