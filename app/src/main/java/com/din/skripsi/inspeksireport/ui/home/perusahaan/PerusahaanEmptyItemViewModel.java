package com.din.skripsi.inspeksireport.ui.home.perusahaan;


public class PerusahaanEmptyItemViewModel {

    private final PerusahaanEmptyItemViewModelListener mListener;

    public PerusahaanEmptyItemViewModel(PerusahaanEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface PerusahaanEmptyItemViewModelListener {

        void onRetryClick();
    }
}
