package com.din.skripsi.inspeksireport.ui.update_user;

import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;

import java.util.List;

public interface UpdateUserNavigator {

    void handleError(Throwable throwable);

    void updateUser();

    void openMainActivity();

}
