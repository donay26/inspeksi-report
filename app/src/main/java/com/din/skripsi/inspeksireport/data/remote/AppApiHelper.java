/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.din.skripsi.inspeksireport.data.remote;

import android.util.Log;

import com.androidnetworking.common.Priority;
import com.din.skripsi.inspeksireport.data.model.api.approval_inspeksi.ApprovalInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.approval_inspeksi.ApprovalInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_inspeksi.CreateInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_inspeksi.CreateInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_item_inspeksi.CreateItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_item_inspeksi.CreateItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_perusahaan.CreatePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_perusahaan.CreatePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_product.CreateProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_product.CreateProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_rekomendasi.CreateRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_rekomendasi.CreateRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_status.create_product.CreateStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_status.create_product.CreateStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_user.CreateUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_user.CreateUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi.DeleteInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi.DeleteInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_item_inspeksi.DeleteItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_item_inspeksi.DeleteItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_perusahaan.DeletePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_perusahaan.DeletePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_product.DeleteProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_product.DeleteProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_rekomendasi.DeleteRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_rekomendasi.DeleteRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_status.DeleteStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_status.DeleteStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_user.DeleteUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_user.DeleteUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.history_inspeksi.HistoryInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.inspeksi.InspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.item_inspeksi.ItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.log_inspeksi.LogInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.login.LoginRequest;
import com.din.skripsi.inspeksireport.data.model.api.login.LoginResponse;
import com.din.skripsi.inspeksireport.data.model.api.master.MasterResponse;
import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_inspeksi.UpdateInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_inspeksi.UpdateInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_item_inspeksi.UpdateItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_item_inspeksi.UpdateItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_perusahaan.UpdatePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_perusahaan.UpdatePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_product.UpdateProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_product.UpdateProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_rekomendasi.UpdateRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_rekomendasi.UpdateRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_status.UpdateStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_status.UpdateStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_user.UpdateUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_user.UpdateUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.user.UserResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppApiHelper implements ApiHelper {


    @Inject
    public AppApiHelper(

    ) {

    }

    @Override
    public Single<LoginResponse> doLoginApiCall(LoginRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_LOGIN)
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<InspeksiResponse> getAllInspeksiApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_ALL_INSPEKSI)
                .build()
                .getObjectSingle(InspeksiResponse.class);
    }

    @Override
    public Single<ItemInspeksiResponse> getItemInspeksiApiCall(int id_inspeksi) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_ITEM_INSPEKSI+id_inspeksi)
                .build()
                .getObjectSingle(ItemInspeksiResponse.class);
    }

    @Override
    public Single<HistoryInspeksiResponse> getAllHistoryApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_ALL_HISTORY)
                .build()
                .getObjectSingle(HistoryInspeksiResponse.class);
    }

    @Override
    public Single<ProductResponse> getAllProductApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_ALL_PRODUCT)
                .build()
                .getObjectSingle(ProductResponse.class);
    }

    @Override
    public Single<UserResponse> getAllUserApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_ALL_USER)
                .build()
                .getObjectSingle(UserResponse.class);
    }

    @Override
    public Single<CreateInspeksiResponse> postCreateInspeksiApiCall(CreateInspeksiRequest request, File file) {
        return Rx2AndroidNetworking.upload(ApiEndPoint.ENDPOINT_POST_CREATE_INSPEKSI)
                .addMultipartFile("image",file)
                .addMultipartParameter("id_user", String.valueOf(request.getIdUser()))
                .addMultipartParameter("unique_id_inspeksi",request.getUniqueIdInspeksi())
                .addMultipartParameter("nomor_surat",request.getNomorSurat())
                .addMultipartParameter("id_perusahaan", String.valueOf(request.getIdPerusahaan()))
                .addMultipartParameter("image_name",file.getName())
                .addMultipartParameter("serial_number",request.getSerialNumber())
                .addMultipartParameter("note",request.getNote())
                .addMultipartParameter("status_report", String.valueOf(0))
                .addMultipartParameter("id_product", String.valueOf(request.getIdProduct()))
                .build()
                .getObjectSingle(CreateInspeksiResponse.class);
    }

    @Override
    public Single<UpdateInspeksiResponse> postUpdateInspeksiApiCall(UpdateInspeksiRequest request) {
        return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_POST_UPDATE_INSPEKSI+request.getIdInspeksi())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(UpdateInspeksiResponse.class);
    }

    @Override
    public Single<ApprovalInspeksiResponse> postApprovalInspeksiApiCall(ApprovalInspeksiRequest request) {
        return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_POST_APPROVAL_INSPEKSI+request.getIdInspeksi())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(ApprovalInspeksiResponse.class);
    }

    @Override
    public Single<DeleteInspeksiResponse> postDeleteInspeksiApiCall(DeleteInspeksiRequest request) {
        return Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_POST_DELETE_INSPEKSI+request.getIdInspeksi())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(DeleteInspeksiResponse.class);
    }

    @Override
    public Single<CreateItemInspeksiResponse> postCreateItemInspeksiApiCall(CreateItemInspeksiRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_POST_CREATE_ITEM_INSPEKSI)
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(CreateItemInspeksiResponse.class);
    }

    @Override
    public Single<UpdateItemInspeksiResponse> postUpdateItemInspeksiApiCall(UpdateItemInspeksiRequest request) {
        return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_POST_UPDATE_ITEM_INSPEKSI+request.getIdItemInspeksi())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(UpdateItemInspeksiResponse.class);
    }

    @Override
    public Single<DeleteItemInspeksiResponse> postDeleteItemInspeksiApiCall(DeleteItemInspeksiRequest request) {
        return Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_POST_DELETE_ITEM_INSPEKSI+request.getIdItemInspeksi())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(DeleteItemInspeksiResponse.class);
    }

    @Override
    public Single<CreateProductResponse> postCreateProductApiCall(CreateProductRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_POST_CREATE_PRODUCT)
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(CreateProductResponse.class);
    }

    @Override
    public Single<UpdateProductResponse> postUpdateProductApiCall(UpdateProductRequest request) {
        return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_POST_UPDATE_PRODUCT+request.getIdProduct())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(UpdateProductResponse.class);
    }

    @Override
    public Single<DeleteProductResponse> postDeleteProductApiCall(DeleteProductRequest request) {
        return Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_POST_DELETE_PRODUCT+request.getIdProduct())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(DeleteProductResponse.class);
    }

    @Override
    public Single<CreateUserResponse> postCreateUserApiCall(CreateUserRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_POST_CREATE_USER)
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(CreateUserResponse.class);
    }

    @Override
    public Single<UpdateUserResponse> postUpdateUserApiCall(UpdateUserRequest request) {
        return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_POST_UPDATE_USER+request.getIdUser())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(UpdateUserResponse.class);

    }

    @Override
    public Single<DeleteUserResponse> postDeleteUserApiCall(DeleteUserRequest request) {
        return Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_POST_DELETE_USER+request.getIdUser())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(DeleteUserResponse.class);

    }

    @Override
    public Single<RekomendasiResponse> getAllRekomendasiApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_ALL_REKOMENDASI)
                .build()
                .getObjectSingle(RekomendasiResponse.class);

    }

    @Override
    public Single<CreateRekomendasiResponse> postCreateRekomendasiApiCall(CreateRekomendasiRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_POST_CREATE_REKOMENDASI)
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(CreateRekomendasiResponse.class);
    }

    @Override
    public Single<UpdateRekomendasiResponse> postUpdateRekomendasiApiCall(UpdateRekomendasiRequest request) {
        return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_POST_UPDATE_REKOMENDASI+request.getIdRekomendasi())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(UpdateRekomendasiResponse.class);
    }

    @Override
    public Single<DeleteRekomendasiResponse> postDeleteRekomendasiApiCall(DeleteRekomendasiRequest request) {
        return Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_POST_DELETE_REKOMENDASI+request.getIdRekomendasi())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(DeleteRekomendasiResponse.class);
    }

    @Override
    public Single<StatusResponse> getAllStatusApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_ALL_STATUS)
                .build()
                .getObjectSingle(StatusResponse.class);
    }

    @Override
    public Single<CreateStatusResponse> postCreateStatusApiCall(CreateStatusRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_POST_CREATE_STATUS)
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(CreateStatusResponse.class);
    }

    @Override
    public Single<UpdateStatusResponse> postUpdateStatusApiCall(UpdateStatusRequest request) {
        return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_POST_UPDATE_STATUS+request.getIdStatus())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(UpdateStatusResponse.class);
    }

    @Override
    public Single<DeleteStatusResponse> postDeleteStatusApiCall(DeleteStatusRequest request) {
        return Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_POST_DELETE_STATUS+request.getIdStatus())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(DeleteStatusResponse.class);
    }


    @Override
    public Single<PerusahaanResponse> getAllPerusahaanApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_ALL_PERUSAHAAN)
                .build()
                .getObjectSingle(PerusahaanResponse.class);
    }

    @Override
    public Single<CreatePerusahaanResponse> postCreatePerusahaanApiCall(CreatePerusahaanRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_POST_CREATE_PERUSAHAAN)
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(CreatePerusahaanResponse.class);
    }

    @Override
    public Single<UpdatePerusahaanResponse> postUpdatePerusahaanApiCall(UpdatePerusahaanRequest request) {
        return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_POST_UPDATE_PERUSAHAAN+request.getIdPerusahaan())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(UpdatePerusahaanResponse.class);
    }

    @Override
    public Single<DeletePerusahaanResponse> postDeletePerusahaanApiCall(DeletePerusahaanRequest request) {
        return Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_POST_DELETE_PERUSAHAAN+request.getIdPerusahaan())
                .addJSONObjectBody(request.toJSON())
                .build()
                .getObjectSingle(DeletePerusahaanResponse.class);
    }

    @Override
    public Single<MasterResponse> getAllMasterApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_MASTER)
                .build()
                .getObjectSingle(MasterResponse.class);

    }

    @Override
    public Single<LogInspeksiResponse> getAllLogInspeksiApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_LOG_INSPEKSI)
                .build()
                .getObjectSingle(LogInspeksiResponse.class);
    }


}
