package com.din.skripsi.inspeksireport.ui.create_item_inspeksi;


import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;

import java.util.List;

public interface CreateItemInspeksiNavigator {

    void handleError(Throwable throwable);

    void createInspeksi();

    void openInspeksiActivity();

    void setSpinnerStatus(List<StatusResponse.DataStatus> dataStatuses);

    void setSpinnerRekomendasi(List<RekomendasiResponse.DataRekomendasi> dataRekomendasis);

}
