package com.din.skripsi.inspeksireport.ui.splash;

public interface SplashNavigator {
    void openLoginActivity();

    void openMainActivity();
}
