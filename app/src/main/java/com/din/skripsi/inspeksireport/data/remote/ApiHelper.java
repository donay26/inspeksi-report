/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.din.skripsi.inspeksireport.data.remote;



import com.din.skripsi.inspeksireport.data.model.api.approval_inspeksi.ApprovalInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.approval_inspeksi.ApprovalInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_inspeksi.CreateInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_inspeksi.CreateInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_item_inspeksi.CreateItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_item_inspeksi.CreateItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_perusahaan.CreatePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_perusahaan.CreatePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_product.CreateProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_product.CreateProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_rekomendasi.CreateRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_rekomendasi.CreateRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_status.create_product.CreateStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_status.create_product.CreateStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.create_user.CreateUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.create_user.CreateUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi.DeleteInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi.DeleteInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_item_inspeksi.DeleteItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_item_inspeksi.DeleteItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_perusahaan.DeletePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_perusahaan.DeletePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_product.DeleteProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_product.DeleteProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_rekomendasi.DeleteRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_rekomendasi.DeleteRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_status.DeleteStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_status.DeleteStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.delete_user.DeleteUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_user.DeleteUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.history_inspeksi.HistoryInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.item_inspeksi.ItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.log_inspeksi.LogInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.login.LoginRequest;
import com.din.skripsi.inspeksireport.data.model.api.login.LoginResponse;
import com.din.skripsi.inspeksireport.data.model.api.inspeksi.InspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.master.MasterResponse;
import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_inspeksi.UpdateInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_inspeksi.UpdateInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_item_inspeksi.UpdateItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_item_inspeksi.UpdateItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_perusahaan.UpdatePerusahaanRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_perusahaan.UpdatePerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_product.UpdateProductRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_product.UpdateProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_rekomendasi.UpdateRekomendasiRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_rekomendasi.UpdateRekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_status.UpdateStatusRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_status.UpdateStatusResponse;
import com.din.skripsi.inspeksireport.data.model.api.update_user.UpdateUserRequest;
import com.din.skripsi.inspeksireport.data.model.api.update_user.UpdateUserResponse;
import com.din.skripsi.inspeksireport.data.model.api.user.UserResponse;

import java.io.File;

import io.reactivex.Single;



public interface ApiHelper {

    Single<LoginResponse> doLoginApiCall(LoginRequest request);

    Single<InspeksiResponse> getAllInspeksiApiCall();

    Single<ItemInspeksiResponse> getItemInspeksiApiCall(int id_inspeksi);

    Single<HistoryInspeksiResponse> getAllHistoryApiCall();

    //
    Single<CreateInspeksiResponse> postCreateInspeksiApiCall(CreateInspeksiRequest request, File file);

    Single<UpdateInspeksiResponse> postUpdateInspeksiApiCall(UpdateInspeksiRequest request);

    Single<ApprovalInspeksiResponse> postApprovalInspeksiApiCall(ApprovalInspeksiRequest request);

    Single<DeleteInspeksiResponse> postDeleteInspeksiApiCall(DeleteInspeksiRequest request);

    //
    Single<CreateItemInspeksiResponse> postCreateItemInspeksiApiCall(CreateItemInspeksiRequest request);

    Single<UpdateItemInspeksiResponse> postUpdateItemInspeksiApiCall(UpdateItemInspeksiRequest request);

    Single<DeleteItemInspeksiResponse> postDeleteItemInspeksiApiCall(DeleteItemInspeksiRequest request);

    //
    Single<ProductResponse> getAllProductApiCall();

    Single<CreateProductResponse> postCreateProductApiCall(CreateProductRequest request);

    Single<UpdateProductResponse> postUpdateProductApiCall(UpdateProductRequest request);

    Single<DeleteProductResponse> postDeleteProductApiCall(DeleteProductRequest request);

    //

    Single<UserResponse> getAllUserApiCall();

    Single<CreateUserResponse> postCreateUserApiCall(CreateUserRequest request);

    Single<UpdateUserResponse> postUpdateUserApiCall(UpdateUserRequest request);

    Single<DeleteUserResponse> postDeleteUserApiCall(DeleteUserRequest request);


    //
    Single<RekomendasiResponse> getAllRekomendasiApiCall();

    Single<CreateRekomendasiResponse> postCreateRekomendasiApiCall(CreateRekomendasiRequest request);

    Single<UpdateRekomendasiResponse> postUpdateRekomendasiApiCall(UpdateRekomendasiRequest request);

    Single<DeleteRekomendasiResponse> postDeleteRekomendasiApiCall(DeleteRekomendasiRequest request);


    //
    Single<StatusResponse> getAllStatusApiCall();

    Single<CreateStatusResponse> postCreateStatusApiCall(CreateStatusRequest request);

    Single<UpdateStatusResponse> postUpdateStatusApiCall(UpdateStatusRequest request);

    Single<DeleteStatusResponse> postDeleteStatusApiCall(DeleteStatusRequest request);

    //
    Single<PerusahaanResponse> getAllPerusahaanApiCall();

    Single<CreatePerusahaanResponse> postCreatePerusahaanApiCall(CreatePerusahaanRequest request);

    Single<UpdatePerusahaanResponse> postUpdatePerusahaanApiCall(UpdatePerusahaanRequest request);

    Single<DeletePerusahaanResponse> postDeletePerusahaanApiCall(DeletePerusahaanRequest request);


    //
    Single<MasterResponse> getAllMasterApiCall();

    //
    Single<LogInspeksiResponse> getAllLogInspeksiApiCall();

}
