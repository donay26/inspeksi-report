package com.din.skripsi.inspeksireport.ui.home.status;

public interface StatusNavigator {
    void handleError(Throwable throwable);
    void openDialog(int idProduct, String nama, int i);
    void isRefresh();
    void isSwipeRefresh(boolean swipe);
    void onBack();
    void openCreateStatusActivity();

}

