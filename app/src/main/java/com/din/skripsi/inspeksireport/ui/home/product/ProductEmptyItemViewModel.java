package com.din.skripsi.inspeksireport.ui.home.product;


public class ProductEmptyItemViewModel {

    private final ProductEmptyItemViewModelListener mListener;

    public ProductEmptyItemViewModel(ProductEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface ProductEmptyItemViewModelListener {

        void onRetryClick();
    }
}
