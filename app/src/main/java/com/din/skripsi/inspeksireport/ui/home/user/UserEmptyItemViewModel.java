package com.din.skripsi.inspeksireport.ui.home.user;


public class UserEmptyItemViewModel {

    private final UserEmptyItemViewModelListener mListener;

    public UserEmptyItemViewModel(UserEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface UserEmptyItemViewModelListener {

        void onRetryClick();
    }
}
