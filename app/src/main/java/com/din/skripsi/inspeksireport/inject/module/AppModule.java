package com.din.skripsi.inspeksireport.inject.module;

import android.app.Application;
import android.content.Context;

import com.din.skripsi.inspeksireport.data.AppDataManager;
import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.local.prefs.AppPreferencesHelper;
import com.din.skripsi.inspeksireport.data.local.prefs.PreferencesHelper;
import com.din.skripsi.inspeksireport.data.remote.ApiHelper;
import com.din.skripsi.inspeksireport.data.remote.AppApiHelper;
import com.din.skripsi.inspeksireport.inject.ApiInfo;
import com.din.skripsi.inspeksireport.inject.PreferenceInfo;
import com.din.skripsi.inspeksireport.utils.AppConstants;
import com.din.skripsi.inspeksireport.utils.rx.AppSchedulerProvider;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper){
        return appApiHelper;
    }

    @Provides
    @ApiInfo
    String provideApiKey(){
        return "";
    }

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager){
        return appDataManager;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }


    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    SchedulerProvider providerSchedulerProvider(){
        return new AppSchedulerProvider();
    }

}
