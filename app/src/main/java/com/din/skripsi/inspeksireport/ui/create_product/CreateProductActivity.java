package com.din.skripsi.inspeksireport.ui.create_product;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityCreateProductBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;

public class CreateProductActivity extends BaseActivity<ActivityCreateProductBinding, CreateProductViewModel> implements CreateProductNavigator {

    private ActivityCreateProductBinding mActivityCreateProductBinding;



    public static Intent newIntent(Context context) {
        return new Intent(context, CreateProductActivity.class);
    }




    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_product;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCreateProductBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setSupportActionBar(mActivityCreateProductBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void createProduct() {
        String namaProduct = mActivityCreateProductBinding.etNama.getText().toString();

        if (mViewModel.isValid(namaProduct)) {
            hideKeyboard();
            mViewModel.createProduct(namaProduct);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(CreateProductActivity.this);
        startActivity(intent);
        finish();
    }


}