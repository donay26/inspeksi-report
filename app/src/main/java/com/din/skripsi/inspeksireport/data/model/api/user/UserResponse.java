package com.din.skripsi.inspeksireport.data.model.api.user;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserResponse {

    @Expose
    @SerializedName("data")
    private List<DataUser> listDataUser;

    @Expose
    @SerializedName("status")
    private boolean statusCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserResponse)) {
            return false;
        }

        UserResponse that = (UserResponse) o;

        if (statusCode != that.statusCode) {
            return false;
        }
        if (!message.equals(that.message)) {
            return false;
        }


        return listDataUser.size() != that.listDataUser.size();
    }

    @Override
    public int hashCode() {

        int result = listDataUser.hashCode();
        result = 31 * result;
        return result;
    }

    public List<DataUser> getListDataUser() {
        return listDataUser;
    }
    public String getMessage(){
        return message;
    }

    public boolean getStatusCode() {
        return statusCode;
    }

    public void setDataUser(List<DataUser> data) {
        this.listDataUser = data;
    }


    public static class DataUser {

        @Expose
        @SerializedName("id_user")
        private int id_User;

        @Expose
        @SerializedName("nama")
        private String nama_User;

        @Expose
        @SerializedName("nik")
        private String nik_user;

        @Expose
        @SerializedName("password")
        private String password_User;

        @Expose
        @SerializedName("created_date")
        private String created_date;

        @Expose
        @SerializedName("status")
        private Integer status_User;

        @Expose
        @SerializedName("role")
        private Integer role_user;



        public String getNamaUser() {
            return nama_User;
        }

        public Integer getStatusUser() {
            return status_User;
        }


        public Integer getRoleUser() {
            return role_user;
        }


        public int getIdUser() {
            return id_User;
        };

        public String getNikUser() {
            return nik_user;
        }

        public String getPasswordUser() {
            return password_User;
        }

        public String getCreatedDate() {
            return created_date;
        }




    }
}
