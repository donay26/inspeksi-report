package com.din.skripsi.inspeksireport.ui.home.status;

import androidx.databinding.ObservableField;


public class StatusItemViewModel {

    public final ObservableField<Integer> _idStatus = new ObservableField<>();
    public final ObservableField<String> _namaStatus = new ObservableField<>();
    public final ObservableField<String> _createdDateStatus = new ObservableField<>();

    public final StatusItemViewModel.StatusItemViewModelListener mListener;

    public StatusItemViewModel(int idStatus, String namaStatus, String createdDate, StatusItemViewModel.StatusItemViewModelListener listener) {
        this.mListener = listener;
        this._idStatus.set(idStatus);
        this._namaStatus.set(namaStatus);
        this._createdDateStatus.set(createdDate);
    }

    public void onItemClick() {
        mListener.onItemClick(_idStatus.get(),_namaStatus.get());
    }

    public interface StatusItemViewModelListener {

        void onItemClick(int idStatus,String namaStatus);
    }

}
