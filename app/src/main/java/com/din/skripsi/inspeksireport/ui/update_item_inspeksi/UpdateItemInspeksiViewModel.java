package com.din.skripsi.inspeksireport.ui.update_item_inspeksi;

import android.text.TextUtils;
import android.util.Log;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.update_item_inspeksi.UpdateItemInspeksiRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

public class UpdateItemInspeksiViewModel extends BaseViewModel<UpdateItemInspeksiNavigator> {



    public UpdateItemInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        loadMasters();
    }

    public boolean isValid(String nama,String remarks) {


        if (TextUtils.isEmpty(nama)) {
            return false;
        }

        if (TextUtils.isEmpty(remarks)) {
            return false;
        }
        return true;
    }

    public void updateItemInspeksi(int id_inspeksi,String nama, Integer status,Integer rekomendasi,String remarks) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postUpdateItemInspeksiApiCall(new UpdateItemInspeksiRequest(id_inspeksi,nama,rekomendasi,remarks,status))
                .map(createInspeksiResponse -> createInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(createInspeksiResponse -> {
                    setIsLoading(false);
                    getNavigator().openInspeksiActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }


    public void loadMasters() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getAllMasterApiCall()
                .map(masterResponse -> masterResponse.getDataMaaster())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(dataMaster -> {
                    setIsLoading(false);
                    if (dataMaster != null) {
//                        Log.d("TAG", "loadQuestionCards: " + dataMaster.getDataRekomendasi().size());
//                        productItemLiveData.setValue(questionList);
                        getNavigator().setSpinnerRekomendasi(dataMaster.getDataRekomendasi());
                        getNavigator().setSpinnerStatus(dataMaster.getDataStatus());
                    }
                }, throwable -> {
                    Log.d("TAG", "loadQuestionCards: " + throwable);
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }



    public void onServerUpdateItemInspeksiClick() {
        getNavigator().updateInspeksi();
    }


}
