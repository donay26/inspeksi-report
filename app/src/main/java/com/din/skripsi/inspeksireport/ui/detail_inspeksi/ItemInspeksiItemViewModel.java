package com.din.skripsi.inspeksireport.ui.detail_inspeksi;

import androidx.databinding.ObservableField;


public class ItemInspeksiItemViewModel {

    public final ObservableField<Integer> _idItemInspeksi = new ObservableField<>();
    public final ObservableField<String> _nama = new ObservableField<>();
    public final ObservableField<String> _status = new ObservableField<>();
    public final ObservableField<Integer> _idStatus = new ObservableField<>();
    public final ObservableField<String> _rekomendasi = new ObservableField<>();
    public final ObservableField<Integer> _idRekomendasi = new ObservableField<>();
    public final ObservableField<String> _remarks = new ObservableField<>();
    public final ObservableField<String> _CreatedDate = new ObservableField<>();

    public final ItemInspeksiItemViewModel.ItemInspeksiItemViewModelListener mListener;

    public ItemInspeksiItemViewModel(int idItemInspeksi, String nama, String status,Integer id_status,String rekomendasi,Integer id_rekomendasi,String remarks, String created_date, ItemInspeksiItemViewModel.ItemInspeksiItemViewModelListener listener) {
        this.mListener = listener;
        this._idItemInspeksi.set(idItemInspeksi);
        this._nama.set(nama);
        this._rekomendasi.set(rekomendasi);
        this._idRekomendasi.set(id_rekomendasi);
        this._remarks.set(remarks);
        this._status.set(status);
        this._idStatus.set(id_status);
        this._CreatedDate.set(created_date);
    }

    public void onItemClick() {
        mListener.onItemClick(_idItemInspeksi.get(),_nama.get(),_rekomendasi.get(),_idRekomendasi.get(),_remarks.get(),_status.get(),_idStatus.get(),_CreatedDate.get());
    }

    public interface ItemInspeksiItemViewModelListener {

        void onItemClick(int idItemInspeksi,String nama_product, String rekomendasi,Integer id_rekomendasi ,String remarks,String status,Integer id_status,String created_date);
    }

}
