package com.din.skripsi.inspeksireport.inject.module;

import androidx.core.util.Supplier;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.din.skripsi.inspeksireport.ViewModelProviderFactory;
import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.ui.base.BaseFragment;
import com.din.skripsi.inspeksireport.ui.home.about.AboutViewModel;
import com.din.skripsi.inspeksireport.ui.home.dashboard.DashboardViewModel;
import com.din.skripsi.inspeksireport.ui.home.dashboard.InspeksiAdapter;
import com.din.skripsi.inspeksireport.ui.home.history.HistoryAdapter;
import com.din.skripsi.inspeksireport.ui.home.history.HistoryViewModel;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanAdapter;
import com.din.skripsi.inspeksireport.ui.home.perusahaan.PerusahaanViewModel;
import com.din.skripsi.inspeksireport.ui.home.product.ProductAdapter;
import com.din.skripsi.inspeksireport.ui.home.product.ProductViewModel;
import com.din.skripsi.inspeksireport.ui.home.rekomendasi.RekomendasiAdapter;
import com.din.skripsi.inspeksireport.ui.home.rekomendasi.RekomendasiViewModel;
import com.din.skripsi.inspeksireport.ui.home.status.StatusAdapter;
import com.din.skripsi.inspeksireport.ui.home.status.StatusViewModel;
import com.din.skripsi.inspeksireport.ui.home.user.UserAdapter;
import com.din.skripsi.inspeksireport.ui.home.user.UserViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {
    private BaseFragment<?, ?> fragment;

    public FragmentModule(BaseFragment<?, ?> fragment) {
        this.fragment = fragment;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager() {
        return new LinearLayoutManager(fragment.getActivity());
    }


    @Provides
    AboutViewModel provideAboutViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<AboutViewModel> supplier = () -> new AboutViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<AboutViewModel> factory = new ViewModelProviderFactory<>(AboutViewModel.class, supplier);
        return new ViewModelProvider(fragment, factory).get(AboutViewModel.class);
    }


    @Provides
    DashboardViewModel provideDashboardViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<DashboardViewModel> supplier = () -> new DashboardViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<DashboardViewModel> factory = new ViewModelProviderFactory<>(DashboardViewModel.class, supplier);
        return new ViewModelProvider(fragment, factory).get(DashboardViewModel.class);
    }

    @Provides
    UserViewModel provideUserViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<UserViewModel> supplier = () -> new UserViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<UserViewModel> factory = new ViewModelProviderFactory<>(UserViewModel.class, supplier);
        return new ViewModelProvider(fragment, factory).get(UserViewModel.class);
    }

    @Provides
    ProductViewModel provideProductViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<ProductViewModel> supplier = () -> new ProductViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<ProductViewModel> factory = new ViewModelProviderFactory<>(ProductViewModel.class, supplier);
        return new ViewModelProvider(fragment, factory).get(ProductViewModel.class);
    }

    @Provides
    HistoryViewModel provideHistoryViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<HistoryViewModel> supplier = () -> new HistoryViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<HistoryViewModel> factory = new ViewModelProviderFactory<>(HistoryViewModel.class, supplier);
        return new ViewModelProvider(fragment, factory).get(HistoryViewModel.class);
    }

    @Provides
    RekomendasiViewModel provideRekomendasiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<RekomendasiViewModel> supplier = () -> new RekomendasiViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<RekomendasiViewModel> factory = new ViewModelProviderFactory<>(RekomendasiViewModel.class, supplier);
        return new ViewModelProvider(fragment, factory).get(RekomendasiViewModel.class);
    }

    @Provides
    StatusViewModel provideStatusViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<StatusViewModel> supplier = () -> new StatusViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<StatusViewModel> factory = new ViewModelProviderFactory<>(StatusViewModel.class, supplier);
        return new ViewModelProvider(fragment, factory).get(StatusViewModel.class);
    }

    @Provides
    PerusahaanViewModel providePerusahaanViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<PerusahaanViewModel> supplier = () -> new PerusahaanViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<PerusahaanViewModel> factory = new ViewModelProviderFactory<>(PerusahaanViewModel.class, supplier);
        return new ViewModelProvider(fragment, factory).get(PerusahaanViewModel.class);
    }


    @Provides
    InspeksiAdapter provideInspeksiAdapter() {
        return new InspeksiAdapter() ;
    }

    @Provides
    UserAdapter provideUserAdapter() {
        return new UserAdapter() ;
    }

    @Provides
    ProductAdapter provideProductAdapter() {
        return new ProductAdapter() ;
    }

    @Provides
    HistoryAdapter provideHistoryAdapter() {
        return new HistoryAdapter() ;
    }

    @Provides
    RekomendasiAdapter provideRekomendasiAdapter() {
        return new RekomendasiAdapter() ;
    }

    @Provides
    StatusAdapter provideStatusAdapter() {
        return new StatusAdapter() ;
    }

    @Provides
    PerusahaanAdapter providePerusahaanAdapter() {
        return new PerusahaanAdapter() ;
    }



}
