package com.din.skripsi.inspeksireport.data.model.api.master;


import com.din.skripsi.inspeksireport.data.model.api.perusahaan.PerusahaanResponse;
import com.din.skripsi.inspeksireport.data.model.api.product.ProductResponse;
import com.din.skripsi.inspeksireport.data.model.api.rekomendasi.RekomendasiResponse;
import com.din.skripsi.inspeksireport.data.model.api.status.StatusResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MasterResponse {

    @Expose
    @SerializedName("data")
    private DataMaster dataMaster;

    @Expose
    @SerializedName("status_code")
    private boolean statusCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MasterResponse)) {
            return false;
        }

        MasterResponse that = (MasterResponse) o;


        if (!message.equals(that.message)) {
            return false;
        }


        return statusCode != that.statusCode;
    }

    @Override
    public int hashCode() {

        int result = dataMaster.hashCode();
        result = 31 * result;
        return result;
    }

    public DataMaster getDataMaaster() {
        return dataMaster;
    }

    public String getMessage() {
        return message;
    }

    public boolean getStatusCode() {
        return statusCode;
    }

    public void setDataMaster(DataMaster data) {
        this.dataMaster = data;
    }


    public static class DataMaster {

        @Expose
        @SerializedName("status")
        private List<StatusResponse.DataStatus> listDataStatus;


        @Expose
        @SerializedName("perusahaan")
        private List<PerusahaanResponse.DataPerusahaan> listDataPerusahaan;

        @Expose
        @SerializedName("rekomendasi")
        private List<RekomendasiResponse.DataRekomendasi> listDataRekomendasi;

        @Expose
        @SerializedName("product")
        private List<ProductResponse.DataProduct> listDataProduct;


        public List<StatusResponse.DataStatus> getDataStatus() {
            return listDataStatus;
        }

        public List<PerusahaanResponse.DataPerusahaan> getDataPerusahaan() {
           return listDataPerusahaan;
        }

        public List<RekomendasiResponse.DataRekomendasi> getDataRekomendasi() {
            return listDataRekomendasi;
        }

        public List<ProductResponse.DataProduct> getDataProduct() {
            return listDataProduct;
        }


    }

//    public static class DataProduct {
//
//        @Expose
//        @SerializedName("id_product")
//        private Integer id_product;
//
//        @Expose
//        @SerializedName("nama")
//        private String nama_product;
//
//        @Expose
//        @SerializedName("status")
//        private Integer status_product;
//
//        @Expose
//        @SerializedName("created_date")
//        private String created_date;
//
//
//        public String getNamaProduct() {
//            return nama_product;
//        }
//
//        public Integer getStatusProduct() {
//            return status_product;
//        }
//
//
//        public Integer getIdProduct() {
//            return id_product;
//        }
//
//        public String getCreatedDate() {
//            return created_date;
//        }
//
//
//    }
}
