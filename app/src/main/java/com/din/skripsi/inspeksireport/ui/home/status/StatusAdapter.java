package com.din.skripsi.inspeksireport.ui.home.status;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.din.skripsi.inspeksireport.databinding.ItemStatusEmptyViewBinding;
import com.din.skripsi.inspeksireport.databinding.ItemStatusViewBinding;
import com.din.skripsi.inspeksireport.ui.base.BaseViewHolder;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class StatusAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<StatusItemViewModel> mStatusResponseList;

    private StatusAdapter.StatusAdapterListener mListener;

    public StatusAdapter() {
        this.mStatusResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mStatusResponseList.isEmpty()) {
            return mStatusResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mStatusResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemStatusViewBinding rekomendasiViewBinding = ItemStatusViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new StatusAdapter.StatusViewHolder(rekomendasiViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemStatusEmptyViewBinding emptyViewBinding = ItemStatusEmptyViewBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new StatusAdapter.EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<StatusItemViewModel> repoList) {
        mStatusResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mStatusResponseList.clear();
    }

    public void setListener(StatusAdapter.StatusAdapterListener listener) {
        this.mListener = listener;
    }

    public interface StatusAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements StatusEmptyItemViewModel.StatusEmptyItemViewModelListener {

        private final ItemStatusEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemStatusEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            StatusEmptyItemViewModel emptyItemViewModel = new StatusEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class StatusViewHolder extends BaseViewHolder {

        private final ItemStatusViewBinding mBinding;

        public StatusViewHolder(ItemStatusViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final StatusItemViewModel mStatusItemViewModel = mStatusResponseList.get(position);

            mBinding.setViewModel(mStatusItemViewModel);
            mBinding.createdDateTextView.setText(CommonUtils.convertTimeStamp(mStatusResponseList.get(position)._createdDateStatus.get()));


            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }


    }
}