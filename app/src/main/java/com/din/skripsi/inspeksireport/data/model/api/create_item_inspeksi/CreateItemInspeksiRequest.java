package com.din.skripsi.inspeksireport.data.model.api.create_item_inspeksi;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class CreateItemInspeksiRequest extends JSONObject {


    @Expose
    @SerializedName("id_inspeksi")
    private int id_inspeksi;

    @Expose
    @SerializedName("nama")
    private String nama;

    @Expose
    @SerializedName("id_rekomendasi")
    private Integer rekomendasi;

    @Expose
    @SerializedName("remarks")
    private String remarks;

    @Expose
    @SerializedName("id_status")
    private Integer status;




    public CreateItemInspeksiRequest(int id_inspeksi, String nama, Integer rekomendasi, String remarks, Integer status) {
        this.id_inspeksi = id_inspeksi;
        this.nama = nama;
        this.rekomendasi = rekomendasi;
        this.remarks = remarks;
        this.status = status;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        CreateItemInspeksiRequest that = (CreateItemInspeksiRequest) object;



        return id_inspeksi == 0 ? id_inspeksi == that.id_inspeksi : that.id_inspeksi == 0;

    }


    @Override
    public int hashCode() {
        int result = nama.hashCode() != 0 ? nama.hashCode() : 0;
        return result;
    }

    public String getNama() {
        return nama;
    }
    public Integer getRekomendasi() {
        return rekomendasi;
    }
    public String getRemarks() {
        return remarks;
    }
    public Integer getIdInspeksi() {
        return id_inspeksi;
    }
    public Integer getStatus() {
        return status;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("nama", getNama());
            jsonObject.put("id_rekomendasi", getRekomendasi());
            jsonObject.put("remarks", getRemarks());
            jsonObject.put("id_inspeksi", getIdInspeksi());
            jsonObject.put("id_status", getStatus());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, CreateItemInspeksiRequest.class);
    }

}
