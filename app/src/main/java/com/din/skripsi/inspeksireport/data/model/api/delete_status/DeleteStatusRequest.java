package com.din.skripsi.inspeksireport.data.model.api.delete_status;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public final class DeleteStatusRequest extends JSONObject {


    @Expose
    @SerializedName("id_status")
    private int id_status;




    public DeleteStatusRequest(int id_status) {
        this.id_status = id_status;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DeleteStatusRequest that = (DeleteStatusRequest) object;



        return id_status == 0 ? id_status == that.id_status : that.id_status == 0;

    }


    @Override
    public int hashCode() {
        int result = id_status != 0 ? id_status : 0;
        return result;
    }

    public Integer getIdStatus() {
        return id_status;
    }




    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id_status", getIdStatus());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, DeleteStatusRequest.class);
    }

}
