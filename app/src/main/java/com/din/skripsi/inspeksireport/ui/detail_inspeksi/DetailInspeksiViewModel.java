package com.din.skripsi.inspeksireport.ui.detail_inspeksi;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.din.skripsi.inspeksireport.data.DataManager;
import com.din.skripsi.inspeksireport.data.model.api.approval_inspeksi.ApprovalInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_inspeksi.DeleteInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.delete_item_inspeksi.DeleteItemInspeksiRequest;
import com.din.skripsi.inspeksireport.data.model.api.item_inspeksi.ItemInspeksiResponse;
import com.din.skripsi.inspeksireport.data.model.api.login.LoginRequest;
import com.din.skripsi.inspeksireport.ui.base.BaseViewModel;
import com.din.skripsi.inspeksireport.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.JsonParser;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class DetailInspeksiViewModel extends BaseViewModel<DetailInspeksiNavigator> implements ItemInspeksiItemViewModel.ItemInspeksiItemViewModelListener {

    private final MutableLiveData<List<ItemInspeksiItemViewModel>> itemInspeksiItemsLiveData;


    private final ObservableField<Integer> userRole = new ObservableField<>();

    public final ObservableField<String> imageUrl = new ObservableField<>();

    public final ObservableField<String> jsonResponse = new ObservableField<>();


    public DetailInspeksiViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        itemInspeksiItemsLiveData = new MutableLiveData<>();
    }

    public ObservableField<Integer> getUserRole() {
        return userRole;
    }
    public ObservableField<String> getImageUrl() {
        return imageUrl;
    }
    public ObservableField<String> getJsonResponse() {
        return jsonResponse;
    }


    public void onNavMenuCreated(String url_image) {


        final Integer currentUserrole = getDataManager().getCurrentUserRole();
        userRole.set(currentUserrole);

        imageUrl.set(url_image);

    }



    public void fetchItemInspeksis(int id_inspeksi) {
//        setIsLoading(true);
        getNavigator().isSwipeRefresh(true);

        getCompositeDisposable().add(getDataManager()
                .getItemInspeksiApiCall(id_inspeksi)
                .map(itemInspeksiResponse -> {
                    Gson gson = new Gson();
                    String json = gson.toJson(itemInspeksiResponse);
                    jsonResponse.set(json);
                  return itemInspeksiResponse.getListDataItemInspeksi();
                })
                .flatMap(this::getViewModelList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(inspeksiResponse -> {
                    Log.e("Listnya", String.valueOf(inspeksiResponse.size()));

                    itemInspeksiItemsLiveData.setValue(inspeksiResponse);
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                }, throwable -> {
                    Log.e("Listnya", String.valueOf(throwable.getMessage()));
//                    setIsLoading(false);
                    getNavigator().isSwipeRefresh(false);
                    getNavigator().handleError(throwable);
                }));
    }


    public void deleteItemInspeksi(int id_item_insepski) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postDeleteItemInspeksiApiCall(new DeleteItemInspeksiRequest(getDataManager().getCurrentUserId(),id_item_insepski))
                .map(deleteItemInspeksiResponse -> deleteItemInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
                    setIsLoading(false);
                    getNavigator().refreshListInspeksi();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }

    public void statusInspeksi(String idUnique,int id_insepski, int status) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postApprovalInspeksiApiCall(new ApprovalInspeksiRequest(idUnique,getDataManager().getCurrentUserId(),id_insepski,status))
                .map(approvalInspeksiResponse -> approvalInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
                    setIsLoading(false);
                    getNavigator().refreshListInspeksi();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }



    public void deleteInspeksi(int id_insepski) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().postDeleteInspeksiApiCall(new DeleteInspeksiRequest(id_insepski))
                .map(deleteInspeksiResponse -> deleteInspeksiResponse)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }



    public LiveData<List<ItemInspeksiItemViewModel>> getItemInspeksiItemsLiveData() {
        return itemInspeksiItemsLiveData;
    }

    private Single<List<ItemInspeksiItemViewModel>> getViewModelList(List<ItemInspeksiResponse.DataItemInspeksi> repoList) {
        return Observable.fromIterable(repoList)
                .map(repo -> {
                    // masuk

                    return new ItemInspeksiItemViewModel(
                            repo.getIdItemInspeksi(), repo.getNama(),
                            repo.getStatus(),repo.getIdStatus(),repo.getRekomendasi(),repo.getIdRekomendasi(),repo.getRemarks(), repo.getCreatedDate(),this);
                }).toList();
    }




    public void onBackClick() {
//        getNavigator().onBack();
    }


    public void toCreateItemInspeksiClick() {
        getNavigator().openCreateItemInspeksi();
    }

    public void toUpdateInspeksiClick() {
        getNavigator().openUpdateInspeksi();
    }

    public void toApproveInspeksiClick() {
        getNavigator().approveInspeksi();
    }

    public void toRejectInspeksiClick() {
        getNavigator().rejectInspeksi();
    }

    public void toPendingInspeksiClick() {
        getNavigator().pendingInspeksi();
    }


    public void toDoneInspeksiClick() {
        getNavigator().doneInspeksi();
    }

    public void toPrintInspeksiClick() {
        getNavigator().printInspeksi();
    }





    public void onServerDeleteInspeksiClick() {
        getNavigator().deleteInspeksi();
    }


    @Override
    public void onItemClick(int idItemInspeksi, String nama_product, String rekomendasi, Integer id_rekomendasi, String remarks, String status, Integer id_status, String created_date) {
        try {
            getNavigator().openDialogItemInspeksi(idItemInspeksi,nama_product,rekomendasi,id_rekomendasi,remarks,status,id_status,created_date);
        } catch (Exception e) {
        }

    }
}
