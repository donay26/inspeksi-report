package com.din.skripsi.inspeksireport.ui.update_user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.din.skripsi.inspeksireport.BR;
import com.din.skripsi.inspeksireport.R;
import com.din.skripsi.inspeksireport.databinding.ActivityUpdateUserBinding;
import com.din.skripsi.inspeksireport.inject.component.ActivityComponent;
import com.din.skripsi.inspeksireport.ui.base.BaseActivity;
import com.din.skripsi.inspeksireport.ui.home.MainActivity;
import com.din.skripsi.inspeksireport.utils.CommonUtils;

public class UpdateUserActivity extends BaseActivity<ActivityUpdateUserBinding, UpdateUserViewModel> implements UpdateUserNavigator  {

    private ActivityUpdateUserBinding mActivityUpdateUserBinding;

    private String[] levelUser = {
            "Super Admin",
            "Kepala Lab",
            "Staff Lab",
            "Officer Lab",
    };

    private String[] statusUser = {
            "NonAktif",
            "Aktif",
    };


    private static final String PREF_KEY_ID_USER = "PREF_KEY_CURRENT_ID_USER";

    private static final String PREF_KEY_NAMA = "PREF_KEY_CURRENT_NAMA";

    private static final String PREF_KEY_STATUS = "PREF_KEY_CURRENT_STATUS";

    private static final String PREF_KEY_ROLE = "PREF_KEY_CURRENT_ROLE";

    private static final String PREF_KEY_PASSWORD = "PREF_KEY_CURRENT_PASSWORD";

    private static final String PREF_KEY_NIK = "PREF_KEY_CURRENT_NIK";
    private Spinner spinnerRole;
    private Spinner spinnerStatus;


    public static Intent newIntent(Context context,
                                   int idUser,
                                   String nama,
                                   int status,
                                   int role,
                                   String password,
                                   String nik) {
        Intent intent = new Intent(context, UpdateUserActivity.class);
        intent.putExtra(PREF_KEY_ID_USER, idUser);
        intent.putExtra(PREF_KEY_NAMA, nama);
        intent.putExtra(PREF_KEY_STATUS, status);
        intent.putExtra(PREF_KEY_ROLE, role);
        intent.putExtra(PREF_KEY_PASSWORD, password);
        intent.putExtra(PREF_KEY_NIK, nik);
        return intent;
    }


    public Integer getPrefKeyIdUser() {
        Bundle intent = getIntent().getExtras();
        Integer id_user = intent.getInt(PREF_KEY_ID_USER, 0);
        return id_user;
    }

    public Integer getPrefKeyStatus() {
        Bundle intent = getIntent().getExtras();
        Integer status = intent.getInt(PREF_KEY_STATUS, 0);
        return status;
    }

    public Integer getPrefRole() {
        Bundle intent = getIntent().getExtras();
        Integer role = intent.getInt(PREF_KEY_ROLE, 0);
        return role;
    }

    public String getPrefKeyNama() {
        Bundle intent = getIntent().getExtras();
        String nama = intent.getString(PREF_KEY_NAMA);
        return nama;
    }

    public String getPrefKeyPassword() {
        Bundle intent = getIntent().getExtras();
        String password = intent.getString(PREF_KEY_PASSWORD);
        return password;
    }

    public String getPrefKeyNIK() {
        Bundle intent = getIntent().getExtras();
        String nik = intent.getString(PREF_KEY_NIK);
        return nik;
    }






    public static Intent newIntent(Context context) {
        return new Intent(context, UpdateUserActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_user;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityUpdateUserBinding = getViewDataBinding();
        mViewModel.setNavigator(this);

        setSupportActionBar(mActivityUpdateUserBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUp();
        spinnerStatus = mActivityUpdateUserBinding.statusSpinner;
        final ArrayAdapter<String> adapterStatus = new ArrayAdapter<>(this,
                R.layout.spinner_item, statusUser);

        // mengeset Array Adapter tersebut ke Spinner
        spinnerStatus.setAdapter(adapterStatus);
        Log.d("==>", String.valueOf(spinnerStatus.getAdapter().getCount()));

        for (int position = 0; position < adapterStatus.getCount(); position++) {
            if(position == getPrefKeyStatus()) {
                spinnerStatus.setSelection(position);
                return;
            }
        }

        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                spinnerStatus.setSelection(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {

    }


    private  void setUp(){
        mActivityUpdateUserBinding.etNamaUser.setText(getPrefKeyNama());
        mActivityUpdateUserBinding.etNIK.setText(getPrefKeyNIK());
        mActivityUpdateUserBinding.etPassword.setText(getPrefKeyPassword());

        spinnerRole = mActivityUpdateUserBinding.roleSpinner;
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, levelUser);

        // mengeset Array Adapter tersebut ke Spinner
        spinnerRole.setAdapter(adapter);

        for (int position = 0; position < adapter.getCount(); position++) {
            if(adapter.getItem(position).equalsIgnoreCase(CommonUtils.convertRoleString(getPrefRole()))) {
                spinnerRole.setSelection(position);
                return;
            }
        }

        spinnerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                spinnerRole.setSelection(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }



    @Override
    public void updateUser() {

        String namaUser = mActivityUpdateUserBinding.etNamaUser.getText().toString();
        String nikUser = mActivityUpdateUserBinding.etNIK.getText().toString();
        String passwrodUser = mActivityUpdateUserBinding.etPassword.getText().toString();
        int role  = CommonUtils.convertRole(mActivityUpdateUserBinding.roleSpinner.getSelectedItem().toString());


        if (mViewModel.isValid(namaUser, nikUser,passwrodUser)) {
            hideKeyboard();
            mViewModel.updateUser(getPrefKeyIdUser(),namaUser, nikUser,passwrodUser,role, mActivityUpdateUserBinding.statusSpinner.getSelectedItemPosition());
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(UpdateUserActivity.this);
        startActivity(intent);
        finish();
    }





}