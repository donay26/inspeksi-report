package com.din.skripsi.inspeksireport.ui.create_perusahaan;

public interface CreatePerusahaanNavigator {

    void handleError(Throwable throwable);

    void createPerusahaan();

    void openMainActivity();

}
