package com.din.skripsi.inspeksireport.ui.home.perusahaan;

import androidx.databinding.ObservableField;


public class PerusahaanItemViewModel {

    public final ObservableField<Integer> _idPerusahaan = new ObservableField<>();
    public final ObservableField<String> _namaPerusahaan = new ObservableField<>();
    public final ObservableField<String> _kodePerusahaan = new ObservableField<>();
    public final ObservableField<String> _alamatPerusahaan = new ObservableField<>();
    public final ObservableField<String> _createdDatePerusahaan = new ObservableField<>();

    public final PerusahaanItemViewModel.PerusahaanItemViewModelListener mListener;

    public PerusahaanItemViewModel(int idPerusahaan, String nama_perusahaan,String kode_perusahaan,String alamat_perusahaan,  String created_date, PerusahaanItemViewModel.PerusahaanItemViewModelListener listener) {
        this.mListener = listener;
        this._idPerusahaan.set(idPerusahaan);
        this._namaPerusahaan.set(nama_perusahaan);
        this._kodePerusahaan.set(kode_perusahaan);
        this._alamatPerusahaan.set(alamat_perusahaan);
        this._createdDatePerusahaan.set(created_date);
    }

    public void onItemClick() {
        mListener.onItemClick(_idPerusahaan.get(),_namaPerusahaan.get(),_kodePerusahaan.get(),_alamatPerusahaan.get());
    }

    public interface PerusahaanItemViewModelListener {

        void onItemClick(int idPerusahaan,String nama_perusahaan,String kode_perusahaan,String alamat_perusahaan);
    }

}
